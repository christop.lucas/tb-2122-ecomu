/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: App
 
  Purpose: This file contains the EcomuApp Widget. This is the root Widget

  Author:  Christopher Lucas
  Date:    01.06.2022
*/
import 'package:ecomu_gestion/app/viewmodel/app_viewmodel.dart';
import 'package:ecomu_gestion/app/viewmodel/menu_viewmodel.dart';
import 'package:ecomu_gestion/app/viewmodel/theme_viewmodel.dart';
import 'package:ecomu_gestion/app/widgets/ecomu_structure_widget.dart';
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/screens/factures_screen/factures_viewmodel.dart';
import 'package:ecomu_gestion/screens/home_screen/home_viewmodel.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/inscriptions_viewmodel.dart';
import 'package:ecomu_gestion/screens/login_screen/login_viewmodel.dart';
import 'package:ecomu_gestion/screens/option_screen/option_viewmodel.dart';
import 'package:ecomu_gestion/screens/person_screen/person_viewmodel.dart';
import 'package:ecomu_gestion/screens/student_screen/student_viewmodel.dart';
import 'package:ecomu_gestion/screens/teacher_screen/teacher_viewmodel.dart';
import 'package:ecomu_gestion/services/ecomu_repository/ecomu_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

class EcomuApp extends StatelessWidget {
  final EcomuRepository _repository;
  const EcomuApp({
    Key? key,
    required EcomuRepository repository,
  })  : _repository = repository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => AppViewModel(),
        ),
        ChangeNotifierProvider(
          create: (_) => MenuViewModel(),
        ),
        ChangeNotifierProvider(
          create: (_) => ThemeViewModel(),
        ),
        ChangeNotifierProvider(
          create: (_) => StudentViewModel(repository: _repository),
        ),
        ChangeNotifierProvider(
          create: (_) => PersonViewModel(repository: _repository),
        ),
        ChangeNotifierProvider(
          create: (_) => HomeViewModel(repository: _repository),
        ),
        ChangeNotifierProvider(
          create: (_) => TeacherViewModel(repository: _repository),
        ),
        ChangeNotifierProvider(
          create: (_) => InscriptionsViewModel(repository: _repository),
        ),
        ChangeNotifierProvider(
          create: (_) => OptionViewModel(repository: _repository),
        ),
        ChangeNotifierProvider(
          create: (_) => LoginViewModel(repository: _repository),
        ),
        ChangeNotifierProvider(
          create: (_) => FactureViewModel(repository: _repository),
        ),
      ],
      child: Consumer<ThemeViewModel>(
        builder: (BuildContext context, themeViewModel, child) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            home: const EcomuStructure(),
            locale: const Locale('fr', 'CH'),
            localizationsDelegates: const [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate
            ],
            supportedLocales: const [
              Locale('fr', 'CH'),
            ],
            theme: ThemeData(
              brightness: Brightness.light,
              useMaterial3: true,
              colorSchemeSeed: themeViewModel.appColor,
              dialogTheme: DialogTheme(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
              elevatedButtonTheme: ElevatedButtonThemeData(
                style: ElevatedButton.styleFrom(
                  onPrimary: Theme.of(context).colorScheme.primary,
                  primary: Theme.of(context).colorScheme.onPrimary,
                  minimumSize: const Size(50, 50),
                  textStyle: const TextStyle(fontSize: 16.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ).copyWith(
                  elevation: ButtonStyleButton.allOrNull(0.0),
                ),
              ),
              textButtonTheme: TextButtonThemeData(
                style: ElevatedButton.styleFrom(
                  minimumSize: const Size(50, 50),
                  textStyle: const TextStyle(fontSize: 16.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ).copyWith(
                  elevation: ButtonStyleButton.allOrNull(0.0),
                ),
              ),
            ),
            darkTheme: ThemeData(
              brightness: Brightness.dark,
              useMaterial3: true,
              colorSchemeSeed: themeViewModel.appColor,
              dialogTheme: DialogTheme(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(2),
                ),
              ),
              elevatedButtonTheme: ElevatedButtonThemeData(
                style: ElevatedButton.styleFrom(
                  minimumSize: const Size(50, 50),
                  textStyle: const TextStyle(fontSize: 16.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ).copyWith(
                  elevation: ButtonStyleButton.allOrNull(0.0),
                ),
              ),
              textButtonTheme: TextButtonThemeData(
                style: ElevatedButton.styleFrom(
                  minimumSize: const Size(50, 50),
                  textStyle: const TextStyle(fontSize: 16.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ).copyWith(
                  elevation: ButtonStyleButton.allOrNull(0.0),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
