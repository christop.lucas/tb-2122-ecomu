/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: App
 
  Purpose: This file contains the widget of the action bar.

  Author:  Christopher Lucas
  Date:    04.06.2022
*/
import 'package:ecomu_gestion/app/viewmodel/app_viewmodel.dart';
import 'package:ecomu_gestion/app/viewmodel/menu_viewmodel.dart';
import 'package:ecomu_gestion/screens/factures_screen/widgets/factures_action_bar_widget.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/widgets/inscriptions_action_bar_widget.dart';
import 'package:ecomu_gestion/screens/option_screen/widgets/option_action_bar_widget.dart';
import 'package:ecomu_gestion/screens/person_screen/widgets/person_action_bar_widget.dart';
import 'package:ecomu_gestion/screens/student_screen/widgets/student_action_bar_widget.dart';
import 'package:ecomu_gestion/screens/teacher_screen/widgets/teacher_action_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EcomuActionBar extends StatefulWidget {
  const EcomuActionBar({
    Key? key,
  }) : super(key: key);

  @override
  State<EcomuActionBar> createState() => _EcomuActionBarState();
}

class _EcomuActionBarState extends State<EcomuActionBar> {
  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<MenuViewModel>();
    return Container(
      color: Theme.of(context).colorScheme.inversePrimary,
      child: Row(
        children: [
          // Hamburger Menu to open/close side menu
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Selector<MenuViewModel, bool>(
              builder: (context, value, child) {
                return IconButton(
                  onPressed: () {
                    viewModel.setOpen = !value;
                  },
                  icon: const Icon(Icons.menu),
                  splashRadius: 1,
                );
              },
              selector: (_, __) => viewModel.open,
            ),
          ),
          // Action bar for the correct displayed view
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 50),
              child: Builder(
                builder: (context) {
                  final appViewModel = context.watch<AppViewModel>();
                  switch (appViewModel.state) {
                    /*case AppState.home:
                      return const HomeActionBar();*/
                    case AppState.inscription:
                      return const InscriptionsActionBar();
                    case AppState.factures:
                      return const FacturesActionBar();
                    case AppState.contacts:
                      return const PersonActionBar();
                    case AppState.eleves:
                      return const StudentActionBar();
                    case AppState.professeurs:
                      return const TeacherActionBar();
                    case AppState.autres:
                      return const OptionActionBar();
                  }
                },
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              border: Border(
                left: BorderSide(
                  width: 1.0,
                  color: Theme.of(context).colorScheme.surfaceVariant,
                ),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 2.0,
                horizontal: 20.0,
              ),
              child: Row(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(right: 16.0),
                    child: CircleAvatar(
                      radius: 25,
                      child: Text(
                        "P",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                  ),
                  DropdownButtonHideUnderline(
                    child: ButtonTheme(
                      alignedDropdown: true,
                      child: DropdownButton(
                        value: "Profil",
                        items: ["Profil", "Deconnexion"].map((String e) {
                          return DropdownMenuItem<String>(
                            value: e,
                            child: Text(e),
                            onTap: () {
                              context.read<AppViewModel>().setConnected = false;
                            },
                          );
                        }).toList(),
                        onChanged: (value) {},
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                    ),
                  ),
                  /*DropdownButton(
                    value: "Deconnexion",
                    items: ["Deconnexion"].map((String e) {
                      return DropdownMenuItem<String>(
                        value: e,
                        child: Text(e),
                      );
                    }).toList(),
                    onChanged: (value) {},
                    
                  ),*/
                  /*const Text(
                    "Profile",
                    style: TextStyle(fontSize: 18),
                  ),*/
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
