/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: App
 
  Purpose: This file contains the widget that handles the content of the
      different menu items.

  Author:  Christopher Lucas
  Date:    04.06.2022
*/
import 'package:ecomu_gestion/app/viewmodel/app_viewmodel.dart';
import 'package:ecomu_gestion/screens/factures_screen/factures_view.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/inscriptions_view.dart';
import 'package:ecomu_gestion/screens/option_screen/option_view.dart';
import 'package:ecomu_gestion/screens/person_screen/person_view.dart';
import 'package:ecomu_gestion/screens/student_screen/student_view.dart';
import 'package:ecomu_gestion/screens/teacher_screen/teacher_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EcomuContent extends StatefulWidget {
  const EcomuContent({Key? key}) : super(key: key);

  @override
  State<EcomuContent> createState() => _EcomuContentState();
}

class _EcomuContentState extends State<EcomuContent> {
  @override
  Widget build(BuildContext context) {
    final viewModel = context.watch<AppViewModel>();
    switch (viewModel.state) {
      /*case AppState.home:
        return const EcomuHomeView();*/
      case AppState.inscription:
        return const EcomuInscriptionsView();
      case AppState.factures:
        return const EcomuFacturesView();
      case AppState.contacts:
        return const EcomuPersonView();
      case AppState.eleves:
        return const EcomuStudentView();
      case AppState.professeurs:
        return const EcomuTeacherView();
      case AppState.autres:
        return const EcomuOptionView();
    }
  }
}
