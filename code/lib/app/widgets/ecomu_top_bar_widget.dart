import 'package:flutter/material.dart';

class EcomuTopBar extends StatelessWidget {
  final Widget children;
  const EcomuTopBar({
    Key? key,
    required this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: ClipRRect(
        child: children,
      ),
    );
  }
}
