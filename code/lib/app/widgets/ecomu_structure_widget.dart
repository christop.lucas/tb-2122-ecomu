/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: App
 
  Purpose: This file contains the widget with all the different main
      components of Ecomu (Action Bar, Menu, Content)

  Author:  Christopher Lucas
  Date:    04.06.2022
*/
import 'package:ecomu_gestion/app/viewmodel/app_viewmodel.dart';
import 'package:ecomu_gestion/app/viewmodel/menu_viewmodel.dart';
import 'package:ecomu_gestion/app/widgets/ecomu_action_bar_widget.dart';
import 'package:ecomu_gestion/app/widgets/ecomu_content_widget.dart';
import 'package:ecomu_gestion/app/widgets/ecomu_menu_widget.dart';
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/screens/login_screen/login_view.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EcomuStructure extends StatelessWidget {
  const EcomuStructure({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.surfaceVariant,
      body: Selector<AppViewModel, bool>(
        builder: (context, connected, child) {
          if (connected) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(bottom: 12.0),
                  child: EcomuActionBar(),
                ),
                Expanded(
                  child: Row(
                    children: [
                      EcomuMenu(
                        pathImage: "images/logo.png",
                        titleImage: local.title,
                        menuItems: context.read<MenuViewModel>().constructMenu(
                              local.menu_items.split(local.splitter),
                            ),
                      ),
                      const EcomuContent(),
                    ],
                  ),
                ),
              ],
            );
          }
          return const LoginView();
        },
        selector: (_, __) => context.read<AppViewModel>().connected,
      ),
    );
  }
}
