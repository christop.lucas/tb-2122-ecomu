/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: App
 
  Purpose: This file contains the widget of the menu.

  Author:  Christopher Lucas
  Date:    04.06.2022
*/
import 'package:ecomu_gestion/app/viewmodel/app_viewmodel.dart';
import 'package:ecomu_gestion/app/viewmodel/menu_viewmodel.dart';
import 'package:ecomu_gestion/models/menu_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EcomuMenu extends StatefulWidget {
  /// Path to the displayed image
  final String pathImage;

  /// Title that follows the image
  final String titleImage;

  /// List of the menu Items [EcomuMenuItem] to be displayed
  final List<EcomuMenuItem> menuItems;
  const EcomuMenu({
    Key? key,
    required this.pathImage,
    required this.titleImage,
    required this.menuItems,
  }) : super(key: key);

  @override
  State<EcomuMenu> createState() => _EcomuMenuState();
}

class _EcomuMenuState extends State<EcomuMenu> {
  @override
  Widget build(BuildContext context) {
    final viewModel = context.watch<MenuViewModel>();
    final appViewModel = context.read<AppViewModel>();
    return Column(
      children: [
        Card(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: SizedBox(
              width: viewModel.open ? 215.0 : 70,
              child: Builder(builder: (context) {
                if (viewModel.open) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Spacer(),
                      Image(
                        image: AssetImage(widget.pathImage),
                        height: 40,
                        width: 40,
                      ),
                      const Spacer(),
                      Text(widget.titleImage),
                      const Spacer(),
                    ],
                  );
                } else {
                  return Image(
                    image: AssetImage(widget.pathImage),
                    height: 40,
                    width: 40,
                  );
                }
              }),
            ),
          ),
        ),
        Expanded(
          child: SizedBox(
            width: viewModel.open ? 225.0 : 80,
            child: ListView.builder(
              controller: ScrollController(),
              itemCount: widget.menuItems.length,
              itemBuilder: (context, index) {
                var selected = (viewModel.selectedItem == index);
                return Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: ListTile(
                    leading: viewModel.open
                        ? Icon(widget.menuItems[index].iconData)
                        : null,
                    title: viewModel.open
                        ? Text(widget.menuItems[index].name)
                        : Icon(widget.menuItems[index].iconData),
                    selected: selected,
                    onTap: () {
                      if (!selected) {
                        viewModel.setSelectedItem = index;
                        appViewModel.setAppState = AppState.values[index];
                      }
                    },
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
