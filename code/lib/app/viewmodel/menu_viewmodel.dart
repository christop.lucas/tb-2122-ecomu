/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: App
 
  Purpose: This file contains the view model for the App View.

  Author:  Christopher Lucas
  Date:    02.06.2022
*/
import 'package:ecomu_gestion/models/menu_item.dart';
import 'package:flutter/material.dart';

class MenuViewModel extends ChangeNotifier {
  var _selectedItem = 0;
  var _open = true;
  var _menuItems = <EcomuMenuItem>[];

  MenuViewModel();

  get selectedItem {
    return _selectedItem;
  }

  set setSelectedItem(int value) {
    _selectedItem = value;
    notifyListeners();
  }

  get open {
    return _open;
  }

  set setOpen(bool value) {
    _open = value;
    notifyListeners();
  }

  List<EcomuMenuItem> get menuItems {
    return _menuItems;
  }

  List<EcomuMenuItem> constructMenu(List<String> menuItemTexts) {
    return [
      /*EcomuMenuItem(
        name: menuItemTexts[0],
        iconData: Icons.home,
      ),*/
      EcomuMenuItem(
        name: menuItemTexts[0],
        iconData: Icons.app_registration,
      ),
      EcomuMenuItem(
        name: menuItemTexts[1],
        iconData: Icons.paid,
      ),
      EcomuMenuItem(
        name: menuItemTexts[2],
        iconData: Icons.group,
      ),
      EcomuMenuItem(
        name: menuItemTexts[3],
        iconData: Icons.school,
      ),
      EcomuMenuItem(
        name: menuItemTexts[4],
        iconData: Icons.person,
      ),
      EcomuMenuItem(
        name: menuItemTexts[5],
        iconData: Icons.mode_edit_outline_sharp,
      ),
    ];
  }

  /*  List<EcomuMenuItem> constructMenu(BuildContext context) {
    var listMenu = <EcomuMenuItem>[];
    var local = AppLocalizations.of(context)!;
    listMenu.add(EcomuMenuItem(
      name: local.menu_item1,
      iconData: Icons.home,
    ));
    listMenu.add(EcomuMenuItem(
      name: local.menu_item2,
      iconData: Icons.app_registration,
    ));
    listMenu.add(EcomuMenuItem(
      name: local.menu_item4,
      iconData: Icons.paid,
    ));
    listMenu.add(EcomuMenuItem(
      name: local.menu_item5,
      iconData: Icons.group,
    ));
    listMenu.add(EcomuMenuItem(
      name: local.menu_item6,
      iconData: Icons.school,
    ));
    listMenu.add(EcomuMenuItem(
      name: local.menu_item7,
      iconData: Icons.person,
    ));
    listMenu.add(EcomuMenuItem(
      name: local.menu_item8,
      iconData: Icons.mode_edit_outline_sharp,
    ));
    return listMenu;
  }*/
}
