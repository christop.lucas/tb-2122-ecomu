/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: App
 
  Purpose: This file contains the view model for the App View.

  Author:  Christopher Lucas
  Date:    02.06.2022
*/
import 'package:flutter/material.dart';

class ThemeViewModel extends ChangeNotifier {
  var _appColor = 5;

  final List<MaterialColor> _colors = [
    Colors.red,
    Colors.pink,
    Colors.purple,
    Colors.deepPurple,
    Colors.indigo,
    Colors.blue,
    Colors.lightBlue,
    Colors.cyan,
    Colors.teal,
    Colors.green,
    Colors.lightGreen,
    Colors.lime,
    Colors.yellow,
    Colors.amber,
    Colors.orange,
    Colors.deepOrange,
    Colors.brown,
  ];

  ThemeViewModel();

  get appColor {
    return _colors[_appColor];
  }

  get availableColors {
    return _colors;
  }

  get selectedColor {
    return _appColor;
  }

  set setAppColor(int value) {
    _appColor = value;
    notifyListeners();
  }
}
