


import 'app_localizations.dart';

/// The translations for French (`fr`).
class AppLocalizationsFr extends AppLocalizations {
  AppLocalizationsFr([String locale = 'fr']) : super(locale);

  @override
  String get splitter => ':';

  @override
  String get menu_items => 'Inscriptions:Factures:Contacts:Eleves:Professeurs:Options';

  @override
  String get btn_return_text => 'Retour';

  @override
  String get btn_cancel_txt => 'Annuler';

  @override
  String get btn_insert_txt => 'Insérer';

  @override
  String get btn_update_txt => 'Modifier';

  @override
  String get btn_delete_txt => 'Supprimer';

  @override
  String get btn_next_text => 'Suivant';

  @override
  String get title => 'Ecomu Gestion';

  @override
  String get title_list => 'Résultat(s) récent(s)';

  @override
  String get title_list_short => 'Récent(s)';

  @override
  String get use_existing_person => 'Utiliser une personne existante';

  @override
  String get match_text => 'Aucun résultat a été trouvé';

  @override
  String get empty_recent_list => 'Vider les recherches récentes';

  @override
  String get student_empty_text_1 => 'Le système ne possède pas d\'élèves.';

  @override
  String get student_empty_text_2 => 'Veuillez insérer un élève avec une inscription !';

  @override
  String get student_empty_btn_text => 'Insérer une nouvelle inscription';

  @override
  String get student_recent_text => 'Aucun élève récemment recherché';

  @override
  String get student_search_text => 'Rechercher un élève';

  @override
  String get student_empty_details => 'Veuillez sélectionner un élève';

  @override
  String get student_title_info_student => 'Information de l\'élève';

  @override
  String get student_title_info_legRep => 'Information du représentant légal';

  @override
  String get student_title_info_billing => 'Information du payeur';

  @override
  String get student_nom => 'Nom :';

  @override
  String get student_prenom => 'Prenom :';

  @override
  String get student_gender => 'Genre :';

  @override
  String get student_adresse => 'Adresse :';

  @override
  String get student_birthday => 'Birthday :';

  @override
  String get student_location => 'Domicile :';

  @override
  String get student_tel_prive => 'Tél. privé :';

  @override
  String get student_tel_prof => 'Tél. professionel :';

  @override
  String get student_email => 'Email :';

  @override
  String get person_btn_new_text => 'Nouvelle personne';

  @override
  String get person_search_text => 'Rechercher une personne';

  @override
  String get person_recent_text => 'Aucun contact récemment recherché';

  @override
  String get date_picker_text => 'Date de naissance';

  @override
  String get person_genders => 'Homme:Femme:Non genré';

  @override
  String get person_titles => 'Monsieur:Madame:Aucun';

  @override
  String get person_attributes_list => 'Date de naissance:Email:Genre:Domicile::Tél. privé:Tél. professionel:';

  @override
  String get person_attributes_short_list => 'Genre:Domicile::Tél. privé:Tél. professionel:';

  @override
  String get person_empty_details => 'Veuillez sélectionner un contact';

  @override
  String get form_person_insert_title => 'Saisir un nouveau contact';

  @override
  String get form_person_modify_title => 'Modifier le contact suivant';

  @override
  String get person_modify_success => 'Le contact a été modifié';

  @override
  String get person_modify_failed => 'Erreur: Le contact ne peut pas être modifié';

  @override
  String get person_delete_success => 'Le contact a été supprimé';

  @override
  String get person_delete_failed => 'Erreur: Le contact ne peut pas être supprimé car il est lié à une inscription';

  @override
  String get form_user_title => 'Titre';

  @override
  String get form_user_lastname => 'Nom';

  @override
  String get form_user_firstname => 'Prénom';

  @override
  String get form_user_email => 'Adresse email';

  @override
  String get form_user_gender => 'Genre';

  @override
  String get form_user_birthday => 'Date de naissance';

  @override
  String get form_user_address => 'Adresse du domicile';

  @override
  String get form_user_zipcode => 'Code Postal';

  @override
  String get form_user_location => 'Localite';

  @override
  String get form_user_profnumber => 'Téléphone Professionel';

  @override
  String get form_user_privnumber => 'Téléphone Privé';

  @override
  String get form_person_dropdown_placeholder => 'Choisissez';

  @override
  String get form_user_student_success => 'L\'élève a été ajouté !';

  @override
  String get form_person_add_success => 'Le contact a été ajouté !';

  @override
  String get form_person_add_error => 'Error: Le contact n\'a pas été ajouté !';

  @override
  String get inscription_btn_new_txt => 'Nouvelle inscription';

  @override
  String get inscription_steps => 'Eleve:Parent:Payeur:Cours:Valider';

  @override
  String get inscription_title_student_form => 'Saisir un nouvel élève';

  @override
  String get inscription_title_repleg_form => 'Saisir un nouveau représentant légal';

  @override
  String get inscription_title_billing_form => 'Saisir un nouveau payeur';

  @override
  String get form_inscription_cours_instrument => 'Instrument';

  @override
  String get form_inscription_cours_length => 'Durée';

  @override
  String get form_inscription_cours_section => 'Section';

  @override
  String get form_inscription_cours_day => 'Jour de cours';

  @override
  String get form_inscription_cours_start_date => 'Date de début';

  @override
  String get form_inscription_cours_hours => 'Heure du cours';

  @override
  String get teacher_search_text => 'Rechercher un professeur';

  @override
  String get teacher_short_title_list => 'Professeurs';

  @override
  String get teacher_title_list => 'Liste des professeurs';

  @override
  String get teacher_btn_new_text => 'Nouveau professeur';

  @override
  String get teacher_empty_details => 'Veuillez sélectionner un professeur';

  @override
  String get teacher_attributes_list => 'Taux engagement:Salaire:Date de naissance:Email:Genre:Domicile::Tél. privé:Tél. professionel';

  @override
  String get teacher_attributes_short_list => 'Taux engagement:Salaire:Genre:Domicile::Tél. privé:Tél. professionel';

  @override
  String get form_teacher_modify_title => 'Modifier le professeur suivant';

  @override
  String get form_teacher_salary => 'Salaire';

  @override
  String get form_teacher_working_rate => 'Taux d\'engagement';

  @override
  String get teacher_add_success => 'Le professeur a été ajouté';

  @override
  String get teacher_add_failed => 'Erreur: Le professeur ne peut pas être ajouté';

  @override
  String get teacher_modify_success => 'Le professeur a été modifié';

  @override
  String get teacher_modify_failed => 'Erreur: Le professeur ne peut pas être modifié';

  @override
  String get teacher_delete_success => 'Le professeur a été supprimé';

  @override
  String get teacher_delete_failed => 'Erreur: Le professeur ne peut pas être supprimé';

  @override
  String get teacher_insert_title => 'Saisir un nouveau professeur';

  @override
  String get params_inscriptions => 'Sections:Type de cours:Cours:Instruments:Ateliers:Cours collectifs:Durées des cours';

  @override
  String get other_instrument_insert_title => 'Insérer un instrument';

  @override
  String get other_instrument_insert_hintText => 'Nom de l\'instrument';

  @override
  String get other_instrument_insert_labelText => 'Insérer un instrument';

  @override
  String get other_instrument_title => 'Liste des Instruments';

  @override
  String get other_atelier_insert_title => 'Insérer un atelier';

  @override
  String get other_atelier_insert_hintText => 'Nom de l\'atelier';

  @override
  String get other_atelier_insert_labelText => 'Insérer un atelier';

  @override
  String get other_atelier_title => 'Liste des ateliers';

  @override
  String get other_cours_collectifs_insert_title => 'Insérer un cours collectif';

  @override
  String get other_cours_collectifs_insert_hintText => 'Nom du cours collectif';

  @override
  String get other_cours_collectifs_insert_labelText => 'Insérer un cours collectif';

  @override
  String get other_cours_collectifs_title => 'Liste des cours collectifs';

  @override
  String get other_cours_insert_title => 'Insérer un cours';

  @override
  String get other_cours_title => 'Liste des cours';

  @override
  String get other_type_cours_insert_title => 'Insérer un type de cours';

  @override
  String get other_type_cours_insert_hintText => 'Nom du type de cours';

  @override
  String get other_type_cours_insert_labelText => 'Insérer un type de cours';

  @override
  String get other_type_cours_title => 'Liste des type de cours';

  @override
  String get other_duree_insert_title => 'Insérer une nouvelle durée de cours';

  @override
  String get other_duree_insert_hintText => 'Durée du cours';

  @override
  String get other_duree_insert_labelText => 'Insérer une durée de cours';

  @override
  String get other_duree_title => 'Liste des durées d\'un cours';

  @override
  String get other_section_insert_title => 'Insérer une nouvelle section';

  @override
  String get other_section_insert_hintText => 'Nom de la section';

  @override
  String get other_section_insert_labelText => 'Section';

  @override
  String get other_section_title => 'Liste des sections d\'un cours';

  @override
  String get other_color_title => 'Couleurs de l\'application';

  @override
  String get other_checkbox_instrumental_txt => 'Prix réduit avec un cours instrumental';

  @override
  String get other_new_section => 'Nouvelle section';

  @override
  String get other_new_type_cours => 'Nouveau type de cours';

  @override
  String get other_new_cours => 'Nouveau cours';

  @override
  String get other_new_instrument => 'Nouvel instrument';

  @override
  String get other_new_workshop => 'Nouvel atelier';

  @override
  String get other_new_cours_collectif => 'Nouveau cours collectif';

  @override
  String get other_new_duration => 'Nouvelle durée';

  @override
  String get other_title_params => 'Options';

  @override
  String get login_username_text => 'Nom d\'utilisateur';

  @override
  String get login_username_hint_text => 'Entre un nom d\'utilisateur';

  @override
  String get login_password_text => 'Mot de passe';

  @override
  String get login_password_hint_text => 'Entre un mot de passe';

  @override
  String get login_btn_connect => 'Se connecter';

  @override
  String get login_error => 'Nom d\'utilisateur et/ou mot de passe incorrect';

  @override
  String get bill_btn_new_txt => 'Nouvelle facture';
}
