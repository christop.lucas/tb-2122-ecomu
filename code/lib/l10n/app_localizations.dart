
import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'app_localizations_fr.dart';

/// Callers can lookup localized strings with an instance of AppLocalizations returned
/// by `AppLocalizations.of(context)`.
///
/// Applications need to include `AppLocalizations.delegate()` in their app's
/// localizationDelegates list, and the locales they support in the app's
/// supportedLocales list. For example:
///
/// ```
/// import 'l10n/app_localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: AppLocalizations.localizationsDelegates,
///   supportedLocales: AppLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the AppLocalizations.supportedLocales
/// property.
abstract class AppLocalizations {
  AppLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('fr')
  ];

  /// No description provided for @splitter.
  ///
  /// In fr, this message translates to:
  /// **':'**
  String get splitter;

  /// No description provided for @menu_items.
  ///
  /// In fr, this message translates to:
  /// **'Inscriptions:Factures:Contacts:Eleves:Professeurs:Options'**
  String get menu_items;

  /// No description provided for @btn_return_text.
  ///
  /// In fr, this message translates to:
  /// **'Retour'**
  String get btn_return_text;

  /// No description provided for @btn_cancel_txt.
  ///
  /// In fr, this message translates to:
  /// **'Annuler'**
  String get btn_cancel_txt;

  /// No description provided for @btn_insert_txt.
  ///
  /// In fr, this message translates to:
  /// **'Insérer'**
  String get btn_insert_txt;

  /// No description provided for @btn_update_txt.
  ///
  /// In fr, this message translates to:
  /// **'Modifier'**
  String get btn_update_txt;

  /// No description provided for @btn_delete_txt.
  ///
  /// In fr, this message translates to:
  /// **'Supprimer'**
  String get btn_delete_txt;

  /// No description provided for @btn_next_text.
  ///
  /// In fr, this message translates to:
  /// **'Suivant'**
  String get btn_next_text;

  /// No description provided for @title.
  ///
  /// In fr, this message translates to:
  /// **'Ecomu Gestion'**
  String get title;

  /// No description provided for @title_list.
  ///
  /// In fr, this message translates to:
  /// **'Résultat(s) récent(s)'**
  String get title_list;

  /// No description provided for @title_list_short.
  ///
  /// In fr, this message translates to:
  /// **'Récent(s)'**
  String get title_list_short;

  /// No description provided for @use_existing_person.
  ///
  /// In fr, this message translates to:
  /// **'Utiliser une personne existante'**
  String get use_existing_person;

  /// No description provided for @match_text.
  ///
  /// In fr, this message translates to:
  /// **'Aucun résultat a été trouvé'**
  String get match_text;

  /// No description provided for @empty_recent_list.
  ///
  /// In fr, this message translates to:
  /// **'Vider les recherches récentes'**
  String get empty_recent_list;

  /// No description provided for @student_empty_text_1.
  ///
  /// In fr, this message translates to:
  /// **'Le système ne possède pas d\'élèves.'**
  String get student_empty_text_1;

  /// No description provided for @student_empty_text_2.
  ///
  /// In fr, this message translates to:
  /// **'Veuillez insérer un élève avec une inscription !'**
  String get student_empty_text_2;

  /// No description provided for @student_empty_btn_text.
  ///
  /// In fr, this message translates to:
  /// **'Insérer une nouvelle inscription'**
  String get student_empty_btn_text;

  /// No description provided for @student_recent_text.
  ///
  /// In fr, this message translates to:
  /// **'Aucun élève récemment recherché'**
  String get student_recent_text;

  /// No description provided for @student_search_text.
  ///
  /// In fr, this message translates to:
  /// **'Rechercher un élève'**
  String get student_search_text;

  /// No description provided for @student_empty_details.
  ///
  /// In fr, this message translates to:
  /// **'Veuillez sélectionner un élève'**
  String get student_empty_details;

  /// No description provided for @student_title_info_student.
  ///
  /// In fr, this message translates to:
  /// **'Information de l\'élève'**
  String get student_title_info_student;

  /// No description provided for @student_title_info_legRep.
  ///
  /// In fr, this message translates to:
  /// **'Information du représentant légal'**
  String get student_title_info_legRep;

  /// No description provided for @student_title_info_billing.
  ///
  /// In fr, this message translates to:
  /// **'Information du payeur'**
  String get student_title_info_billing;

  /// No description provided for @student_nom.
  ///
  /// In fr, this message translates to:
  /// **'Nom :'**
  String get student_nom;

  /// No description provided for @student_prenom.
  ///
  /// In fr, this message translates to:
  /// **'Prenom :'**
  String get student_prenom;

  /// No description provided for @student_gender.
  ///
  /// In fr, this message translates to:
  /// **'Genre :'**
  String get student_gender;

  /// No description provided for @student_adresse.
  ///
  /// In fr, this message translates to:
  /// **'Adresse :'**
  String get student_adresse;

  /// No description provided for @student_birthday.
  ///
  /// In fr, this message translates to:
  /// **'Birthday :'**
  String get student_birthday;

  /// No description provided for @student_location.
  ///
  /// In fr, this message translates to:
  /// **'Domicile :'**
  String get student_location;

  /// No description provided for @student_tel_prive.
  ///
  /// In fr, this message translates to:
  /// **'Tél. privé :'**
  String get student_tel_prive;

  /// No description provided for @student_tel_prof.
  ///
  /// In fr, this message translates to:
  /// **'Tél. professionel :'**
  String get student_tel_prof;

  /// No description provided for @student_email.
  ///
  /// In fr, this message translates to:
  /// **'Email :'**
  String get student_email;

  /// No description provided for @person_btn_new_text.
  ///
  /// In fr, this message translates to:
  /// **'Nouvelle personne'**
  String get person_btn_new_text;

  /// No description provided for @person_search_text.
  ///
  /// In fr, this message translates to:
  /// **'Rechercher une personne'**
  String get person_search_text;

  /// No description provided for @person_recent_text.
  ///
  /// In fr, this message translates to:
  /// **'Aucun contact récemment recherché'**
  String get person_recent_text;

  /// No description provided for @date_picker_text.
  ///
  /// In fr, this message translates to:
  /// **'Date de naissance'**
  String get date_picker_text;

  /// No description provided for @person_genders.
  ///
  /// In fr, this message translates to:
  /// **'Homme:Femme:Non genré'**
  String get person_genders;

  /// No description provided for @person_titles.
  ///
  /// In fr, this message translates to:
  /// **'Monsieur:Madame:Aucun'**
  String get person_titles;

  /// No description provided for @person_attributes_list.
  ///
  /// In fr, this message translates to:
  /// **'Date de naissance:Email:Genre:Domicile::Tél. privé:Tél. professionel:'**
  String get person_attributes_list;

  /// No description provided for @person_attributes_short_list.
  ///
  /// In fr, this message translates to:
  /// **'Genre:Domicile::Tél. privé:Tél. professionel:'**
  String get person_attributes_short_list;

  /// No description provided for @person_empty_details.
  ///
  /// In fr, this message translates to:
  /// **'Veuillez sélectionner un contact'**
  String get person_empty_details;

  /// No description provided for @form_person_insert_title.
  ///
  /// In fr, this message translates to:
  /// **'Saisir un nouveau contact'**
  String get form_person_insert_title;

  /// No description provided for @form_person_modify_title.
  ///
  /// In fr, this message translates to:
  /// **'Modifier le contact suivant'**
  String get form_person_modify_title;

  /// No description provided for @person_modify_success.
  ///
  /// In fr, this message translates to:
  /// **'Le contact a été modifié'**
  String get person_modify_success;

  /// No description provided for @person_modify_failed.
  ///
  /// In fr, this message translates to:
  /// **'Erreur: Le contact ne peut pas être modifié'**
  String get person_modify_failed;

  /// No description provided for @person_delete_success.
  ///
  /// In fr, this message translates to:
  /// **'Le contact a été supprimé'**
  String get person_delete_success;

  /// No description provided for @person_delete_failed.
  ///
  /// In fr, this message translates to:
  /// **'Erreur: Le contact ne peut pas être supprimé car il est lié à une inscription'**
  String get person_delete_failed;

  /// No description provided for @form_user_title.
  ///
  /// In fr, this message translates to:
  /// **'Titre'**
  String get form_user_title;

  /// No description provided for @form_user_lastname.
  ///
  /// In fr, this message translates to:
  /// **'Nom'**
  String get form_user_lastname;

  /// No description provided for @form_user_firstname.
  ///
  /// In fr, this message translates to:
  /// **'Prénom'**
  String get form_user_firstname;

  /// No description provided for @form_user_email.
  ///
  /// In fr, this message translates to:
  /// **'Adresse email'**
  String get form_user_email;

  /// No description provided for @form_user_gender.
  ///
  /// In fr, this message translates to:
  /// **'Genre'**
  String get form_user_gender;

  /// No description provided for @form_user_birthday.
  ///
  /// In fr, this message translates to:
  /// **'Date de naissance'**
  String get form_user_birthday;

  /// No description provided for @form_user_address.
  ///
  /// In fr, this message translates to:
  /// **'Adresse du domicile'**
  String get form_user_address;

  /// No description provided for @form_user_zipcode.
  ///
  /// In fr, this message translates to:
  /// **'Code Postal'**
  String get form_user_zipcode;

  /// No description provided for @form_user_location.
  ///
  /// In fr, this message translates to:
  /// **'Localite'**
  String get form_user_location;

  /// No description provided for @form_user_profnumber.
  ///
  /// In fr, this message translates to:
  /// **'Téléphone Professionel'**
  String get form_user_profnumber;

  /// No description provided for @form_user_privnumber.
  ///
  /// In fr, this message translates to:
  /// **'Téléphone Privé'**
  String get form_user_privnumber;

  /// No description provided for @form_person_dropdown_placeholder.
  ///
  /// In fr, this message translates to:
  /// **'Choisissez'**
  String get form_person_dropdown_placeholder;

  /// No description provided for @form_user_student_success.
  ///
  /// In fr, this message translates to:
  /// **'L\'élève a été ajouté !'**
  String get form_user_student_success;

  /// No description provided for @form_person_add_success.
  ///
  /// In fr, this message translates to:
  /// **'Le contact a été ajouté !'**
  String get form_person_add_success;

  /// No description provided for @form_person_add_error.
  ///
  /// In fr, this message translates to:
  /// **'Error: Le contact n\'a pas été ajouté !'**
  String get form_person_add_error;

  /// No description provided for @inscription_btn_new_txt.
  ///
  /// In fr, this message translates to:
  /// **'Nouvelle inscription'**
  String get inscription_btn_new_txt;

  /// No description provided for @inscription_steps.
  ///
  /// In fr, this message translates to:
  /// **'Eleve:Parent:Payeur:Cours:Valider'**
  String get inscription_steps;

  /// No description provided for @inscription_title_student_form.
  ///
  /// In fr, this message translates to:
  /// **'Saisir un nouvel élève'**
  String get inscription_title_student_form;

  /// No description provided for @inscription_title_repleg_form.
  ///
  /// In fr, this message translates to:
  /// **'Saisir un nouveau représentant légal'**
  String get inscription_title_repleg_form;

  /// No description provided for @inscription_title_billing_form.
  ///
  /// In fr, this message translates to:
  /// **'Saisir un nouveau payeur'**
  String get inscription_title_billing_form;

  /// No description provided for @form_inscription_cours_instrument.
  ///
  /// In fr, this message translates to:
  /// **'Instrument'**
  String get form_inscription_cours_instrument;

  /// No description provided for @form_inscription_cours_length.
  ///
  /// In fr, this message translates to:
  /// **'Durée'**
  String get form_inscription_cours_length;

  /// No description provided for @form_inscription_cours_section.
  ///
  /// In fr, this message translates to:
  /// **'Section'**
  String get form_inscription_cours_section;

  /// No description provided for @form_inscription_cours_day.
  ///
  /// In fr, this message translates to:
  /// **'Jour de cours'**
  String get form_inscription_cours_day;

  /// No description provided for @form_inscription_cours_start_date.
  ///
  /// In fr, this message translates to:
  /// **'Date de début'**
  String get form_inscription_cours_start_date;

  /// No description provided for @form_inscription_cours_hours.
  ///
  /// In fr, this message translates to:
  /// **'Heure du cours'**
  String get form_inscription_cours_hours;

  /// No description provided for @teacher_search_text.
  ///
  /// In fr, this message translates to:
  /// **'Rechercher un professeur'**
  String get teacher_search_text;

  /// No description provided for @teacher_short_title_list.
  ///
  /// In fr, this message translates to:
  /// **'Professeurs'**
  String get teacher_short_title_list;

  /// No description provided for @teacher_title_list.
  ///
  /// In fr, this message translates to:
  /// **'Liste des professeurs'**
  String get teacher_title_list;

  /// No description provided for @teacher_btn_new_text.
  ///
  /// In fr, this message translates to:
  /// **'Nouveau professeur'**
  String get teacher_btn_new_text;

  /// No description provided for @teacher_empty_details.
  ///
  /// In fr, this message translates to:
  /// **'Veuillez sélectionner un professeur'**
  String get teacher_empty_details;

  /// No description provided for @teacher_attributes_list.
  ///
  /// In fr, this message translates to:
  /// **'Taux engagement:Salaire:Date de naissance:Email:Genre:Domicile::Tél. privé:Tél. professionel'**
  String get teacher_attributes_list;

  /// No description provided for @teacher_attributes_short_list.
  ///
  /// In fr, this message translates to:
  /// **'Taux engagement:Salaire:Genre:Domicile::Tél. privé:Tél. professionel'**
  String get teacher_attributes_short_list;

  /// No description provided for @form_teacher_modify_title.
  ///
  /// In fr, this message translates to:
  /// **'Modifier le professeur suivant'**
  String get form_teacher_modify_title;

  /// No description provided for @form_teacher_salary.
  ///
  /// In fr, this message translates to:
  /// **'Salaire'**
  String get form_teacher_salary;

  /// No description provided for @form_teacher_working_rate.
  ///
  /// In fr, this message translates to:
  /// **'Taux d\'engagement'**
  String get form_teacher_working_rate;

  /// No description provided for @teacher_add_success.
  ///
  /// In fr, this message translates to:
  /// **'Le professeur a été ajouté'**
  String get teacher_add_success;

  /// No description provided for @teacher_add_failed.
  ///
  /// In fr, this message translates to:
  /// **'Erreur: Le professeur ne peut pas être ajouté'**
  String get teacher_add_failed;

  /// No description provided for @teacher_modify_success.
  ///
  /// In fr, this message translates to:
  /// **'Le professeur a été modifié'**
  String get teacher_modify_success;

  /// No description provided for @teacher_modify_failed.
  ///
  /// In fr, this message translates to:
  /// **'Erreur: Le professeur ne peut pas être modifié'**
  String get teacher_modify_failed;

  /// No description provided for @teacher_delete_success.
  ///
  /// In fr, this message translates to:
  /// **'Le professeur a été supprimé'**
  String get teacher_delete_success;

  /// No description provided for @teacher_delete_failed.
  ///
  /// In fr, this message translates to:
  /// **'Erreur: Le professeur ne peut pas être supprimé'**
  String get teacher_delete_failed;

  /// No description provided for @teacher_insert_title.
  ///
  /// In fr, this message translates to:
  /// **'Saisir un nouveau professeur'**
  String get teacher_insert_title;

  /// No description provided for @params_inscriptions.
  ///
  /// In fr, this message translates to:
  /// **'Sections:Type de cours:Cours:Instruments:Ateliers:Cours collectifs:Durées des cours'**
  String get params_inscriptions;

  /// No description provided for @other_instrument_insert_title.
  ///
  /// In fr, this message translates to:
  /// **'Insérer un instrument'**
  String get other_instrument_insert_title;

  /// No description provided for @other_instrument_insert_hintText.
  ///
  /// In fr, this message translates to:
  /// **'Nom de l\'instrument'**
  String get other_instrument_insert_hintText;

  /// No description provided for @other_instrument_insert_labelText.
  ///
  /// In fr, this message translates to:
  /// **'Insérer un instrument'**
  String get other_instrument_insert_labelText;

  /// No description provided for @other_instrument_title.
  ///
  /// In fr, this message translates to:
  /// **'Liste des Instruments'**
  String get other_instrument_title;

  /// No description provided for @other_atelier_insert_title.
  ///
  /// In fr, this message translates to:
  /// **'Insérer un atelier'**
  String get other_atelier_insert_title;

  /// No description provided for @other_atelier_insert_hintText.
  ///
  /// In fr, this message translates to:
  /// **'Nom de l\'atelier'**
  String get other_atelier_insert_hintText;

  /// No description provided for @other_atelier_insert_labelText.
  ///
  /// In fr, this message translates to:
  /// **'Insérer un atelier'**
  String get other_atelier_insert_labelText;

  /// No description provided for @other_atelier_title.
  ///
  /// In fr, this message translates to:
  /// **'Liste des ateliers'**
  String get other_atelier_title;

  /// No description provided for @other_cours_collectifs_insert_title.
  ///
  /// In fr, this message translates to:
  /// **'Insérer un cours collectif'**
  String get other_cours_collectifs_insert_title;

  /// No description provided for @other_cours_collectifs_insert_hintText.
  ///
  /// In fr, this message translates to:
  /// **'Nom du cours collectif'**
  String get other_cours_collectifs_insert_hintText;

  /// No description provided for @other_cours_collectifs_insert_labelText.
  ///
  /// In fr, this message translates to:
  /// **'Insérer un cours collectif'**
  String get other_cours_collectifs_insert_labelText;

  /// No description provided for @other_cours_collectifs_title.
  ///
  /// In fr, this message translates to:
  /// **'Liste des cours collectifs'**
  String get other_cours_collectifs_title;

  /// No description provided for @other_cours_insert_title.
  ///
  /// In fr, this message translates to:
  /// **'Insérer un cours'**
  String get other_cours_insert_title;

  /// No description provided for @other_cours_title.
  ///
  /// In fr, this message translates to:
  /// **'Liste des cours'**
  String get other_cours_title;

  /// No description provided for @other_type_cours_insert_title.
  ///
  /// In fr, this message translates to:
  /// **'Insérer un type de cours'**
  String get other_type_cours_insert_title;

  /// No description provided for @other_type_cours_insert_hintText.
  ///
  /// In fr, this message translates to:
  /// **'Nom du type de cours'**
  String get other_type_cours_insert_hintText;

  /// No description provided for @other_type_cours_insert_labelText.
  ///
  /// In fr, this message translates to:
  /// **'Insérer un type de cours'**
  String get other_type_cours_insert_labelText;

  /// No description provided for @other_type_cours_title.
  ///
  /// In fr, this message translates to:
  /// **'Liste des type de cours'**
  String get other_type_cours_title;

  /// No description provided for @other_duree_insert_title.
  ///
  /// In fr, this message translates to:
  /// **'Insérer une nouvelle durée de cours'**
  String get other_duree_insert_title;

  /// No description provided for @other_duree_insert_hintText.
  ///
  /// In fr, this message translates to:
  /// **'Durée du cours'**
  String get other_duree_insert_hintText;

  /// No description provided for @other_duree_insert_labelText.
  ///
  /// In fr, this message translates to:
  /// **'Insérer une durée de cours'**
  String get other_duree_insert_labelText;

  /// No description provided for @other_duree_title.
  ///
  /// In fr, this message translates to:
  /// **'Liste des durées d\'un cours'**
  String get other_duree_title;

  /// No description provided for @other_section_insert_title.
  ///
  /// In fr, this message translates to:
  /// **'Insérer une nouvelle section'**
  String get other_section_insert_title;

  /// No description provided for @other_section_insert_hintText.
  ///
  /// In fr, this message translates to:
  /// **'Nom de la section'**
  String get other_section_insert_hintText;

  /// No description provided for @other_section_insert_labelText.
  ///
  /// In fr, this message translates to:
  /// **'Section'**
  String get other_section_insert_labelText;

  /// No description provided for @other_section_title.
  ///
  /// In fr, this message translates to:
  /// **'Liste des sections d\'un cours'**
  String get other_section_title;

  /// No description provided for @other_color_title.
  ///
  /// In fr, this message translates to:
  /// **'Couleurs de l\'application'**
  String get other_color_title;

  /// No description provided for @other_checkbox_instrumental_txt.
  ///
  /// In fr, this message translates to:
  /// **'Prix réduit avec un cours instrumental'**
  String get other_checkbox_instrumental_txt;

  /// No description provided for @other_new_section.
  ///
  /// In fr, this message translates to:
  /// **'Nouvelle section'**
  String get other_new_section;

  /// No description provided for @other_new_type_cours.
  ///
  /// In fr, this message translates to:
  /// **'Nouveau type de cours'**
  String get other_new_type_cours;

  /// No description provided for @other_new_cours.
  ///
  /// In fr, this message translates to:
  /// **'Nouveau cours'**
  String get other_new_cours;

  /// No description provided for @other_new_instrument.
  ///
  /// In fr, this message translates to:
  /// **'Nouvel instrument'**
  String get other_new_instrument;

  /// No description provided for @other_new_workshop.
  ///
  /// In fr, this message translates to:
  /// **'Nouvel atelier'**
  String get other_new_workshop;

  /// No description provided for @other_new_cours_collectif.
  ///
  /// In fr, this message translates to:
  /// **'Nouveau cours collectif'**
  String get other_new_cours_collectif;

  /// No description provided for @other_new_duration.
  ///
  /// In fr, this message translates to:
  /// **'Nouvelle durée'**
  String get other_new_duration;

  /// No description provided for @other_title_params.
  ///
  /// In fr, this message translates to:
  /// **'Options'**
  String get other_title_params;

  /// No description provided for @login_username_text.
  ///
  /// In fr, this message translates to:
  /// **'Nom d\'utilisateur'**
  String get login_username_text;

  /// No description provided for @login_username_hint_text.
  ///
  /// In fr, this message translates to:
  /// **'Entre un nom d\'utilisateur'**
  String get login_username_hint_text;

  /// No description provided for @login_password_text.
  ///
  /// In fr, this message translates to:
  /// **'Mot de passe'**
  String get login_password_text;

  /// No description provided for @login_password_hint_text.
  ///
  /// In fr, this message translates to:
  /// **'Entre un mot de passe'**
  String get login_password_hint_text;

  /// No description provided for @login_btn_connect.
  ///
  /// In fr, this message translates to:
  /// **'Se connecter'**
  String get login_btn_connect;

  /// No description provided for @login_error.
  ///
  /// In fr, this message translates to:
  /// **'Nom d\'utilisateur et/ou mot de passe incorrect'**
  String get login_error;

  /// No description provided for @bill_btn_new_txt.
  ///
  /// In fr, this message translates to:
  /// **'Nouvelle facture'**
  String get bill_btn_new_txt;
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(lookupAppLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['fr'].contains(locale.languageCode);

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}

AppLocalizations lookupAppLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'fr': return AppLocalizationsFr();
  }

  throw FlutterError(
    'AppLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
