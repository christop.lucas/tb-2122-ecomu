/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Main
 
  Purpose: This file contains the main function that launches the Flutter App.

  Author:  Christopher Lucas
  Date:    01.06.2022
*/
import 'package:ecomu_gestion/app/app.dart';
import 'package:ecomu_gestion/services/ecomu_api/ecomu_api.dart';
import 'package:ecomu_gestion/services/ecomu_repository/ecomu_repository.dart';
import 'package:flutter/material.dart';
import 'package:window_size/window_size.dart';

/// Entry point to "Ecomu Gestion" application.
///
/// This application will be used in the context of a music school.
/// The goal of this app is to manage students, teachers and enrolments
/// to a music school.
void main() {
  WidgetsFlutterBinding.ensureInitialized();
  // Setting Window size
  setWindowMinSize(const Size(960, 540));
  setWindowMaxSize(Size.infinite);
  // Initialising repository with database and api helper
  final repository = EcomuRepository(
    api: EcomuApi(),
  );
  // Adds the EcomuApp Widget as root of the Widget tree
  runApp(
    EcomuApp(
      repository: repository,
    ),
  );
}
