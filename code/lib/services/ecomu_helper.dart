class EcomuHelper {
  static String? simpleValidator(String? value) {
    return (value == null || value.isEmpty)
        ? 'Veuillez renseigner une valeur'
        : null;
  }

  static String? emailValidator(String? value) {
    String? result;
    if (value == null || value.isEmpty) {
      result = 'Veuillez renseigner une valeur';
    } else {
      var emailValid = RegExp(
        //r"^[A-zÀ-ú0-9.A-zÀ-ú0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+",
        r"^[A-zÀ-ú0-9.A-zÀ-ú0-9.!#$%&'*+-/=?^_`]+@[a-zA-Z0-9]+\.[a-zA-Z]+",
      ).hasMatch(value);
      if (!emailValid) {
        result = "L'email n'est pas valide";
      }
    }
    return result;
  }

  static String? numberValidator(String? value) {
    String? result;
    if (value == null || value.isEmpty) {
      result = 'Veuillez renseigner une valeur';
    } else {
      var fieldValid = RegExp(r"^[0-9]*$").hasMatch(value);
      if (!fieldValid) {
        result = "Ce champs ne contenir seulement des numéros";
      }
    }
    return result;
  }
}
