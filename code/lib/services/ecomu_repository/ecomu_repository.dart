/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Model
 
  Purpose: This file contains the http helper class. 

  Author:  Christopher Lucas
  Date:    02.06.2022
*/
import 'dart:async';

import 'package:ecomu_gestion/models/bill.dart';
import 'package:ecomu_gestion/models/course.dart';
import 'package:ecomu_gestion/models/course_day.dart';
import 'package:ecomu_gestion/models/duration.dart';
import 'package:ecomu_gestion/models/inscription.dart';
import 'package:ecomu_gestion/models/instrument.dart';
import 'package:ecomu_gestion/models/person.dart';
import 'package:ecomu_gestion/models/professor.dart';
import 'package:ecomu_gestion/models/registration.dart';
import 'package:ecomu_gestion/models/registration_add.dart';
import 'package:ecomu_gestion/models/section.dart';
import 'package:ecomu_gestion/models/student.dart';
import 'package:ecomu_gestion/models/type_course.dart';
import 'package:ecomu_gestion/models/workshop.dart';
import 'package:ecomu_gestion/services/ecomu_api/ecomu_api.dart';

/// This class is the repository of Ecomu Gestion. A repository is need
/// to manage the data of the application. It calls the different
/// helper classes to fetch and persist data.
class EcomuRepository {
  /// Http helper
  final EcomuApi _api;

  /// All persons [Person]
  var persons = <Person>[];

  /// All students
  var students = <Student>[];

  /// All students
  var professors = <Professor>[];

  /// All instruments [Instrument]
  var _instruments = <Instrument>[];

  /// All workshops [Workshop]
  var _workshops = <Workshop>[];

  /// All courses [Course]
  var _courses = <Course>[];

  /// All collective courses [Course]
  var _collectiveCourses = <Course>[];

  /// All type of courses [CourseType]
  var _courseTypes = <CourseType>[];

  /// All sections [Section]
  var _sections = <Section>[];

  /// All course durations
  var _durations = <CourseDuration>[];

  /// All course durations
  var _days = <CourseDay>[];

  /// Contructs the repository by initiliasing the database and http helper
  /// classes
  EcomuRepository({
    required EcomuApi api,
  }) : _api = api {
    //_initRepository();
  }

  void _initRepository() async {
    await fetchInstruments();
  }

  Future<List<Person>?> fetchRepLegsByStudent(int idStudent) {
    return _api.fetchLegalRepsByStudent(idStudent);
  }

  Future<List<Person>?> fetchPayersByStudent(int idStudent) {
    return _api.fetchPayersByStudent(idStudent);
  }

  Future<bool> fetchPersons() async {
    var isSuccessful = false;
    final result = await _api.fetchPersons();
    if (result != null) {
      persons = result;
      isSuccessful = true;
    }
    return isSuccessful;
  }

  Future<bool> insertPerson(Person p) async {
    var isSuccessful = false;
    final result = await _api.insertPerson(p);
    if (result != -1) {
      p.idPerson = result;
      persons.add(p);
      isSuccessful = true;
    }
    return isSuccessful;
  }

  Future<int> insertPerson2(Person p) {
    return _api.insertPerson(p);
  }

  Future<bool> updatePerson(Person old, Person p) async {
    var isSuccessful = false;
    final result = await _api.updatePerson(p);
    if (result) {
      persons.remove(old);
      persons.add(p);
      isSuccessful = true;
    }
    return isSuccessful;
  }

  Future<bool> updatePerson2(Person p) async {
    var isSuccessful = false;
    final result = await _api.updatePerson(p);
    if (result) {
      isSuccessful = await fetchPersons();
    }
    return isSuccessful;
  }

  Future<bool> deletePerson(dynamic p) async {
    var isSuccessful = false;
    final result = await _api.deletePerson(p.idPerson!);
    if (result) {
      if (p is Student) students.remove(p);
      if (p is Person) persons.remove(p);
      isSuccessful = true;
    }
    return isSuccessful;
  }

  Future<bool> fetchStudents() async {
    var isSuccessful = false;
    final result = await _api.fetchStudents();
    if (result != null) {
      students = result;
      isSuccessful = true;
    }
    return isSuccessful;
  }

  Future<bool> fetchProfessors() async {
    var isSuccessful = false;
    final result = await _api.fetchProfessors();
    if (result != null) {
      professors = result;
      isSuccessful = true;
    }
    return isSuccessful;
  }

  Future<List<Professor>> fetchProfessors2() async {
    final result = await _api.fetchProfessors();
    if (result != null) {
      return result;
    }
    return <Professor>[];
  }

  Future<List<Professor>> fetchProfessorsByInstrument(int idInstrument) async {
    final result = await _api.fetchProfessorsByInstrument(idInstrument);
    if (result != null) {
      return result;
    }
    return <Professor>[];
  }

  Future<bool> insertProfessor(
    Person p,
    double workingRate,
    double salary,
  ) async {
    if (p.idPerson == null) {
      // Add new person before adding teacher
      final resultPerson = await _api.insertPerson(p);
      if (resultPerson != -1) {
        p.idPerson = resultPerson;
      } else {
        return false;
      }
    } else {
      // Update existing person before adding teacher
      final resultPerson = await _api.updatePerson(p);
      if (!resultPerson) {
        return false;
      }
    }
    // Add new teacher
    final prof = Professor.convertFromPerson(p, salary, workingRate, null);
    final resultProf = await _api.insertProfessor(
      prof,
    );
    if (resultProf != -1) {
      prof.idProfessor = resultProf;
      professors.add(prof);
    } else {
      return false;
    }
    return true;
  }

  Future<bool> updateProfessor(
    Person p,
    double workingRate,
    double salary,
    int idProfessor,
  ) async {
    final resultPerson = await _api.updatePerson(p);
    if (resultPerson) {
      final prof =
          Professor.convertFromPerson(p, salary, workingRate, idProfessor);
      final resultProf = await _api.updateProfessor(prof);
      if (resultProf) {
        if (await fetchProfessors()) return true;
      }
    }
    return false;
  }

  Future<bool> deleteProfessor(int idProfessor) {
    return _api.deleteProfessor(idProfessor);
  }

  Future<List<Registration>?> fetchRegistrations() {
    return _api.fetchRegistrations();
  }

  Future<bool> insertRegistration(
    Person student,
    Person? legRep,
    Person? billing,
    bool sameStudent,
    RegistrationAdd registration,
  ) async {
    if (student.idPerson == null) {
      final idStudent = await insertPerson2(student);
      registration.idStudent = idStudent;
    } else {
      updatePerson2(student);
      registration.idStudent = student.idPerson;
    }
    if (legRep != null) {
      if (legRep.idPerson == null) {
        final idLegRep = await insertPerson2(legRep);
        registration.idRepLeg = idLegRep;
      } else {
        updatePerson2(legRep);
        registration.idRepLeg = legRep.idPerson;
      }
    } else {
      registration.idRepLeg = registration.idStudent;
    }
    if (billing != null) {
      if (billing.idPerson == null) {
        final idPerson = await insertPerson2(billing);
        registration.idPayer = idPerson;
      } else {
        updatePerson2(billing);
        registration.idPayer = billing.idPerson;
      }
    } else {
      if (sameStudent) {
        registration.idPayer = registration.idStudent;
      } else {
        registration.idPayer = registration.idRepLeg;
      }
    }

    _api.insertRegistration(registration);
    return true;
  }

/* --- Instruments Function ------------------------------------ */
  Future<List<Instrument>?> fetchInstruments() {
    return _api.fetchInstruments();
  }

  Future<bool> fetchInstrumentsOld() async {
    var isSuccessful = false;
    final result = await _api.fetchInstruments();
    if (result != null) {
      _instruments = result;
      isSuccessful = true;
    }
    return isSuccessful;
  }

  Future<bool> insertInstrument(String name) async {
    var isSuccessful = false;
    final result = await _api.insertInstrument(name);
    if (result != -1) {
      _instruments.add(Instrument(idInstrument: result, name: name));
      isSuccessful = true;
    }
    return isSuccessful;
  }

  List<Instrument> get instruments {
    return _instruments;
  }

  Future<bool> deleteInstrument(Instrument instrument) async {
    var isSuccessful = false;
    final result = await _api.deleteInstrument(instrument.idInstrument);
    if (result) {
      _instruments.remove(instrument);
      isSuccessful = true;
    }
    return isSuccessful;
  }

  /* --- Workshop Function ------------------------------------ */
  Future<List<Workshop>?> fetchWorkshops() {
    return _api.fetchWorkshops();
  }

  Future<bool> fetchWorkshopsOld() async {
    var isSuccessful = false;
    final result = await _api.fetchWorkshops();
    if (result != null) {
      _workshops = result;
      isSuccessful = true;
    }
    return isSuccessful;
  }

  Future<bool> insertWorkshop(String name) async {
    var isSuccessful = false;
    final result = await _api.insertWorkshop(name);
    if (result != -1) {
      _workshops.add(Workshop(idWorkshop: result, workshop: name));
      isSuccessful = true;
    }
    return isSuccessful;
  }

  List<Workshop> get workshops {
    return _workshops;
  }

  Future<bool> deleteWorkshop(Workshop workshop) async {
    var isSuccessful = false;
    final result = await _api.deleteWorkshop(workshop.idWorkshop);
    if (result) {
      _workshops.remove(workshop);
      isSuccessful = true;
    }
    return isSuccessful;
  }

  /* --- Courses Function ---------------------------------------------- */

  Future<List<Course>?> fetchCourses() {
    return _api.fetchCourses();
  }

  Future<bool> fetchCoursesOld() async {
    var isSuccessful = false;
    final result = await _api.fetchCourses();
    if (result != null) {
      _courses = result;
      isSuccessful = true;
    }
    return isSuccessful;
  }

  List<Course> get courses {
    return _courses;
  }

  Future<bool> insertCourse(Course course) async {
    var isSuccessful = false;
    final result = await _api.insertCourse(course);
    if (result != -1) {
      course.idCourse = result;
      _courses.add(course);
      isSuccessful = true;
    }
    return isSuccessful;
  }

  Future<bool> deleteCourse(Course course) async {
    var isSuccessful = false;
    final result = await _api.deleteCourse(course.idCourse!);
    if (result) {
      _courses.remove(course);
      isSuccessful = true;
    }
    return isSuccessful;
  }

  Future<List<Course>?> fetchCollectiveCourses() {
    return _api.fetchCollectiveCourses();
  }

  Future<bool> insertCollectiveCourse(Course course) async {
    var isSuccessful = false;
    final result = await _api.insertCollectiveCourse(course);
    if (result != -1) {
      course.idCourse = result;
      // _collectiveCourses.add(course);
      isSuccessful = true;
    }
    return isSuccessful;
  }

  List<Course> get collectiveCourses {
    return _collectiveCourses;
  }

  /* --- Duration Function -------------------------------------------- */
  Future<List<CourseDuration>?> fetchDurations() {
    return _api.fetchDurations();
  }

  Future<bool> fetchDurationsOld() async {
    var isSuccessful = false;
    final result = await _api.fetchDurations();
    if (result != null) {
      _durations = result;
      isSuccessful = true;
    }
    return isSuccessful;
  }

  Future<bool> insertDuration(int duration) async {
    var isSuccessful = false;
    final result = await _api.insertDuration(duration);
    if (result) {
      // _durations.add(duration);
      isSuccessful = true;
    }
    return isSuccessful;
  }

  List<CourseDuration> get durations {
    return _durations;
  }

  Future<bool> deleteDuration(CourseDuration duration) async {
    var isSuccessful = false;
    final result = await _api.deleteDuration(duration.duration);
    if (result) {
      _durations.remove(duration);
      isSuccessful = true;
    }
    return isSuccessful;
  }

  /* --- Secitons Function ------------------------------------ */
  Future<List<Section>?> fetchSections() {
    return _api.fetchSections();
  }

  Future<bool> fetchSectionsOld() async {
    var isSuccessful = false;
    final result = await _api.fetchSections();
    if (result != null) {
      _sections = result;
      isSuccessful = true;
    }
    return isSuccessful;
  }

  Future<bool> insertSection(String name) async {
    var isSuccessful = false;
    final result = await _api.insertSection(name);
    if (result != -1) {
      _sections.add(Section(idSection: result, name: name));
      isSuccessful = true;
    }
    return isSuccessful;
  }

  List<Section> get sections {
    return _sections;
  }

  Future<bool> deleteSection(Section s) async {
    var isSuccessful = false;
    final result = await _api.deleteSection(s.idSection);
    if (result) {
      _sections.remove(s);
      isSuccessful = true;
    }
    return isSuccessful;
  }

  /* --- Course Type Function ------------------------------------ */
  Future<List<CourseType>?> fetchCourseTypes() {
    return _api.fetchCourseTypes();
  }

  Future<bool> fetchCourseTypesOld() async {
    var isSuccessful = false;
    final result = await _api.fetchCourseTypes();
    if (result != null) {
      _courseTypes = result;
      isSuccessful = true;
    }
    return isSuccessful;
  }

  Future<bool> insertCourseType(String name, bool hasInstrumental) async {
    var isSuccessful = false;
    var type = CourseType(
      idCourseType: -1,
      name: name,
      hasInstrumental: hasInstrumental,
    );
    final result = await _api.insertCourseType(type);
    if (result != -1) {
      type.idCourseType = result;
      _courseTypes.add(type);
      isSuccessful = true;
    }
    return isSuccessful;
  }

  List<CourseType> get courseTypes {
    return _courseTypes;
  }

  Future<bool> deleteCourseType(CourseType type) async {
    var isSuccessful = false;
    final result = await _api.deleteCourseType(type.idCourseType);
    if (result) {
      isSuccessful = true;
    }
    return isSuccessful;
  }

  /* --- Days Function ------------------------------------ */
  Future<bool> fetchDays() async {
    var isSuccessful = false;
    final result = await _api.fetchDays();
    if (result != null) {
      _days = result;
      isSuccessful = true;
    }
    return isSuccessful;
  }

  List<CourseDay> get days {
    return _days;
  }

  /* --- Bills Function ------------------------------------ */
  Future<List<Bill>> fetchBills() async {
    final result = await _api.fetchBills();
    if (result != null) {
      return result;
    }
    return <Bill>[];
  }

  bool signIn(String username, String password) {
    return username == "test" && password == "test";
  }
}
