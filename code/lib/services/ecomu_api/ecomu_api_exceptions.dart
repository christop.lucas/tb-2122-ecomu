/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Exceptions
 
  Purpose: This file contains the excepetions classes of Ecomu Gestion 

  Author:  Christopher Lucas
  Date:    02.06.2022
*/
class RequestFailure implements Exception {
  final cause = "Exception: serveur is inaccessible !";
  RequestFailure();
}

class PersonFailure implements Exception {
  final cause = "Exception: person exists !";
  PersonFailure();
}

class UnauthorizedError implements Exception {
  final String cause;
  UnauthorizedError({required this.cause});
}
