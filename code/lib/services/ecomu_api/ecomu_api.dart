/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Main
 
  Purpose: This file contains the http helper class. 

  Author:  Christopher Lucas
  Date:    01.06.2022
*/
import 'dart:convert';

import 'package:ecomu_gestion/models/bill.dart';
import 'package:ecomu_gestion/models/billing_person.dart';
import 'package:ecomu_gestion/models/course.dart';
import 'package:ecomu_gestion/models/course_day.dart';
import 'package:ecomu_gestion/models/duration.dart';
import 'package:ecomu_gestion/models/inscription.dart';
import 'package:ecomu_gestion/models/instrument.dart';
import 'package:ecomu_gestion/models/person.dart';
import 'package:ecomu_gestion/models/professor.dart';
import 'package:ecomu_gestion/models/registration.dart';
import 'package:ecomu_gestion/models/registration_add.dart';
import 'package:ecomu_gestion/models/rep_leg.dart';
import 'package:ecomu_gestion/models/section.dart';
import 'package:ecomu_gestion/models/student.dart';
import 'package:ecomu_gestion/models/type_course.dart';
import 'package:ecomu_gestion/models/workshop.dart';
import 'package:ecomu_gestion/services/ecomu_api/ecomu_api_exceptions.dart';
import 'package:http/http.dart' as http;

/// Handles http request to and from the server.
///
/// EcomuApi transform the incoming JSON into the correct object or list
/// of objects and the outgoing objects or list of objects into JSON.
class EcomuApi {
  /// HTTP Client to handle the requests
  final _httpClient = http.Client();

  /// Serveur base URL
  final _serverUrl = "https://ecomu.cfy.ch";

  /// API Rest route to [Person] data
  final _persons = "/persons";

  /// API Rest route to [Instrument] data
  final _instruments = "/instruments";

  /// API Rest route to [Student] data
  final _students = "/students";

  /// API Rest route to [Professeur] data
  final _professeurs = "/professors";

  /// API Rest route to [Inscription] data
  final _registrations = "/registrations";

  /// API Rest route to [LegRep] data
  final _legReps = "/legalreps";

  /// API Rest route to [Person] data
  final _payers = "/payers";

  /// API Rest route to [Workshop] data
  final _workshops = "/workshops";

  /// API Rest route to Duration data
  final _durations = "/durations";

  /// API Rest route to [Section] data
  final _sections = "/sections";

  /// API Rest route to [Course] data
  final _courses = "/courses";

  final _types = "/types";

  final _collectives = "/collectives";

  final _individuals = "/individuals";

  final _days = "/days";

  // routes of the differentes operations
  /// Route to CRUD operation Create
  final _add = "/add";

  /// Route to CRUD operation Update
  final _edit = "/edit";

  /// Route to CRUD operation Delete
  final _delete = "/delete";

  /// Route to
  final _details = "/details";

  final _bills = "/bills";

  final _journal = "/journal";

  final _reminders = "/reminders";

  /// Response message
  final _message = "message";

  /// Header information
  final _headerInfo = <String, String>{
    'Content-Type': 'application/json; charset=UTF-8',
  };
  // Constructor
  EcomuApi();
  // Error code handler
  static void checkStatusCode(int statusCode) {
    switch (statusCode) {
      case 400:
        throw PersonFailure();
      case 404:
        throw RequestFailure();
      case 401:
        throw UnauthorizedError(cause: "test");
      default:
      // Do nothing
    }
  }

  /// Fetchs all the persons in the external database using a GET request.
  ///
  /// Returns a list of persons if successful. Otherwise, returns null.
  Future<List<Person>?> fetchLegalRepsByStudent(int idStudent) async {
    final url = Uri.parse("$_serverUrl$_legReps/$idStudent");
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      // Return a list of Persons this list can be empty
      return dataJson.map((e) => Person.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  /// Fetchs all the persons in the external database using a GET request.
  ///
  /// Returns a list of persons if successful. Otherwise, returns null.
  Future<List<Person>?> fetchPayersByStudent(int idStudent) async {
    final url = Uri.parse("$_serverUrl$_payers/$idStudent");
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      // Return a list of Persons this list can be empty
      return dataJson.map((e) => Person.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  /* --- HTTP request for persons --------------------------------------- */

  /// Fetchs all the persons in the external database using a GET request.
  ///
  /// Returns a list of persons if successful. Otherwise, returns null.
  Future<List<Person>?> fetchPersons() async {
    final url = Uri.parse(_serverUrl + _persons);
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      if (dataResponse.statusCode != 200) {
        throw RequestFailure();
      }
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      // Return a list of Persons this list can be empty
      return dataJson.map((e) => Person.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  /// Inserts [person] in the external database using a POST request
  ///
  /// Returns the idPerson if the person was added succesfuly. Otherwise, -1.
  Future<int> insertPerson(Person person) async {
    final url = Uri.parse(_serverUrl + _persons + _add);
    try {
      // execute request
      final dataResponse = await _httpClient.post(
        url,
        body: jsonEncode(person),
        headers: _headerInfo,
      );
      print(dataResponse.body);
      // can throw an exception if status code invalid
      checkStatusCode(dataResponse.statusCode);
      // The location of the idPerson
      return jsonDecode(dataResponse.body)[_message];
    } catch (e) {
      print(e);
      return -1;
    }
  }

  /// Updates a person [person] in the external database using a POST request
  ///
  /// Returns  if the person was updated succesfuly. Otherwise, -1.
  Future<bool> updatePerson(Person person) async {
    final url = Uri.parse("$_serverUrl$_persons/${person.idPerson}$_edit");
    try {
      final dataResponse = await _httpClient.post(
        url,
        body: jsonEncode(person),
        headers: _headerInfo,
      );
      print(dataResponse.body);

      checkStatusCode(dataResponse.statusCode);
      return true;
    } catch (e) {
      print(e);

      return false;
    }
  }

  /// Updates a person [person] in the external database using a POST request
  ///
  /// Returns  if the person was updated succesfuly. Otherwise, -1.
  Future<bool> deletePerson(int idPerson) async {
    final url = Uri.parse("$_serverUrl$_persons/$idPerson$_delete");
    try {
      final dataResponse = await _httpClient.delete(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      return true;
    } catch (e) {
      return false;
    }
  }

  /* --- HTTP request for instruments ----------------------------------- */

  /// Fetchs all the persons in the external database using GET request. The
  ///
  /// Returns a list of persons if successful. Otherwise, returns null.
  Future<List<Instrument>?> fetchInstruments() async {
    final url = Uri.parse(_serverUrl + _instruments);
    try {
      final dataResponse = await _httpClient.get(
        url,
      );
      if (dataResponse.statusCode != 200) {
        throw RequestFailure();
      }
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      // Return a list of Persons this list can be empty
      return dataJson.map((e) => Instrument.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  /// Deletes a [Instrument] in the external database using a DELETE request
  ///
  /// Returns true if the instrument was updated succesfuly. Otherwise, false.
  Future<bool> deleteInstrument(int idInstrument) async {
    final url = Uri.parse("$_serverUrl$_instruments/$idInstrument$_delete");
    try {
      final dataResponse = await _httpClient.delete(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)['message'] == 'OK';
    } catch (e) {
      return false;
    }
  }

  /// Inserts a instrument in the external database using a POST request
  ///
  /// Returns the idPerson if the person was added succesfuly. Otherwise, -1.
  Future<int> insertInstrument(String name) async {
    final url = Uri.parse(_serverUrl + _instruments + _add);
    try {
      final dataResponse = await _httpClient.post(
        url,
        body: jsonEncode({'name': name}),
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)["message"];
    } catch (e) {
      return -1;
    }
  }

  /* --- HTTP request for students --------------------------------------- */

  /// Fetchs all the inscriptions in the external database using GET request. The
  ///
  /// Returns a list of inscriptions if successful. Otherwise, returns null.
  Future<List<Registration>?> fetchRegistrations() async {
    final url = Uri.parse(_serverUrl + _registrations);
    try {
      final dataResponse = await _httpClient.get(
        url,
      );
      if (dataResponse.statusCode != 200) {
        throw RequestFailure();
      }
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      // Return a list of Students this list can be empty
      return dataJson.map((e) => Registration.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  /// Fetchs all the inscriptions in the external database using GET request. The
  ///
  /// Returns a list of inscriptions if successful. Otherwise, returns null.
  Future<List<Registration>?> fetchRegistration(int idRegistration) async {
    final url =
        Uri.parse("$_serverUrl$_registrations/$idRegistration$_details");
    try {
      final dataResponse = await _httpClient.get(
        url,
      );
      if (dataResponse.statusCode != 200) {
        throw RequestFailure();
      }
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      // Return a list of Students this list can be empty
      return dataJson.map((e) => Registration.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  /// Inserts a registration in the external database using a POST request
  ///
  /// Returns the idPerson if the person was added succesfuly. Otherwise, -1.
  Future<int> insertRegistration(RegistrationAdd i) async {
    final url = Uri.parse(_serverUrl + _registrations + _add);
    try {
      final dataResponse = await _httpClient.post(
        url,
        body: jsonEncode(i),
        headers: _headerInfo,
      );
      print(dataResponse.body);
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)["message"];
    } catch (e) {
      print(e);
      return -1;
    }
  }

  /* --- HTTP request for professeurs ----------------------------------- */

  /// Fetchs all the professeurs in the external database using GET request.
  ///
  /// Returns a list of professeurs if successful. Otherwise, returns null.
  Future<List<Professor>?> fetchProfessors() async {
    final url = Uri.parse(_serverUrl + _professeurs);
    try {
      final dataResponse = await _httpClient.get(
        url,
      );
      if (dataResponse.statusCode != 200) {
        throw RequestFailure();
      }
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      // Return a list of professeurs this list can be empty
      return dataJson.map((e) => Professor.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  Future<int> insertProfessor(Professor p) async {
    final url = Uri.parse(_serverUrl + _professeurs + _add);
    try {
      final dataResponse = await _httpClient.post(
        url,
        body: jsonEncode(p),
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)["message"];
    } catch (e) {
      return -1;
    }
  }

  /// Updates a professor [Professor] in the external database using
  /// a DELETE request.
  ///
  /// Returns true if the person was deleted succesfuly. Otherwise, false.
  Future<bool> updateProfessor(Professor p) async {
    final url = Uri.parse("$_serverUrl$_professeurs/${p.idProfessor}$_edit");
    try {
      final dataResponse = await _httpClient.post(
        url,
        headers: _headerInfo,
        body: jsonEncode(p),
      );
      checkStatusCode(dataResponse.statusCode);
      return true;
    } catch (e) {
      return false;
    }
  }

  /// Deletes a professor [Professor] in the external database using
  /// a DELETE request.
  ///
  /// Returns true if the person was deleted succesfuly. Otherwise, false.
  Future<bool> deleteProfessor(int idProfessor) async {
    final url = Uri.parse("$_serverUrl$_professeurs/$idProfessor$_delete");
    try {
      final dataResponse = await _httpClient.delete(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<List<Professor>?> fetchProfessorsByInstrument(int idInstrument) async {
    final url =
        Uri.parse("$_serverUrl$_instruments/$idInstrument$_professeurs");
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      print(dataResponse.body);
      checkStatusCode(dataResponse.statusCode);
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      // Return a list of professeurs this list can be empty
      return dataJson.map((e) => Professor.fromJson(e)).toList();
    } catch (e) {
      print(e);
      return null;
    }
  }

  /* --- HTTP request for students ----------------------------------- */

  /// Fetchs all the students in the external database using GET request.
  ///
  /// Returns a list of students if successful. Otherwise, returns null.
  Future<List<Student>?> fetchStudents() async {
    final url = Uri.parse(_serverUrl + _students);
    try {
      final dataResponse = await _httpClient.get(
        url,
      );
      if (dataResponse.statusCode != 200) {
        throw RequestFailure();
      }
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      // Return a list of professeurs this list can be empty
      return dataJson.map((e) => Student.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  /* --- HTTP request for legal represents ------------------------------- */

  /// Fetchs all the legal represents in the external database
  /// using GET request.
  ///
  /// Returns a list of legal represents if successful.
  /// Otherwise, returns null.
  Future<List<LegRep>?> fetchLegReps() async {
    final url = Uri.parse(_serverUrl + _legReps);
    try {
      final dataResponse = await _httpClient.get(
        url,
      );
      if (dataResponse.statusCode != 200) {
        throw RequestFailure();
      }
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      // Return a list of professeurs this list can be empty
      return dataJson.map((e) => LegRep.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

/* --- HTTP request for billing person ------------------------------- */
  /// Fetchs the billing persons of student using [idStudent]
  /// in the external database using GET request.
  ///
  /// Returns a list of legal represents if successful.
  /// Otherwise, returns null.
  Future<List<BillingPerson>?> fetchBillingByStudent(int idStudent) async {
    final url = Uri.parse("$_serverUrl$_legReps/$idStudent");
    try {
      final dataResponse = await _httpClient.get(
        url,
      );
      if (dataResponse.statusCode != 200) {
        throw RequestFailure();
      }
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      // Return a list of professeurs this list can be empty
      return dataJson.map((e) => BillingPerson.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  /* --- HTTP request for workshops ------------------------------- */
  Future<List<Workshop>?> fetchWorkshops() async {
    final url = Uri.parse(_serverUrl + _workshops);
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      // Return a list of professeurs this list can be empty
      return dataJson.map((e) => Workshop.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  Future<int> insertWorkshop(String name) async {
    final url = Uri.parse(_serverUrl + _workshops + _add);
    try {
      final dataResponse = await _httpClient.post(
        url,
        headers: _headerInfo,
        body: jsonEncode({'name': name}),
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)["message"];
    } catch (e) {
      return -1;
    }
  }

  /// Deletes a [Workshop] in the external database using a DELETE request
  ///
  /// Returns true if the workshop was deleted succesfuly. Otherwise, false.
  Future<bool> deleteWorkshop(int idWorkshop) async {
    final url = Uri.parse("$_serverUrl$_workshops/$idWorkshop$_delete");
    try {
      final dataResponse = await _httpClient.delete(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)['message'] == 'OK';
    } catch (e) {
      return false;
    }
  }

  /* --- HTTP request for durations ------------------------------- */
  Future<List<CourseDuration>?> fetchDurations() async {
    final url = Uri.parse(_serverUrl + _durations);
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      return dataJson.map((e) => CourseDuration.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  Future<bool> insertDuration(int duration) async {
    final url = Uri.parse(_serverUrl + _durations + _add);
    try {
      final dataResponse = await _httpClient.post(
        url,
        headers: _headerInfo,
        body: jsonEncode({'duration': duration}),
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)["status"] == "success";
    } catch (e) {
      return false;
    }
  }

  /// Deletes a [CourseDuration] in the external database using a DELETE request
  ///
  /// Returns true if the duration was deleted succesfuly. Otherwise, false.
  Future<bool> deleteDuration(int idDuration) async {
    final url = Uri.parse("$_serverUrl$_durations/$idDuration$_delete");
    try {
      final dataResponse = await _httpClient.delete(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)['message'] == 'OK';
    } catch (e) {
      return false;
    }
  }

  /* --- HTTP request for durations ------------------------------- */
  Future<List<CourseType>?> fetchCourseTypes() async {
    final url = Uri.parse(_serverUrl + _courses + _types);
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);

      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      return dataJson.map((e) => CourseType.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  Future<int> insertCourseType(CourseType type) async {
    final url = Uri.parse(_serverUrl + _courses + _types + _add);
    try {
      final dataResponse = await _httpClient.post(
        url,
        headers: _headerInfo,
        body: jsonEncode(type),
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)["message"];
    } catch (e) {
      return -1;
    }
  }

  /// Deletes a course [Course] in the external database using a DELETE request
  ///
  /// Returns true if the section was deleted succesfuly. Otherwise, false.
  Future<bool> deleteCourseType(int idCourseType) async {
    final url = Uri.parse("$_serverUrl$_courses$_types/$idCourseType$_delete");
    try {
      final dataResponse = await _httpClient.delete(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)['message'] == 'OK';
    } catch (e) {
      return false;
    }
  }

  /* --- HTTP request for courses ------------------------------- */

  Future<List<Course>?> fetchCourses() async {
    final url = Uri.parse(_serverUrl + _courses);
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      return dataJson.map((e) => Course.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  Future<int> insertCourse(Course course) async {
    final url = Uri.parse(_serverUrl + _courses + _add);
    try {
      final dataResponse = await _httpClient.post(
        url,
        headers: _headerInfo,
        body: jsonEncode(course),
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)["message"];
    } catch (e) {
      return -1;
    }
  }

  /// Deletes a course [Course] in the external database using a DELETE request
  ///
  /// Returns true if the section was deleted succesfuly. Otherwise, false.
  Future<bool> deleteCourse(int idCourse) async {
    final url = Uri.parse("$_serverUrl$_courses/$idCourse$_delete");
    try {
      final dataResponse = await _httpClient.delete(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)['message'] == 'OK';
    } catch (e) {
      return false;
    }
  }

  Future<List<Course>?> fetchIndividualCourses() async {
    final url = Uri.parse(_serverUrl + _courses + _individuals);
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      return dataJson.map((e) => Course.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  Future<List<Course>?> fetchWorkshopCourses() async {
    final url = Uri.parse(_serverUrl + _courses + _workshops);
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      return dataJson.map((e) => Course.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  Future<List<Course>?> fetchCollectiveCourses() async {
    final url = Uri.parse(_serverUrl + _courses + _collectives);
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      return dataJson.map((e) => Course.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  Future<int> insertCollectiveCourse(Course course) async {
    final url = Uri.parse(_serverUrl + _courses + _collectives + _add);
    try {
      final dataResponse = await _httpClient.post(
        url,
        headers: _headerInfo,
        body: jsonEncode(course),
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)["message"];
    } catch (e) {
      return -1;
    }
  }

  /* --- HTTP request for sections ------------------------------------ */

  Future<List<Section>?> fetchSections() async {
    final url = Uri.parse(_serverUrl + _sections);
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      return dataJson.map((e) => Section.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  Future<int> insertSection(String name) async {
    final url = Uri.parse(_serverUrl + _sections + _add);
    try {
      final dataResponse = await _httpClient.post(
        url,
        body: jsonEncode({'section': name}),
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)["message"];
    } catch (e) {
      return -1;
    }
  }

  /// Deletes a section [Section] in the external database using a DELETE request
  ///
  /// Returns  if the section was updated succesfuly. Otherwise, false.
  Future<bool> deleteSection(int idSection) async {
    final url = Uri.parse("$_serverUrl$_sections/$idSection$_delete");
    try {
      final dataResponse = await _httpClient.delete(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)['message'] == 'OK';
    } catch (e) {
      return false;
    }
  }

  /* --- HTTP request for days ------------------------------------ */

  Future<List<CourseDay>?> fetchDays() async {
    final url = Uri.parse(_serverUrl + _days);
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      checkStatusCode(dataResponse.statusCode);
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      return dataJson.map((e) => CourseDay.fromJson(e)).toList();
    } catch (e) {
      return null;
    }
  }

  /* --- HTTP request for bills ------------------------------- */
  Future<List<Bill>?> fetchBills() async {
    final url = Uri.parse(_serverUrl + _bills);
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      print(dataResponse.body);
      checkStatusCode(dataResponse.statusCode);
      final dataJson = jsonDecode(dataResponse.body) as List<dynamic>;
      return dataJson.map((e) => Bill.fromJson(e)).toList();
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<Bill?> fetchBill(int idBill) async {
    final url = Uri.parse("$_serverUrl$_bills/$idBill");
    try {
      final dataResponse = await _httpClient.get(
        url,
        headers: _headerInfo,
      );
      print(dataResponse.body);
      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<int> insertBill(Bill b) async {
    final url = Uri.parse(_serverUrl + _bills + _add);
    try {
      final dataResponse = await _httpClient.post(
        url,
        body: jsonEncode(b),
        headers: _headerInfo,
      );
      print(dataResponse.body);

      checkStatusCode(dataResponse.statusCode);
      return jsonDecode(dataResponse.body)["message"];
    } catch (e) {
      print(e);
      return -1;
    }
  }
}
