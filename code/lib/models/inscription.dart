/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Models
 
  Purpose: This file contains the Bean person 

  Author:  Christopher Lucas
  Date:    13.06.2022
*/
/// Represents an inscription of Ecomu App
class Inscription {
  /// Identifier for inscription class
  int? idRegistration;

  /// Foreign key linked to person table
  int? idStudent;

  /// Foreign key linked to person table
  int? idLegRep;

  /// Foreign key linked to person table
  int? idPayer;

  /// Foreign key linked to cours table
  int idCourse;

  /// Foreign key linked to section table
  int idSection;

  /// Foreign key linked to jour table
  int idDay;

  /// Time of the lesson
  String hour;

  /// Start date of the lesson
  String beginDate;

  /// Is the inscription active
  int isActive;

  /// Constructs inscription class
  Inscription({
    this.idRegistration,
    this.idStudent,
    this.idLegRep,
    this.idPayer,
    required this.idCourse,
    required this.idSection,
    required this.idDay,
    required this.hour,
    required this.beginDate,
    required this.isActive,
  });

  /// Converts a JSON to an [Inscription]
  Inscription.fromJson(Map<String, dynamic> json)
      : idRegistration = int.parse(json['id_registration']),
        idStudent = int.parse(json['id_student']),
        idLegRep = int.parse(json['id_repleg']),
        idPayer = int.parse(json['id_payer']),
        idCourse = int.parse(json['id_course']),
        idSection = int.parse(json['id_section']),
        idDay = json['id_day'],
        hour = json['hour'],
        beginDate = json['begin_date'],
        isActive = int.parse(json['is_active']);

  /// Converts an [Inscription] to a JSON
  Map<String, dynamic> toJson() => {
        'id_registration': idRegistration,
        'id_student': idStudent,
        'id_repleg': idLegRep,
        'id_payer': idPayer,
        'id_course': idCourse,
        'id_section': idSection,
        'id_day': idDay,
        'hour': hour,
        'begin_date': beginDate,
        'is_active': isActive,
      };
}
