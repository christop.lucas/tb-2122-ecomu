import 'package:ecomu_gestion/models/duration.dart';

class Registration {
  int? idRegistration;
  int? idStudent;
  String studentFirstname;
  String studentLastname;
  int? idRepLeg;
  String repLegFirstname;
  String repLegLastname;
  int? idPayer;
  String payerFirstname;
  String payerLastname;
  int idProfessor;
  String professorFirstname;
  String professorLastname;
  String courseName;
  CourseDuration duration;
  String courseType;
  String? courseWorkshop;
  String? instrument;
  String day;
  String section;
  String hour;
  String beginDate;
  bool isActive;

  Registration({
    this.idRegistration,
    this.idStudent,
    required this.studentFirstname,
    required this.studentLastname,
    this.idRepLeg,
    required this.repLegFirstname,
    required this.repLegLastname,
    this.idPayer,
    required this.payerFirstname,
    required this.payerLastname,
    required this.idProfessor,
    required this.professorFirstname,
    required this.professorLastname,
    required this.courseName,
    required this.duration,
    required this.courseType,
    required this.courseWorkshop,
    required this.instrument,
    required this.day,
    required this.section,
    required this.hour,
    required this.beginDate,
    required this.isActive,
  });

  Registration.fromJson(Map<String, dynamic> json)
      : idRegistration = json['id_registration'],
        idStudent = json['id_student'],
        studentFirstname = json['student_firstname'],
        studentLastname = json['student_lastname'],
        idRepLeg = json['id_repleg'],
        repLegFirstname = json['repleg_firstname'],
        repLegLastname = json['repleg_lastname'],
        idPayer = json['id_payer'],
        payerFirstname = json['payer_firstname'],
        payerLastname = json['payer_lastname'],
        idProfessor = json['id_professor'],
        professorFirstname = json['professor_firstname'],
        professorLastname = json['professor_lastname'],
        courseName = json['course_name'],
        duration = CourseDuration(duration: json['duration']),
        courseType = json['course_type'],
        courseWorkshop = json['course_workshop'],
        instrument = json['instrument'],
        day = json['day'],
        section = json['section'],
        hour = json['hour'],
        beginDate = json['begin_date'],
        isActive = json['is_active'] == 1;

  Map<String, dynamic> toJson() => {
        "id_registration": idRegistration,
        "id_student": idStudent,
        "student_firstname": studentFirstname,
        "student_lastname": studentLastname,
        "id_repleg": idRepLeg,
        "repleg_firstname": repLegFirstname,
        "repleg_lastname": repLegLastname,
        "id_payer": idPayer,
        "payer_firstname": payerFirstname,
        "payer_lastname": payerLastname,
        "id_professor": idProfessor,
        "professor_firstname": professorFirstname,
        "professor_lastname": professorLastname,
        "course_name": courseName,
        "duration": duration.duration,
        "course_type": courseType,
        "course_workshop": courseWorkshop,
        "instrument": instrument,
        "day": day,
        "section": section,
        "hour": hour,
        "begin_date": beginDate,
        "is_active": isActive
      };

  String get student {
    return "$studentFirstname $studentLastname";
  }

  String get repLeg {
    return "$repLegFirstname $repLegLastname";
  }

  String get payer {
    return "$payerFirstname $payerLastname";
  }

  String get professsor {
    return "$professorFirstname $professorLastname";
  }

  String get time {
    var s = hour.split(":");
    return "${s[0]}:${s[1]}";
  }
}
