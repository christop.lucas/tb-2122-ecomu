class RegistrationAdd {
  int? idStudent;
  int? idRepLeg;
  int? idPayer;
  int idProfessor;
  int idCourse;
  int? idInstrument;
  int? idWorkshop;
  int idSection;
  int idDay;
  String hour;
  String beginDate;

  RegistrationAdd({
    this.idStudent,
    this.idRepLeg,
    this.idPayer,
    required this.idProfessor,
    required this.idCourse,
    this.idInstrument,
    this.idWorkshop,
    required this.idSection,
    required this.idDay,
    required this.hour,
    required this.beginDate,
  });

  Map<String, dynamic> toJson() => {
        "id_student": idStudent,
        "id_repleg": idRepLeg,
        "id_payer": idPayer,
        "id_professor": idProfessor,
        "id_course": idCourse,
        "id_workshop": idWorkshop,
        "id_instrument": idInstrument,
        "id_day": idDay,
        "id_section": idSection,
        "hour": hour,
        "begin_date": beginDate
      };
}
