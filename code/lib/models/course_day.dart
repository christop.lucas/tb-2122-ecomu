class CourseDay {
  int idDay;
  String day;

  CourseDay({required this.idDay, required this.day});

  CourseDay.fromJson(Map<String, dynamic> json)
      : idDay = json['id_day'],
        day = json['day'];

  Map<String, dynamic> toJson() => {
        'id_day': idDay,
        'day': day,
      };
}
