class CourseType {
  int idCourseType;
  String name;
  bool hasInstrumental;

  CourseType({
    required this.idCourseType,
    required this.name,
    required this.hasInstrumental,
  });

  CourseType.fromJson(Map<String, dynamic> json)
      : idCourseType = json['id_course_type'],
        name = json['name'],
        hasInstrumental = json['has_instrumental'] == 1;

  Map<String, dynamic> toJson() => {
        'id_course_type': idCourseType,
        'name': name,
        'has_instrumental': hasInstrumental,
      };
  @override
  String toString() {
    return name;
  }

  bool isIndividuel() {
    return idCourseType == 1;
  }

  bool isWorkshop() {
    return idCourseType == 2;
  }

  bool isCollectif() {
    return idCourseType == 3;
  }
}
