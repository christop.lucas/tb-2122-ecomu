class CourseDuration {
  int duration;
  String formatedDuration;

  CourseDuration({required this.duration})
      : formatedDuration = "${duration ~/ (60 * 1000)} min.";

  CourseDuration.fromJson(Map<String, dynamic> json)
      : duration = json['duration'],
        formatedDuration = "${json['duration'] ~/ (60 * 1000)} min.";

  Map<String, dynamic> toJson() => {
        'duration': duration,
      };
  @override
  String toString() {
    return formatedDuration;
  }
}
