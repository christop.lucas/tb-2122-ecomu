class Workshop {
  int idWorkshop;
  String workshop;

  Workshop({
    required this.idWorkshop,
    required this.workshop,
  });

  Workshop.fromJson(Map<String, dynamic> json)
      : idWorkshop = json['id_workshop'],
        workshop = json['workshop'];

  Map<String, dynamic> toJson() => {
        'id_workshop': idWorkshop,
        'workshop': workshop,
      };
  @override
  String toString() {
    return workshop;
  }
}
