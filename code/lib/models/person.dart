/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Models
 
  Purpose: This file contains the Bean person 

  Author:  Christopher Lucas
  Date:    13.06.2022
*/
import 'package:intl/intl.dart';

/// Represents a generic person of the Ecomu App
class Person {
  int? idPerson;
  String? title;
  String? firstName;
  String? lastName;
  String? gender;
  String? address;
  String? birthday;
  int? zipCode;
  String? location;
  String? privateNumber;
  String? profNumber;
  String? email;

  Person({
    this.idPerson,
    this.title,
    this.firstName,
    this.lastName,
    this.gender,
    this.address,
    this.birthday,
    this.zipCode,
    this.location,
    this.privateNumber,
    this.profNumber,
    this.email,
  });

  Person.fromJson(Map<String, dynamic> json)
      : idPerson = json['id_person'],
        title = json['title'],
        firstName = json['firstName'],
        lastName = json['lastName'],
        gender = json['gender'],
        address = json['address'],
        birthday =
            DateFormat("yyyy-MM-dd").format(DateTime.parse(json['birthday'])),
        zipCode = json['zipCode'],
        location = json['location'],
        privateNumber = json['privateNumber'],
        profNumber = json['profNumber'],
        email = json['email'];

  Map<String, dynamic> toJson() => {
        'title': title,
        'firstName': firstName,
        'lastName': lastName,
        'gender': gender,
        'address': address,
        'birthday': birthday,
        'zipCode': zipCode.toString(),
        'location': location,
        'privateNumber': privateNumber,
        'profNumber': profNumber,
        'email': email,
      };
  @override
  String toString() {
    return '$idPerson:$title:$firstName:$lastName:$gender:$address:$birthday:$zipCode:$location:$privateNumber:$profNumber:$email';
  }
}
