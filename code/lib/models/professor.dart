/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Models
 
  Purpose: This file contains the Bean person 

  Author:  Christopher Lucas
  Date:    13.06.2022
*/
/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Models
 
  Purpose: This file contains the Bean person 

  Author:  Christopher Lucas
  Date:    13.06.2022
*/
import 'package:ecomu_gestion/models/person.dart';

/// Represents a professor of the Ecomu App
class Professor {
  int? idPerson;
  int? idProfessor;
  String? title;
  String firstName;
  String lastName;
  String? gender;
  String? address;
  String? birthday;
  int? zipCode;
  String? location;
  String? privateNumber;
  String? profNumber;
  String? email;
  double? workingRate;
  double? salary;

  Professor({
    required this.idPerson,
    required this.idProfessor,
    required this.title,
    required this.firstName,
    required this.lastName,
    required this.gender,
    required this.address,
    this.birthday,
    required this.zipCode,
    required this.location,
    required this.privateNumber,
    required this.profNumber,
    required this.email,
    required this.workingRate,
    required this.salary,
  });

  Professor.fromJson(Map<String, dynamic> json)
      : idPerson = json['id_person'],
        idProfessor = json['id_professor'],
        title = json['title'],
        firstName = json['firstName'] ?? json['prof_firstname'],
        lastName = json['lastName'] ?? json['prof_lastname'],
        gender = json['gender'],
        address = json['address'],
        birthday = json['birthday'],
        zipCode = json['zipCode'],
        location = json['location'],
        privateNumber = json['privateNumber'],
        profNumber = json['profNumber'],
        email = json['email'],
        workingRate = (json['working_rate'] is int)
            ? json['working_rate'].toDouble()
            : json['working_rate'],
        salary = (json['salary'] is int)
            ? json['salary'].toDouble()
            : json['salary'];

  Map<String, dynamic> toJson() => {
        'working_rate': workingRate,
        'salary': salary,
        'id_person': idPerson,
      };

  static Professor convertFromPerson(
    Person p,
    double salary,
    double workingRate,
    int? idProfessor,
  ) {
    return Professor(
      idPerson: p.idPerson,
      idProfessor: idProfessor,
      title: p.title!,
      firstName: p.firstName!,
      lastName: p.lastName!,
      gender: p.gender!,
      address: p.address!,
      zipCode: p.zipCode!,
      location: p.location!,
      privateNumber: p.privateNumber!,
      profNumber: p.profNumber!,
      email: p.email!,
      workingRate: workingRate,
      salary: salary,
    );
  }
}
