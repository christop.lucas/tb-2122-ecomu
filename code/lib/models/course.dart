class Course {
  int? idCourse;
  String name;
  int childPrice;
  int adultPrice;
  int? idCourseType;
  String? courseTypeName;
  int childInstrumentalPrice;
  int adultInstrumentalPrice;
  int duration;

  Course({
    this.idCourse,
    required this.name,
    required this.childPrice,
    required this.adultPrice,
    this.idCourseType,
    this.courseTypeName,
    required this.adultInstrumentalPrice,
    required this.childInstrumentalPrice,
    required this.duration,
  });

  Course.fromJson(Map<String, dynamic> json)
      : idCourse = json['id_course'],
        name = json['name'],
        childPrice = json['child_price'],
        adultPrice = json["adult_price"],
        idCourseType = json["id_course_type"],
        courseTypeName = json['course_type_name'],
        childInstrumentalPrice = json["child_instrumental_price"],
        adultInstrumentalPrice = json["adult_instrumental_price"],
        duration = json["duration"];

  Map<String, dynamic> toJson() => {
        'id_course': idCourse,
        'name': name,
        'child_price': childPrice,
        'adult_price': adultPrice,
        'id_course_type': idCourseType,
        'course_type_name': courseTypeName,
        'child_instrumental_price': childInstrumentalPrice,
        'adult_instrumental_price': adultInstrumentalPrice,
        'duration': duration,
      };

  int durationMinutes() {
    return duration ~/ (60 * 1000);
  }

  String displayDuration() {
    return "${durationMinutes()} min.";
  }

  String childPriceCombined() {
    final instrument =
        (childInstrumentalPrice != 0) ? " / $childInstrumentalPrice .-" : "";
    return "$childPrice .-$instrument";
  }

  String adultPriceCombined() {
    final instrument =
        (adultInstrumentalPrice != 0) ? " / $adultInstrumentalPrice .-" : "";
    return "$adultPrice .-$instrument";
  }
}
