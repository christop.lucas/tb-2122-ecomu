/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Models
 
  Purpose: This file contains the Bean person 

  Author:  Christopher Lucas
  Date:    13.06.2022
*/
/// Represents the legal guardian of a [Student] of the Ecomu App
class LegRep {
  final int idPerson;
  final String title;
  final String firstName;
  final String lastName;
  final String gender;
  final String address;
  final String? birthday;
  final int zipCode;
  final String location;
  final String privateNumber;
  final String profNumber;
  final String email;

  LegRep({
    required this.idPerson,
    required this.title,
    required this.firstName,
    required this.lastName,
    required this.gender,
    required this.address,
    this.birthday,
    required this.zipCode,
    required this.location,
    required this.privateNumber,
    required this.profNumber,
    required this.email,
  });

  LegRep.fromJson(Map<String, dynamic> json)
      : idPerson = int.parse(json['id_person']),
        title = json['title'],
        firstName = json['firstName'],
        lastName = json['lastName'],
        gender = json['gender'],
        address = json['address'],
        birthday = json['birthday'],
        zipCode = int.parse(json['zipCode']),
        location = json['location'],
        privateNumber = json['privateNumber'],
        profNumber = json['profNumber'],
        email = json['email'];

  Map<String, dynamic> toJson() => {
        'id_person': idPerson,
        'title': title,
        'firstName': firstName,
        'lastName': lastName,
        'gender': gender,
        'address': address,
        'birthday': birthday,
        'zipCode': zipCode,
        'location': location,
        'privateNumber': privateNumber,
        'profNumber': profNumber,
        'email': email,
      };
}
