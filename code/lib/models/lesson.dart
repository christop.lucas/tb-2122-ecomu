class Lesson {
  String type;
  String details;
  String prof;
  String eleve;
  String heure;
  String jour;

  Lesson({
    required this.details,
    required this.eleve,
    required this.heure,
    required this.jour,
    required this.prof,
    required this.type,
  });
}
