class Section {
  int idSection;
  String name;

  Section({
    required this.idSection,
    required this.name,
  });

  Section.fromJson(Map<String, dynamic> json)
      : idSection = json['id_section'],
        name = json['name'];

  Map<String, dynamic> toJson() => {
        'id_section': idSection,
        'name': name,
      };

  @override
  String toString() {
    return name;
  }
}
