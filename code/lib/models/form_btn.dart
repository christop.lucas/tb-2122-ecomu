class FormBtn {
  String text;
  void Function()? function;

  FormBtn({
    required this.text,
    required this.function,
  });
}
