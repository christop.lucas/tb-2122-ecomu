/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Models
 
  Purpose: This file contains the Bean instrument 

  Author:  Christopher Lucas
  Date:    13.06.2022
*/
/// Represents an musical instrument of the Ecomu App
class Instrument {
  /// Identifier for instrument class
  final int idInstrument;

  /// Name of the instrument
  final String name;

  /// Constructs the instrument Bean
  Instrument({
    required this.idInstrument,
    required this.name,
  });

  /// Converts a JSON to an [Instrument]
  Instrument.fromJson(Map<String, dynamic> json)
      : idInstrument = json['id_instrument'],
        name = json['name'];

  /// Converts an [Instrument] to a JSON
  Map<String, dynamic> toJson() => {
        'id_instrument': idInstrument,
        'name': name,
      };
  @override
  String toString() {
    return name;
  }
}
