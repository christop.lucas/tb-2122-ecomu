class ProfInstrument {
  int idProfessor;
  String profFirstname;
  String profLastname;

  ProfInstrument({
    required this.idProfessor,
    required this.profFirstname,
    required this.profLastname,
  });
  ProfInstrument.fromJson(Map<String, dynamic> json)
      : idProfessor = json['id_professor'],
        profFirstname = json['prof_firstname'],
        profLastname = json['prof_lastname'];

  Map<String, dynamic> toJson() => {
        'id_professor': idProfessor,
        'prof_firstname': profFirstname,
        'prof_lastname': profLastname,
      };
}
