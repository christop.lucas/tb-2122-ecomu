import 'package:intl/intl.dart';

class Bill {
  int? idBill;
  String? dateOfIssue;
  bool acquitted;
  double total;
  String? dateBeginReminder;
  int? idStudent;
  String studentFirstname;
  String studentLastname;
  int? idRepLeg;
  String repLegFirstname;
  String repLegLastname;
  int? idPayer;
  String payerFirstname;
  String payerLastname;
  int idProfessor;
  String professorFirstname;
  String professorLastname;
  int idCourse;
  String courseName;
  double coursePrice;
  String courseType;
  String? courseWorkshop;
  String? instrument;
  double registrationFee;
  double familyDiscount;
  double otherDiscount;
  double creditNote;

  Bill({
    this.idBill,
    this.idStudent,
    required this.studentFirstname,
    required this.studentLastname,
    this.idRepLeg,
    required this.repLegFirstname,
    required this.repLegLastname,
    this.idPayer,
    required this.payerFirstname,
    required this.payerLastname,
    required this.idProfessor,
    required this.professorFirstname,
    required this.professorLastname,
    required this.idCourse,
    required this.courseName,
    required this.coursePrice,
    required this.courseType,
    required this.courseWorkshop,
    required this.instrument,
    required this.registrationFee,
    required this.familyDiscount,
    required this.otherDiscount,
    required this.creditNote,
    required this.dateBeginReminder,
    required this.dateOfIssue,
    required this.acquitted,
    required this.total,
  });
/*(json['working_rate'] is int)
            ? json['working_rate'].toDouble()
            : json['working_rate'],*/
  Bill.fromJson(Map<String, dynamic> json)
      : idBill = json['id_bill'],
        dateOfIssue = json['date_of_issue'],
        acquitted = json['acquitted'] == 1,
        total =
            (json['total'] is int) ? json['total'].toDouble() : json['total'],
        dateBeginReminder = json['date_begin_reminder'],
        idStudent = json['id_student'],
        studentFirstname = json['student_firstname'],
        studentLastname = json['student_lastname'],
        idRepLeg = json['id_repleg'],
        repLegFirstname = json['repleg_firstname'],
        repLegLastname = json['repleg_lastname'],
        idPayer = json['id_payer'],
        payerFirstname = json['payer_firstname'],
        payerLastname = json['payer_lastname'],
        idProfessor = json['id_professor'],
        professorFirstname = json['professor_firstname'],
        professorLastname = json['professor_lastname'],
        idCourse = json['id_course'],
        courseName = json['course_name'],
        courseType = json['course_type'],
        courseWorkshop = json['course_workshop'],
        instrument = json['instrument'],
        registrationFee = (json['registration_fee'] is int)
            ? json['registration_fee'].toDouble()
            : json['registration_fee'],
        familyDiscount = (json['family_discount'] is int)
            ? json['family_discount'].toDouble()
            : json['family_discount'],
        otherDiscount = (json['other_discount'] is int)
            ? json['other_discount'].toDouble()
            : json['other_discount'],
        creditNote = (json['credit_note'] is int)
            ? json['credit_note'].toDouble()
            : json['credit_note'],
        coursePrice = (json['course_price'] is int)
            ? json['course_price'].toDouble()
            : json['course_price'];

  Map<String, dynamic> toJson() => {
        "id_bill": idBill,
        "date_of_issue": dateOfIssue,
        "acquitted": acquitted,
        "total": total,
        "date_begin_reminder": dateBeginReminder,
        "id_student": idStudent,
        "student_firstname": studentFirstname,
        "student_lastname": studentLastname,
        "id_repleg": idRepLeg,
        "repleg_firstname": repLegFirstname,
        "repleg_lastname": repLegLastname,
        "id_payer": idPayer,
        "payer_firstname": payerFirstname,
        "payer_lastname": payerLastname,
        "id_professor": idProfessor,
        "professor_firstname": professorFirstname,
        "professor_lastname": professorLastname,
        "course_name": courseName,
        "id_course": idCourse,
        "course_type": courseType,
        "course_workshop": courseWorkshop,
        "instrument": instrument,
        "registration_fee": registrationFee,
        "family_discount": familyDiscount,
        "other_discount": otherDiscount,
        "credit_note": creditNote,
        "course_price": coursePrice
      };

  String get student {
    return "$studentFirstname $studentLastname";
  }

  String get professor {
    return "$professorFirstname $professorLastname";
  }

  String get totalPrice {
    final price =
        total + registrationFee - familyDiscount - otherDiscount - creditNote;
    return "${(price < 0) ? 0 : price} CHF";
  }

  String get isAcquitted {
    return (acquitted) ? "Oui" : "Non";
  }

  String get formatedDate {
    final date = DateTime.parse(dateOfIssue!);
    return DateFormat.yMMMMd('fr_CH').format(date);
  }
}
