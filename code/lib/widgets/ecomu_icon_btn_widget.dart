import 'package:flutter/material.dart';

class IconButtonList extends StatelessWidget {
  final void Function()? action;
  final Color? color;
  final IconData icon;
  const IconButtonList({
    Key? key,
    this.action,
    required this.icon,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(icon),
      splashRadius: 18,
      color: color,
      onPressed: action,
    );
  }
}
