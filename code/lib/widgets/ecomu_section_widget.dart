/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: General Widgets
 
  Purpose: This widget is a section card of the content.

  Author:  Christopher Lucas
  Date:    04.06.2022
*/
import 'package:flutter/material.dart';

class EcomuSection extends StatelessWidget {
  /// The flex priority of the section
  final int? priority;

  /// The custom padding for a [EcomuSection]. Default is all(8.0)
  final EdgeInsets? padding;

  /// The child Widget of the [EcomuSection]
  final Widget child;

  /// Constructs a section for the Ecomu App
  const EcomuSection({
    Key? key,
    this.priority,
    this.padding,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: priority ?? 1,
      child: Card(
        child: Padding(
          padding: (padding != null) ? padding! : const EdgeInsets.all(8.0),
          child: child,
        ),
      ),
    );
  }
}
