/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Models
 
  Purpose: This file contains the Bean person 

  Author:  Christopher Lucas
  Date:    13.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// Builds a Date picker
class DatePickerField extends StatefulWidget {
  /// Placeholder text
  final String title;

  /// Text Field controller
  final TextEditingController? controller;

  /// Date format fot the birth date
  final DateFormat df = DateFormat("yyyy-MM-dd");

  /// Function to validate the field
  final String? Function(String?) validator;

  final DateTime? initDate;

  final DateTime? firstDate;
  final DateTime? lastDate;

  /// Constructs Stateful Widget
  DatePickerField({
    Key? key,
    required this.title,
    this.controller,
    required this.validator,
    this.initDate,
    this.firstDate,
    this.lastDate,
  }) : super(key: key);

  @override
  DatePickerState createState() => DatePickerState();
}

/// State class for the [DatePickerField]
class DatePickerState extends State<DatePickerField> {
  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    return TextFormField(
      controller: widget.controller,
      readOnly: true,
      decoration: InputDecoration(
        label: Text(widget.title),
        border: const OutlineInputBorder(),
      ),
      onTap: () async {
        final now = DateTime.now();
        DateTime? pickedDate = await showDatePicker(
          context: context,
          initialDate: (widget.initDate != null)
              ? widget.initDate!
              : DateTime(2000, 1, 1),
          firstDate: (widget.firstDate != null)
              ? widget.firstDate!
              : DateTime(1950, 1, 1),
          lastDate:
              (widget.lastDate != null) ? widget.lastDate! : DateTime(now.year),
          initialEntryMode: DatePickerEntryMode.input,
          helpText: local.date_picker_text,
        );
        if (pickedDate != null) {
          widget.controller?.text = widget.df.format(pickedDate);
        }
      },
      validator: widget.validator,
    );
  }
}
