/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: General Widget
 
  Purpose: The Widget builds the correct form

  Author:  Christopher Lucas
  Date:    13.06.2022
*/
import 'package:flutter/material.dart';

/// Widget for a textfield in a any form like [EcomuUserForm]
class EcomuFormField extends StatelessWidget {
  /// Indicates if their is a left neighbour to add padding
  final bool hasLeftNeighbour;

  /// Placeholder value of the [TextFormField]
  final String text;

  /// Text controller for the [TextFormField]
  final TextEditingController controller;
  final String? Function(String?)? validator;

  final bool? enabled;

  /// Constructs the widget
  const EcomuFormField({
    Key? key,
    required this.hasLeftNeighbour,
    required this.text,
    required this.controller,
    this.validator,
    this.enabled,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: hasLeftNeighbour ? 16.0 : 0.0),
      child: TextFormField(
        enabled: enabled,
        controller: controller,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          labelText: text,
        ),
        validator: validator,
      ),
    );
  }
}
