/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: General Widget
 
  Purpose: The Widget builds the correct form

  Author:  Christopher Lucas
  Date:    13.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/screens/student_screen/student_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EcomuUserForm extends StatefulWidget {
  final TextEditingController lastNameController;
  final TextEditingController firstNameController;
  final TextEditingController emailController;
  final TextEditingController birthdayController;
  final TextEditingController addressController;
  final TextEditingController zipCodeController;
  final TextEditingController locationController;
  final TextEditingController privNumberController;
  final TextEditingController profNumberController;
  final bool completeForm;

  const EcomuUserForm({
    Key? key,
    required this.lastNameController,
    required this.firstNameController,
    required this.emailController,
    required this.birthdayController,
    required this.addressController,
    required this.zipCodeController,
    required this.locationController,
    required this.privNumberController,
    required this.profNumberController,
    required this.completeForm,
  }) : super(key: key);

  @override
  State<EcomuUserForm> createState() => _EcomuUserFormState();
}

class _EcomuUserFormState extends State<EcomuUserForm> {
  String? valueTitle;
  String? valueGender;
  @override
  Widget build(BuildContext context) {
    return const SizedBox.shrink();
    /*return Row(
      children: [
        (widget.completeForm)
            ? const Expanded(
                child: SizedBox.shrink(),
              )
            : const SizedBox.shrink(),
        Expanded(
          flex: 5,
          child: Form(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 32.0,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: DropdownButtonFormField(
                          decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            labelText: local.form_user_title,
                          ),
                          value: valueTitle,
                          hint: Text("Choisissez"),
                          onChanged: (String? value) {
                            setState(() {
                              if (value != null) valueTitle = value;
                            });
                          },
                          items: local.person_titles
                              .split(":")
                              .map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                      const Expanded(
                        flex: 2,
                        child: SizedBox.shrink(),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 32.0,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: EcomuFormField(
                          controller: widget.lastNameController,
                          hasLeftNeighbour: false,
                          text: local.form_user_lastname,
                        ),
                      ),
                      Expanded(
                        child: EcomuFormField(
                          controller: widget.firstNameController,
                          hasLeftNeighbour: true,
                          text: local.form_user_firstname,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 32.0,
                  ),
                  child: EcomuFormField(
                    controller: widget.emailController,
                    hasLeftNeighbour: false,
                    text: local.form_user_email,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 32.0, bottom: 16.0),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: DropdownButtonFormField(
                          decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            labelText: local.form_user_gender,
                          ),
                          hint: Text("Choisissez"),
                          value: valueGender,
                          onChanged: (String? value) {
                            setState(() {
                              if (value != null) valueGender = value;
                            });
                          },
                          items: local.person_genders
                              .split(":")
                              .map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16.0),
                          child: DatePickerField(
                            title: local.form_user_birthday,
                            controller: widget.birthdayController,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const Divider(),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: EcomuFormField(
                    controller: widget.addressController,
                    hasLeftNeighbour: false,
                    text: local.form_user_address,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 32.0,
                    bottom: 16.0,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: EcomuFormField(
                          controller: widget.zipCodeController,
                          hasLeftNeighbour: false,
                          text: local.form_user_zipcode,
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: EcomuFormField(
                          controller: widget.locationController,
                          hasLeftNeighbour: true,
                          text: local.form_user_location,
                        ),
                      ),
                    ],
                  ),
                ),
                const Divider(),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 16.0,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: EcomuFormField(
                          controller: widget.privNumberController,
                          hasLeftNeighbour: false,
                          text: local.form_user_profnumber,
                        ),
                      ),
                      Expanded(
                        child: EcomuFormField(
                          controller: widget.profNumberController,
                          hasLeftNeighbour: true,
                          text: local.form_user_privnumber,
                        ),
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 32.0),
                    child: Wrap(
                      spacing: 20,
                      children: [
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            onPrimary:
                                Theme.of(context).colorScheme.onSecondary,
                            primary: Theme.of(context).colorScheme.secondary,
                            minimumSize: const Size(50, 50),
                            textStyle: const TextStyle(fontSize: 16.0),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                          ).copyWith(
                              elevation: ButtonStyleButton.allOrNull(0.0)),
                          onPressed: () {
                            viewModel.setStudentState = StudentState.empty;
                            resetControllers(context);
                          },
                          child: const Text("Annuler"),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            onPrimary: Theme.of(context).colorScheme.onPrimary,
                            primary: Theme.of(context).colorScheme.primary,
                            minimumSize: const Size(50, 50),
                            textStyle: const TextStyle(fontSize: 16.0),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                          ).copyWith(
                              elevation: ButtonStyleButton.allOrNull(0.0)),
                          onPressed: () async {
                            var zipCode =
                                (widget.zipCodeController.text.isEmpty)
                                    ? null
                                    : int.parse(widget.zipCodeController.text);
                            final result = await viewModel.insertStudent(
                              Person(
                                idPerson: null,
                                title: valueTitle,
                                firstName: widget.firstNameController.text,
                                lastName: widget.lastNameController.text,
                                gender: valueGender,
                                address: widget.addressController.text,
                                zipCode: zipCode,
                                location: widget.locationController.text,
                                privateNumber: widget.privNumberController.text,
                                profNumber: widget.profNumberController.text,
                                email: widget.emailController.text,
                                birthday: widget.birthdayController.text,
                              ),
                            );
                            if (result) {
                              Future.delayed(Duration.zero, () {
                                resetControllers(context);
                              });
                            }
                            final snackBar = SnackBar(
                              content: Text(
                                (result)
                                    ? local.form_user_student_success
                                    : "Error",
                                style: const TextStyle(color: Colors.white),
                              ),
                              backgroundColor: (Colors.black),
                            );
                            Future.delayed(Duration.zero, () {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                            });
                          },
                          child: Text(local.btn_insert_txt),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        (widget.completeForm)
            ? const Expanded(
                child: SizedBox.shrink(),
              )
            : const SizedBox.shrink(),
      ],
    );*/
  }

  void resetControllers(BuildContext context) {
    widget.firstNameController.clear();
    widget.lastNameController.clear();
    widget.addressController.clear();
    widget.locationController.clear();
    widget.privNumberController.clear();
    widget.emailController.clear();
    widget.birthdayController.clear();
    widget.zipCodeController.clear();
    widget.profNumberController.clear();
    setState(() {
      valueGender = null;
      valueTitle = null;
    });
  }
}
