/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: General Widgets
 
  Purpose: This widget is a section column.

  Author:  Christopher Lucas
  Date:    23.06.2022
*/
import 'package:flutter/material.dart';

/// Builds a search field Widget
class SearchField extends StatelessWidget {
  /// Manages the value of the textfield
  final TextEditingController controller;

  /// Placeholder text of the textfield
  final String text;

  /// Action executed when the textfield value is changed
  final void Function(String value) action;

  /// Radius of the corners of the textfieldy
  final double? borderRadius;

  /// Constructs the [EcomuSearch] Widget
  const SearchField(
      {Key? key,
      required this.controller,
      required this.text,
      required this.action,
      this.borderRadius})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      decoration: InputDecoration(
        prefixIcon: const Icon(Icons.search),
        hintText: text,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderRadius ?? 10.0),
        ),
      ),
      onChanged: action,
    );
  }
}
