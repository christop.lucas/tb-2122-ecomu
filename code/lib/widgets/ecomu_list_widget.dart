/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Teacher
 
  Purpose: This file contains the Widget of the list of teachers.

  Author:  Christopher Lucas
  Date:    07.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_placeholder_widget.dart';
import 'package:flutter/material.dart';

class EcomuList extends StatelessWidget {
  final int type;
  final Function(int)? onItemClick;
  final bool Function() matchFunction;
  final List<dynamic> list;
  const EcomuList({
    Key? key,
    required this.type,
    this.onItemClick,
    required this.matchFunction,
    required this.list,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // localization text
    var local = AppLocalizations.of(context)!;
    // boolean for medium list
    final md = type == 1;
    // boolean for large list
    final lg = type == 0;
    if (list.isNotEmpty) {
      return ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "${list[index].lastName} ${list[index].firstName}",
              ),
            ),
            subtitle: (md || lg)
                ? Align(
                    alignment: Alignment.centerLeft,
                    child: Text(list[index].email),
                  )
                : null,
            leading: (lg)
                ? Container(
                    width: 48,
                    height: 48,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Theme.of(context).colorScheme.surfaceVariant,
                    ),
                    child: Center(
                      child: Text(
                        list[index].lastName[0],
                        style: const TextStyle(fontSize: 20),
                      ),
                    ),
                  )
                : null,
            onTap: () {
              onItemClick!(index);
            },
          );
        },
      );
    } else {
      if (matchFunction()) {
        return EcomuSectionPlaceholder(
          icon: Icons.search_off,
          text: local.match_text,
        );
      } else {
        return EcomuSectionPlaceholder(
          icon: Icons.access_time,
          text: local.person_recent_text,
        );
      }
    }
  }
}
