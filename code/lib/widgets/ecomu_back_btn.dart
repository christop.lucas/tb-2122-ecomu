import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:flutter/material.dart';

class ReturnButton extends StatelessWidget {
  final void Function() action;
  const ReturnButton({
    Key? key,
    required this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    return Align(
      alignment: Alignment.centerLeft,
      child: TextButton.icon(
        onPressed: action,
        icon: const Icon(Icons.arrow_back_ios),
        label: Text(local.btn_return_text),
      ),
    );
  }
}
