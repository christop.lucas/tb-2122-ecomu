/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: General Widgets
 
  Purpose: This widget is a with a list of labels with a value.

  Author:  Christopher Lucas
  Date:    07.06.2022
*/
import 'package:flutter/material.dart';

/// Builds the list of text Widget
class EcomuListLabelValue extends StatelessWidget {
  /// The list of data to be displayed
  final List<String> data;

  /// Vertical alignment of the data
  final CrossAxisAlignment crossAlignment;

  final bool overflow;
  final double fontSize;

  /// Constructs the Widget EcomuListLabelValue
  const EcomuListLabelValue({
    Key? key,
    required this.data,
    required this.crossAlignment,
    required this.overflow,
    required this.fontSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final lst = data.map(
      (text) {
        return Container(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: Text(
            text,
            style: TextStyle(
              fontSize: fontSize,
            ),
            overflow: (overflow) ? TextOverflow.ellipsis : null,
          ),
        );
      },
    ).toList();
    return Column(
      crossAxisAlignment: crossAlignment,
      children: lst,
    );
  }
}
