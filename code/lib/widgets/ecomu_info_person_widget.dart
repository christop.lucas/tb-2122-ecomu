/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: General Widgets
 
  Purpose: This widget is a with a list of labels with a value.

  Author:  Christopher Lucas
  Date:    07.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/screens/person_screen/person_viewmodel.dart';
import 'package:ecomu_gestion/widgets/ecomu_label_value_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// Builds a Widget as rows of label and value
class EcomuInfoPerson extends StatelessWidget {
  /// list of labels to be displayed
  final List<String> dataLabel;

  final bool bigInfo;

  /// Constructs the EcomuInfoPerson Widget
  const EcomuInfoPerson({
    Key? key,
    required this.dataLabel,
    required this.bigInfo,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    if (bigInfo) {
      var lbl = local.person_attributes_short_list.split(local.splitter);
      var data = context.read<PersonViewModel>().buildPersonInfo(lbl, bigInfo);
      return Padding(
        padding: const EdgeInsets.only(
          bottom: 8.0,
          top: 8.0,
          left: 24.0,
          right: 24.0,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                EcomuListLabelValue(
                  crossAlignment: CrossAxisAlignment.start,
                  data: data.keys.toList(),
                  overflow: false,
                  fontSize: 28.0,
                ),
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 32.0),
                    child: EcomuListLabelValue(
                      crossAlignment: CrossAxisAlignment.start,
                      data: data.values.toList(),
                      overflow: true,
                      fontSize: 28.0,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    } else {
      var lbl = local.person_attributes_list.split(local.splitter);
      var data = context.read<PersonViewModel>().buildPersonInfo(lbl, bigInfo);
      return Padding(
        padding: const EdgeInsets.only(
          bottom: 8.0,
          top: 8.0,
          left: 24.0,
          right: 24.0,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                EcomuListLabelValue(
                  crossAlignment: CrossAxisAlignment.start,
                  data: data.keys.toList(),
                  overflow: false,
                  fontSize: 20.0,
                ),
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 32.0),
                    child: EcomuListLabelValue(
                      crossAlignment: CrossAxisAlignment.start,
                      data: data.values.toList(),
                      overflow: true,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }
  }
}
