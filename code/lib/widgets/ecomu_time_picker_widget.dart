/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Models
 
  Purpose: This file contains the Bean person 

  Author:  Christopher Lucas
  Date:    13.06.2022
*/
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// Builds a Date picker
class TimePickerField extends StatefulWidget {
  /// Placeholder text
  final String title;

  /// Text Field controller
  final TextEditingController? controller;

  /// Date format fot the birth date
  final DateFormat tf = DateFormat("kk:mm");

  /// Function to validate the field
  final String? Function(String?) validator;

  /// Constructs Stateful Widget
  TimePickerField({
    Key? key,
    required this.title,
    this.controller,
    required this.validator,
  }) : super(key: key);

  @override
  TimePickerFieldState createState() => TimePickerFieldState();
}

/// State class for the [DatePickerField]
class TimePickerFieldState extends State<TimePickerField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      readOnly: true,
      decoration: InputDecoration(
        label: Text(widget.title),
        border: const OutlineInputBorder(),
      ),
      onTap: () async {
        final now = DateTime.now();
        final pickedTime = await showTimePicker(
          context: context,
          initialTime: const TimeOfDay(hour: 8, minute: 0),
        );
        if (pickedTime != null) {
          final date = DateTime(
            now.year,
            now.month,
            now.day,
            (pickedTime.hour == 24) ? 0 : pickedTime.hour,
            pickedTime.minute,
          );
          widget.controller?.text = widget.tf.format(date);
        }
      },
      validator: widget.validator,
    );
  }
}
