/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: General Widgets
 
  Purpose: This widget is a section column.

  Author:  Christopher Lucas
  Date:    04.06.2022
*/
import 'package:flutter/material.dart';

/// Builds of column of sections
class EcomuSectionColumn extends StatelessWidget {
  // The flex priority of the section
  final int? priority;
  // The children of the section row
  final List<Widget> children;

  const EcomuSectionColumn({
    Key? key,
    required this.children,
    this.priority,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: priority ?? 1,
      child: Row(
        children: children,
      ),
    );
  }
}
