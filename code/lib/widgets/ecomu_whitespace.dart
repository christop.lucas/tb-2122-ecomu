import 'package:flutter/material.dart';

class WhiteSpace extends StatelessWidget {
  final bool isActive;
  const WhiteSpace({
    Key? key,
    required this.isActive,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (isActive) {
      return const Expanded(
        child: SizedBox.shrink(),
      );
    } else {
      return const SizedBox.shrink();
    }
  }
}
