import 'package:ecomu_gestion/widgets/ecomu_whitespace.dart';
import 'package:flutter/material.dart';

class ContentSpacing extends StatelessWidget {
  final Widget child;
  final int? priority;
  const ContentSpacing({
    Key? key,
    required this.child,
    this.priority,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Row(
          children: [
            WhiteSpace(isActive: constraints.biggest.width > 700),
            Expanded(
              flex: priority ?? 1,
              child: child,
            ),
            WhiteSpace(isActive: constraints.biggest.width > 700),
          ],
        );
      },
    );
  }
}
