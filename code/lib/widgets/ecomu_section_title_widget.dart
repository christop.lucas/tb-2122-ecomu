/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: General Widgets
 
  Purpose: This file contains the widget for the main title card.

  Author:  Christopher Lucas
  Date:    04.06.2022
*/
import 'package:flutter/material.dart';

/// Builds a Widget to display the title of a big Card.
class EcomuSectionTitle extends StatelessWidget {
  // Text of the title
  final String text;

  final TextStyle? style;

  final EdgeInsets? padding;

  const EcomuSectionTitle({
    Key? key,
    required this.text,
    this.style,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: (padding != null) ? padding! : const EdgeInsets.only(top: 16),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          text,
          style: (style == null)
              ? const TextStyle(
                  fontSize: 24,
                )
              : style,
        ),
      ),
    );
  }
}
