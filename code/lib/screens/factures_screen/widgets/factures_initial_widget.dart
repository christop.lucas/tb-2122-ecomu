import 'package:ecomu_gestion/screens/factures_screen/widgets/factures_table_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_title_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';

class FactureInitial extends StatelessWidget {
  const FactureInitial({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return EcomuSection(
      child: ContentSpacing(
        priority: 5,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            EcomuSectionTitle(
              text: "Liste des factures",
            ),
            Divider(),
            Expanded(
              child: FactureTable(),
            ),
          ],
        ),
      ),
    );
  }
}
