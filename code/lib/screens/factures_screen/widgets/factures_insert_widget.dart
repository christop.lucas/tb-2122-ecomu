import 'package:ecomu_gestion/screens/factures_screen/factures_viewmodel.dart';
import 'package:ecomu_gestion/widgets/ecomu_back_btn.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_title_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FactureInsert extends StatelessWidget {
  const FactureInsert({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<FactureViewModel>();
    return EcomuSection(
        child: Column(
      children: [
        ReturnButton(
          action: viewModel.back,
        ),
        ContentSpacing(
          priority: 5,
          child: Column(
            children: const [
              EcomuSectionTitle(
                text: "Insérer une facture",
              ),
              Divider(),
            ],
          ),
        )
      ],
    ));
  }
}
