/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Factures
 
  Purpose: This file contains Factures View

  Author:  Christopher Lucas
  Date:    13.07.2022
*/
import 'package:ecomu_gestion/models/bill.dart';
import 'package:ecomu_gestion/models/registration.dart';
import 'package:ecomu_gestion/screens/factures_screen/factures_viewmodel.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/inscriptions_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class FactureTable extends StatelessWidget {
  const FactureTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<FactureViewModel>();

    return LayoutBuilder(
      builder: (context, constraint) {
        final maxHeight = constraint.maxHeight - viewModel.dataPagerHeight;
        final height = viewModel.wishedTableHeight;
        return FutureBuilder<List<Bill>>(
          future: viewModel.getBills(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var bills = snapshot.data!;
              final source = FactureDataSource(
                rowsPerPage: viewModel.rowsPerPage,
                dataPagerHeight: viewModel.dataPagerHeight,
                bills: bills,
              );
              return Column(
                children: [
                  SizedBox(
                    height: (maxHeight < height) ? maxHeight : height,
                    width: constraint.maxWidth,
                    child: SfDataGrid(
                      source: source,
                      rowHeight: viewModel.rowHeight,
                      headerRowHeight: viewModel.headerHeight,
                      columns: [
                        GridColumn(
                          columnWidthMode: ColumnWidthMode.fill,
                          columnName: 'eleve',
                          label: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            alignment: Alignment.centerLeft,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: const [
                                Text(
                                  'Eleve',
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                          ),
                        ),
                        GridColumn(
                          columnWidthMode: ColumnWidthMode.fill,
                          columnName: 'prof',
                          label: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            alignment: Alignment.centerLeft,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: const [
                                Text(
                                  'Prof',
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                          ),
                        ),
                        GridColumn(
                          columnWidthMode: ColumnWidthMode.fill,
                          columnName: 'type',
                          label: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Type',
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        GridColumn(
                          columnWidthMode: ColumnWidthMode.fill,
                          columnName: 'name',
                          label: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Nom du cours',
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        GridColumn(
                          columnWidthMode: ColumnWidthMode.fill,
                          columnName: 'total',
                          label: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Montant total',
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        GridColumn(
                          columnWidthMode: ColumnWidthMode.fill,
                          columnName: 'acquitted',
                          label: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Acquitté',
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        GridColumn(
                          columnWidthMode: ColumnWidthMode.fill,
                          columnName: 'dateIssue',
                          label: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              "Date d'émission",
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                      ],
                      selectionMode: SelectionMode.single,
                      onSelectionChanged: (addedRows, removedRows) {
                        final index =
                            source.dataGridRows.indexOf(addedRows.last);
                        viewModel.setSelectedBill = bills[index];
                      },
                    ),
                  ),
                  SizedBox(
                    height: viewModel.dataPagerHeight,
                    child: SfDataPager(
                      delegate: source,
                      pageCount: (bills.length / viewModel.rowsPerPage)
                          .ceil()
                          .toDouble(),
                      direction: Axis.horizontal,
                    ),
                  ),
                ],
              );
            }
            return Column();
          },
        );
      },
    );
  }
}

class FactureDataSource extends DataGridSource {
  final int rowsPerPage;
  final double dataPagerHeight;
  final List<Bill> bills;
  var paginatedBills = <Bill>[];
  FactureDataSource({
    required this.bills,
    required this.rowsPerPage,
    required this.dataPagerHeight,
  }) {
    buildPaginatedDataGridRows();
  }

  List<DataGridRow> dataGridRows = [];

  @override
  List<DataGridRow> get rows => dataGridRows;

  @override
  DataGridRowAdapter? buildRow(DataGridRow row) {
    return DataGridRowAdapter(
      cells: row.getCells().map<Widget>(
        (dataGridCell) {
          return Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(
              dataGridCell.value.toString(),
              overflow: TextOverflow.ellipsis,
            ),
          );
        },
      ).toList(),
    );
  }

  @override
  Future<bool> handlePageChange(int oldPageIndex, int newPageIndex) async {
    int startIndex = newPageIndex * rowsPerPage;
    int endIndex = startIndex + rowsPerPage;
    if (startIndex < bills.length) {
      paginatedBills = bills
          .getRange(
              startIndex, (endIndex <= bills.length) ? endIndex : bills.length)
          .toList(growable: false);
      buildPaginatedDataGridRows();
    } else {
      paginatedBills = [];
    }
    notifyListeners();

    return true;
  }

  void buildPaginatedDataGridRows() {
    dataGridRows = paginatedBills
        .map<DataGridRow>(
          (bill) => DataGridRow(
            cells: [
              DataGridCell<String>(
                columnName: 'eleve',
                value: bill.student,
              ),
              DataGridCell<String>(
                columnName: 'prof',
                value: bill.professor,
              ),
              DataGridCell<String>(
                columnName: 'type',
                value: bill.courseType,
              ),
              DataGridCell<String>(
                columnName: 'name',
                value: bill.courseName,
              ),
              DataGridCell<String>(
                columnName: 'total',
                value: bill.totalPrice,
              ),
              DataGridCell<String>(
                columnName: 'acquitted',
                value: bill.isAcquitted,
              ),
              DataGridCell<String>(
                columnName: 'dateIssue',
                value: bill.formatedDate,
              ),
            ],
          ),
        )
        .toList(growable: false);
  }
}
