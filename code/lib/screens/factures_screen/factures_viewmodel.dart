/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Factures
 
  Purpose: This file contains Factures View

  Author:  Christopher Lucas
  Date:    13.07.2022
*/
import 'package:ecomu_gestion/models/bill.dart';
import 'package:ecomu_gestion/services/ecomu_repository/ecomu_repository.dart';
import 'package:flutter/material.dart';

enum FactureState { initial, insert }

class FactureViewModel extends ChangeNotifier {
  var _state = FactureState.initial;
  final EcomuRepository _repository;
  Bill? _selectedBill;
  /* --- PARAMS FOR TABLE ---------------------------------------- */
  final int _rowsPerPage = 14;
  final double _dataPagerHeight = 60.0;
  final double _rowHeight = 49.0;
  final double _headerHeight = 56.0;

  FactureViewModel({
    required EcomuRepository repository,
  }) : _repository = repository;

  FactureState get state {
    return _state;
  }

  set setFactureState(FactureState value) {
    _state = value;
    notifyListeners();
  }

  Future<List<Bill>> getBills() {
    return _repository.fetchBills();
  }

  void back() {
    _state = FactureState.initial;
    notifyListeners();
  }

  Bill? get selectedBill {
    return _selectedBill;
  }

  set setSelectedBill(Bill? value) {
    _selectedBill = value;
  }

  get rowsPerPage {
    return _rowsPerPage;
  }

  get dataPagerHeight {
    return _dataPagerHeight;
  }

  get rowHeight {
    return _rowHeight;
  }

  get headerHeight {
    return _headerHeight;
  }

  get wishedTableHeight {
    return headerHeight + rowHeight * rowsPerPage;
  }
}
