import 'package:ecomu_gestion/models/person.dart';
import 'package:ecomu_gestion/models/student.dart';
import 'package:ecomu_gestion/services/ecomu_repository/ecomu_repository.dart';
import 'package:flutter/material.dart';

enum StudentState { initial, empty, modify }

class StudentViewModel extends ChangeNotifier {
  final EcomuRepository _repository;
  var _state = StudentState.initial;

  /// Currently selected [Person] by the user
  Student? _studentSelected;
  final searchController = TextEditingController();
  var _listStudents = <Student>[];
  var _listRecentStudents = <Student>[];

  StudentViewModel({required EcomuRepository repository})
      : _repository = repository;

  Future<List<Person>> getRepLeg() async {
    final result =
        await _repository.fetchRepLegsByStudent(_studentSelected!.idPerson!);
    if (result != null) {
      return result;
    }
    return [];
  }

  Future<List<Person>> getPayer() async {
    final result =
        await _repository.fetchPayersByStudent(_studentSelected!.idPerson!);
    if (result != null) {
      return result;
    }
    return [];
  }

  void initStudentViewModel() async {
    final result = await _repository.fetchStudents();
    if (result && _repository.students.isEmpty) {
      _state = StudentState.empty;
    }
    notifyListeners();
  }

  Future<bool> deleteStudent() async {
    final result = await _repository.deletePerson(_studentSelected!);
    if (result) {
      _removeSelectedPerson();
      _studentSelected = null;
      _state = StudentState.initial;
      notifyListeners();
    }
    return result;
  }

  void _removeSelectedPerson() {
    _listRecentStudents.remove(_studentSelected);
    _listStudents.remove(_studentSelected);
  }

  void searchAction(int index) {
    _studentSelected = _listStudents[index];
    addRecentSearch(index);
    searchController.clear();
    _listStudents = _listRecentStudents;
    notifyListeners();
  }

  void searchStudent(String query) {
    if (query.isNotEmpty) {
      _listStudents = _repository.students.where((person) {
        final name = person.lastName.toLowerCase();
        final firstname = person.firstName.toLowerCase();
        final input = query.toLowerCase();
        return "$name $firstname".contains(input) ||
            "$firstname $name".contains(input);
      }).toList();
    } else {
      _listStudents = _listRecentStudents;
    }
    notifyListeners();
  }

  void addRecentSearch(int index) {
    if (!_listRecentStudents.contains(_listStudents[index])) {
      _listRecentStudents.add(_listStudents[index]);
    }
  }

  void back2initial() {
    _state = StudentState.initial;
    _studentSelected = null;
    notifyListeners();
  }

  bool hasMatch() {
    return _repository.persons.isNotEmpty && searchController.text.isNotEmpty;
  }

  bool hasBirthday(Student student) {
    return student.birthday != null;
  }

  List<Student> get listStudents {
    final end = _listStudents.length > 100 ? 100 : _listStudents.length;
    return _listStudents.sublist(0, end);
  }

  get state {
    return _state;
  }

  Student? get selectedStudent {
    return _studentSelected;
  }

  set setSelectedStudent(Student? value) {
    _studentSelected = value;
    notifyListeners();
  }

  set setStudentState(StudentState value) {
    _state = value;
    notifyListeners();
  }
}
