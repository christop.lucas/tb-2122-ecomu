/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Students
 
  Purpose: This file contains the View Model for the Person view.

  Author:  Christopher Lucas
  Date:    06.06.2022
*/
import 'package:ecomu_gestion/models/person.dart';
import 'package:ecomu_gestion/models/student.dart';
import 'package:ecomu_gestion/services/ecomu_repository/ecomu_repository.dart';
import 'package:flutter/material.dart';

/// The different states of the [StudentViewModel]
enum StudentState { initial, failed, empty, detail, insert }

/// The View Model gets the necessary data for the [StudentView].
class StudentViewModelOLD extends ChangeNotifier {
  /// The selected [Student] by the user. The user details will be displayed.
  Person? _studentSelected;

  /// The current state [StudentState] of the viewModel.
  var _state = StudentState.initial;

  /// The list of [Student] that are currently displayed.
  var _listStudents = <Person>[];

  /// The list of all the [Student] in the local database.
  var _listAllStudents = <Person>[];

  /// The list of all the recents searched [Student]
  final _listRecentStudents = <Person>[];
/*
  /// The list of all the recents searched [RepLeg]
  final _listRepLegs = <RepLeg>[];

  /// The list of all the recents searched [RepLeg]
  final _listBillingPersons = <BillingPerson>[];
*/
  /// The repository with the data.
  final EcomuRepository _repository;

  /// Constructs the View Model by checking the local [Student] data.
  StudentViewModelOLD({required EcomuRepository repository})
      : _repository = repository;

  Future<bool> insertStudent(Person p) async {
    // Insert into external database
    await _repository.insertPerson(p);
    return true;
  }

  void addStudent(Person p) {
    _listAllStudents.add(p);
    notifyListeners();
  }

  /// Resets the recent list of students.
  void resetRecentsList() {
    _listStudents = _listRecentStudents;
    notifyListeners();
  }

  /// Searches for a [Student] in the list of all the students.
  /// The parameter [query] is the input of the search textfield.
  void searchStudent(String query) {
    if (query.isNotEmpty) {
      _listStudents = _listAllStudents.where((student) {
        final name = student.lastName!.toLowerCase();
        final firstname = student.firstName!.toLowerCase();
        final input = query.toLowerCase();
        return name.contains(input) || firstname.contains(input);
      }).toList();
    } else {
      _listStudents = _listRecentStudents;
    }
    notifyListeners();
  }

  StudentState get state {
    return _state;
  }

  set setStudentState(StudentState value) {
    _state = value;
    notifyListeners();
  }

  void addRecentSearch(int index) {
    if (!_listRecentStudents.contains(_listStudents[index])) {
      _listRecentStudents.add(_listStudents[index]);
    }
  }

  void removeRecentSearch(int index) {
    if (_listStudents[index] == _studentSelected) _studentSelected = null;
    _listRecentStudents.remove(_listStudents[index]);
    _listStudents = _listRecentStudents;
    notifyListeners();
  }

  List<Person> get listStudents {
    return _listStudents;
  }

  int get countStudents {
    return _listAllStudents.length;
  }

  Person? get selectedStudent {
    return _studentSelected;
  }

  void selectStudent(int index) async {
    _studentSelected = _listStudents[index];
    /*final id = _studentSelected!.idPerson;
    final inscriptions = await _repository.getInscriptionByIdPerson(id);
    if (inscriptions.isNotEmpty) {
      for (var i = 0; i < inscriptions.length; i++) {
        var ins = inscriptions[i];
        final repLegal = RepLeg(
            person: await _repository.getLocalPerson(ins.idLegRep) as Person);
        _listRepLegs.add(repLegal);
        //if (ins.idPayer != ins.idLegRep) {
        final billingPerson = BillingPerson(
            person: await _repository.getLocalPerson(ins.idPayer) as Person);
        _listBillingPersons.add(billingPerson);
        //}
      }
    }*/
  }

  List<String> studentAttributes() {
    var result = <String>[];
    var p = _studentSelected!;
    result.add(p.gender!);
    result.add(p.address!);
    result.add("${p.zipCode} ${p.location}");
    result.add(p.privateNumber!);
    result.add(p.profNumber!);
    return result;
  }

  /*List<String> repLegAttributes() {
    var result = <String>[];
    var p = _listRepLegs[0].person;

    result.add(p.gender);
    result.add(p.address);
    result.add("${p.zipCode} ${p.location}");
    result.add(p.privateNumber);
    result.add(p.profNumber);
    return result;
  }

  List<String> billingAttributes() {
    var result = <String>[];
    var p = _listBillingPersons[0].person;
    result.add(p.gender);
    result.add(p.address);
    result.add("${p.zipCode} ${p.location}");
    result.add(p.privateNumber);
    result.add(p.profNumber);
    return result;
  }*/
}
