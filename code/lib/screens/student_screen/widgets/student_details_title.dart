import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/student.dart';
import 'package:ecomu_gestion/screens/student_screen/student_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class StudentDetailsTitle extends StatelessWidget {
  /// [Person] that is selected by the user
  final Student student;

  /// Indicates if there is enough space for the big menu
  final bool bigTitle;

  /// Constructs the [PersonDetailsTitle] Widget
  const StudentDetailsTitle({
    Key? key,
    required this.student,
    required this.bigTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<StudentViewModel>();
    var birthday = "";
    if (viewModel.hasBirthday(student)) {
      var date = DateTime.parse(student.birthday!);
      birthday = DateFormat('dd MMMM yyyy').format(date);
    }
    if (bigTitle) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 3,
            child: Row(
              children: [
                Container(
                  width: 96,
                  height: 96,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Theme.of(context).colorScheme.surfaceVariant,
                  ),
                  child: Center(
                    child: Text(
                      student.lastName[0],
                      style: const TextStyle(fontSize: 48),
                    ),
                  ),
                ),
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 24.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Text(
                            "${student.firstName} ${student.lastName}",
                            style: const TextStyle(fontSize: 32),
                          ),
                        ),
                        FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Text(
                            (birthday.isEmpty)
                                ? student.email
                                : "$birthday, ${student.email}",
                            style: const TextStyle(
                              fontSize: 28,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              IconButton(
                onPressed: null, //viewModel.updateAction,
                icon: const Icon(Icons.edit),
                iconSize: 36.0,
                splashRadius: 8.0,
                color: Theme.of(context).colorScheme.primary,
              ),
              IconButton(
                onPressed: () => deleteAction(context),
                icon: const Icon(Icons.delete),
                iconSize: 36.0,
                splashRadius: 8.0,
                color: Theme.of(context).colorScheme.secondary,
              )
            ],
          ),
        ],
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${student.firstName} ${student.lastName}",
            style: const TextStyle(
              fontSize: 22,
            ),
          ),
          Row(
            children: [
              IconButton(
                onPressed: () {
                  viewModel.setStudentState = StudentState.modify;
                },
                icon: const Icon(Icons.edit),
                iconSize: 36.0,
                splashRadius: 8.0,
                color: Theme.of(context).colorScheme.primary,
              ),
              IconButton(
                onPressed: () => deleteAction(context),
                icon: const Icon(Icons.delete),
                iconSize: 36.0,
                splashRadius: 8.0,
                color: Theme.of(context).colorScheme.secondary,
              )
            ],
          ),
        ],
      );
    }
  }

  void deleteAction(BuildContext context) async {
    final result = await context.read<StudentViewModel>().deleteStudent();
    Future.delayed(Duration.zero, () {
      final local = AppLocalizations.of(context)!;
      final snackBar = SnackBar(
        content: Text(
          (result) ? local.person_delete_success : local.person_delete_failed,
          textAlign: TextAlign.end,
        ),
        backgroundColor: Theme.of(context).colorScheme.primary,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }
}
