import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/screens/student_screen/student_viewmodel.dart';
import 'package:ecomu_gestion/screens/student_screen/widgets/student_details_widget.dart';
import 'package:ecomu_gestion/screens/student_screen/widgets/student_empty_widget.dart';
import 'package:ecomu_gestion/screens/student_screen/widgets/student_search_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_column_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StudentContent extends StatelessWidget {
  const StudentContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<StudentViewModel>();
    // localization text
    var local = AppLocalizations.of(context)!;
    return Selector<StudentViewModel, StudentState>(
      builder: (context, value, child) {
        switch (value) {
          case StudentState.initial:
            return const EcomuSectionColumn(
              children: [
                StudentSearch(),
                StudentDetails(),
              ],
            );
          case StudentState.empty:
            return const StudentEmpty();
          case StudentState.modify:
            return Text("data");
        }
      },
      selector: (_, __) => viewModel.state,
    );
  }
}
