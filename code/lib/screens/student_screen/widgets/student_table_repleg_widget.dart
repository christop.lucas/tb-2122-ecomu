import 'package:ecomu_gestion/models/person.dart';
import 'package:ecomu_gestion/screens/student_screen/student_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class StudentTableRepLeg extends StatelessWidget {
  const StudentTableRepLeg({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<StudentViewModel>();
    return FutureBuilder<List<Person>>(
      future: viewModel.getRepLeg(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          var replegs = snapshot.data!;
          final source = RepLegDataSource(
            replegs: replegs,
          );
          return SfDataGrid(
            source: source,
            columns: [
              GridColumn(
                columnWidthMode: ColumnWidthMode.fill,
                columnName: 'firstname',
                label: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  alignment: Alignment.centerLeft,
                  child: const Text(
                    'Prénom',
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              GridColumn(
                columnWidthMode: ColumnWidthMode.fill,
                columnName: 'lastname',
                label: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  alignment: Alignment.centerLeft,
                  child: const Text(
                    'Nom',
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              GridColumn(
                columnWidthMode: ColumnWidthMode.fill,
                columnName: 'phone',
                label: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  alignment: Alignment.centerLeft,
                  child: const Text(
                    'Téléphone',
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              GridColumn(
                columnWidthMode: ColumnWidthMode.fill,
                columnName: 'email',
                label: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  alignment: Alignment.centerLeft,
                  child: const Text(
                    'Email',
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ],
          );
        }
        return Column();
      },
    );
  }
}

class RepLegDataSource extends DataGridSource {
  final List<Person> replegs;
  RepLegDataSource({
    required this.replegs,
  }) {
    buildPaginatedDataGridRows();
  }

  List<DataGridRow> dataGridRows = [];

  @override
  List<DataGridRow> get rows => dataGridRows;

  @override
  DataGridRowAdapter? buildRow(DataGridRow row) {
    return DataGridRowAdapter(
      cells: row.getCells().map<Widget>(
        (dataGridCell) {
          return Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(
              dataGridCell.value.toString(),
              overflow: TextOverflow.ellipsis,
            ),
          );
        },
      ).toList(),
    );
  }

  void buildPaginatedDataGridRows() {
    dataGridRows = replegs
        .map<DataGridRow>(
          (repleg) => DataGridRow(
            cells: [
              DataGridCell<String>(
                columnName: 'firstname',
                value: repleg.firstName,
              ),
              DataGridCell<String>(
                columnName: 'lastname',
                value: repleg.lastName,
              ),
              DataGridCell<String>(
                columnName: 'phone',
                value: repleg.privateNumber,
              ),
              DataGridCell<String>(
                columnName: 'email',
                value: repleg.email,
              ),
            ],
          ),
        )
        .toList(growable: false);
  }
}
