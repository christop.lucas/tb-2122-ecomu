/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Students
 
  Purpose: This file contains the Widget when no students

  Author:  Christopher Lucas
  Date:    02.06.2022
*/
import 'package:ecomu_gestion/app/viewmodel/app_viewmodel.dart';
import 'package:ecomu_gestion/app/viewmodel/menu_viewmodel.dart';
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StudentEmpty extends StatelessWidget {
  const StudentEmpty({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appViewModel = context.read<AppViewModel>();
    final menuViewModel = context.read<MenuViewModel>();
    final local = AppLocalizations.of(context)!;
    const style = TextStyle(
      fontSize: 28.0,
    );
    return EcomuSection(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            local.student_empty_text_1,
            style: style,
          ),
          Text(
            local.student_empty_text_2,
            style: style,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: ElevatedButton(
              onPressed: () {
                appViewModel.setAppState = AppState.inscription;
                menuViewModel.setSelectedItem = AppState.inscription.index;
              },
              style: ElevatedButton.styleFrom(
                onPrimary: Theme.of(context).colorScheme.onPrimary,
                primary: Theme.of(context).colorScheme.primary,
              ),
              child: Text(local.student_empty_btn_text),
            ),
          ),
        ],
      ),
    );
  }
}
