/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Students
 
  Purpose: This file contains the Widget of the details of a student.

  Author:  Christopher Lucas
  Date:    02.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/student.dart';
import 'package:ecomu_gestion/screens/student_screen/student_viewmodel.dart';
import 'package:ecomu_gestion/screens/student_screen/widgets/student_details_title.dart';
import 'package:ecomu_gestion/screens/student_screen/widgets/student_table_payer_widget.dart';
import 'package:ecomu_gestion/screens/student_screen/widgets/student_table_repleg_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_back_btn.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:ecomu_gestion/widgets/ecomu_info_person_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_placeholder_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_title_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StudentDetails extends StatelessWidget {
  final priorityDetails = 5;
  final priority = 3;
  const StudentDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<StudentViewModel>();
    final local = AppLocalizations.of(context)!;
    var lbl = local.person_attributes_list.split(local.splitter);
    return Selector<StudentViewModel, Student?>(
      builder: (context, student, child) {
        print("rebuild details");
        // Display Tooltip
        if (student == null) {
          return EcomuSection(
            priority: priority,
            padding: const EdgeInsets.all(16.0),
            child: EcomuSectionPlaceholder(
              text: local.student_empty_details,
              icon: Icons.school,
            ),
          );
        }
        return EcomuSection(
          priority: priority,
          padding: const EdgeInsets.all(16.0),
          child: LayoutBuilder(
            builder: (context, constraints) {
              final bigTitle = constraints.biggest.width > 700;
              return Column(
                children: [
                  // Return Button top left
                  ReturnButton(
                    action: () {
                      viewModel.setSelectedStudent = null;
                    },
                  ),
                  ContentSpacing(
                    priority: priorityDetails,
                    child: Column(
                      children: [
                        StudentDetailsTitle(
                          student: student,
                          bigTitle: bigTitle,
                        ),
                        const Divider(),
                      ],
                    ),
                  ),

                  Expanded(
                    child: SingleChildScrollView(
                      controller: ScrollController(),
                      child: ContentSpacing(
                        priority: priorityDetails,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          // ignore: prefer_const_literals_to_create_immutables
                          children: [
                            EcomuSectionTitle(
                              text: student.displayAddress,
                            ),
                            EcomuSectionTitle(
                              text: "Tél. primaire : ${student.privateNumber}",
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 16.0),
                              child: EcomuSectionTitle(
                                text: "Tél. secondaire : ${student.profNumber}",
                              ),
                            ),
                            const EcomuSectionTitle(
                              text: "Information représentant légal",
                            ),
                            StudentTableRepLeg(),
                            const EcomuSectionTitle(
                              text: "Information payeur",
                            ),
                            StudentTablePayer(),
                          ],
                        ),
                      ), //no const
                    ),
                  ),
                ],
              );
            },
          ),
        );
      },
      selector: (_, __) => viewModel.selectedStudent,
    );
  }
  /*
  const StudentTableRepLeg(),
  return EcomuSection(
      priority: 3,
      padding: const EdgeInsets.only(top: 16.0, left: 16.0),
      child: Selector<StudentViewModel, Student?>(
          builder: (context, student, child) {
            if (student == null) {
              return EcomuSectionPlaceholder(
                text: local.student_empty_details,
                icon: Icons.school,
              );
            } else {
              return LayoutBuilder(
                builder: (context, constraints) {
                  return Column(
                    children: [
                      // Return Button top left
                      Padding(
                        padding: const EdgeInsets.only(bottom: 16.0),
                        child: ReturnButton(
                          action: viewModel.back2initial,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal:
                              (constraints.biggest.width > 700) ? 72.0 : 32.0,
                        ),
                        child: StudentDetailsTitle(
                          student: student,
                          bigTitle: constraints.biggest.width > 700,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal:
                              (constraints.biggest.width > 700) ? 72.0 : 32.0,
                        ),
                        child: const Divider(),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal:
                              (constraints.biggest.width > 700) ? 72.0 : 32.0,
                        ),
                        child: Column(
                          children: const [
                            Text("Afficher les informations élèves"),
                            Text("Afficher les informations parent"),
                            Text("Afficher les informations payeur"),
                            Text("Afficher les informations inscription"),
                            Text("Afficher les informations cours"),
                          ],
                        ),
                      ),
                    ],
                  );
                },
              );
            }
          },
          selector: (_, __) => viewModel.selectedStudent),
    );
    /*if (s == null) {
      return EcomuSectionPlaceholder(
        text: local.student_empty_details,
        icon: Icons.school,
      );
    } else {
      String birthday = "";
      if (s.birthday != null) {
        var date = DateTime.parse(s.birthday!);
        birthday = DateFormat('dd MMMM yyyy').format(date);
      }
      return Padding(
        padding: const EdgeInsets.all(0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  width: 96,
                  height: 96,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Theme.of(context).colorScheme.surfaceVariant,
                  ),
                  child: Center(
                    child: Text(
                      s.lastName![0],
                      style: const TextStyle(fontSize: 48),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 24.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${s.lastName} ${s.firstName}",
                        style: const TextStyle(fontSize: 32),
                      ),
                      Text(
                        (birthday.isEmpty) ? s.email! : "$birthday, ${s.email}",
                        style: const TextStyle(
                          fontSize: 28,
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const Divider(),
            EcomuSectionTitle(
              text: local.student_title_info_student,
              isUnderline: true,
            ),
            /*EcomuInfoPerson(
              data: viewModel.studentAttributes(),
              dataLabel: dataLabel,
            ),*/
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    EcomuSectionTitle(
                      text: local.student_title_info_legRep,
                      isUnderline: true,
                    ),
                    /*EcomuInfoPerson(
                      data: [], //viewModel.repLegAttributes(),
                      dataLabel: dataLabel,
                    ),*/
                  ],
                ),
                const Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    EcomuSectionTitle(
                      text: local.student_title_info_billing,
                      isUnderline: true,
                    ),
                    /*EcomuInfoPerson(
                      data: [], //viewModel.billingAttributes(),
                      dataLabel: dataLabel,
                    ),*/
                  ],
                ),
              ],
            ),
          ],
        ),
      );*/
  }

  List<String> constructLabels(BuildContext buildContext) {
    var result = <String>[];
    var local = AppLocalizations.of(buildContext)!;
    result.add(local.student_gender);
    result.add(local.student_location);
    result.add("");
    result.add(local.student_tel_prive);
    result.add(local.student_tel_prof);
    return result;
  }*/
}
