import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/screens/student_screen/student_viewmodel.dart';
import 'package:ecomu_gestion/screens/student_screen/widgets/student_list_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_search_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_title_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StudentSearch extends StatelessWidget {
  const StudentSearch({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<StudentViewModel>();
    final local = AppLocalizations.of(context)!;
    return EcomuSection(
      priority: 1,
      child: Padding(
        padding: const EdgeInsets.only(
          left: 16.0,
          right: 16.0,
          top: 16.0,
        ),
        child: Column(
          children: [
            SearchField(
              controller: viewModel.searchController,
              text: local.student_search_text,
              action: viewModel.searchStudent,
            ),
            LayoutBuilder(
              builder: (context, constraints) {
                if (constraints.biggest.width < 250) {
                  return EcomuSectionTitle(
                    text: local.title_list_short,
                  );
                } else {
                  return EcomuSectionTitle(
                    text: local.title_list,
                  );
                }
              },
            ),
            const Divider(),
            Expanded(
              child: LayoutBuilder(
                builder: (context, constraints) {
                  if (constraints.biggest.width < 250) {
                    return const StudentList(type: 2);
                  } else if (constraints.biggest.width < 310) {
                    return const StudentList(type: 1);
                  } else {
                    return const StudentList(type: 0);
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
