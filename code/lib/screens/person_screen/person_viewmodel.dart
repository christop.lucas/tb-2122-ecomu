/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Person
 
  Purpose: This file contains the View Model for the Person view.

  Author:  Christopher Lucas
  Date:    02.06.2022
*/
import 'package:ecomu_gestion/models/person.dart';
import 'package:ecomu_gestion/services/ecomu_repository/ecomu_repository.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

enum PersonState { initial, empty, detail, insert, modify }

class PersonViewModel extends ChangeNotifier {
  var updated = false;

  /// Etat de la vue [PersonView]
  var _state = PersonState.initial;

  /// Repository contenant les données des personnes
  final EcomuRepository _repository;

  /// List of persons [Person] that are displayed
  var _listPersons = <Person>[];

  /// list of recently searached persons [Person]
  var _listRecentPersons = <Person>[];

  /// Currently selected [Person] by the user
  Person? _personSelected;

  /// The gender dropdown value
  String? _valueGender;

  /// The title dropdown value
  String? _valueTitle;
  // Text Controllers
  var searchController = TextEditingController();
  var lastNameController = TextEditingController();
  var firstNameController = TextEditingController();
  var emailController = TextEditingController();
  var birthdayController = TextEditingController();
  var addressController = TextEditingController();
  var zipCodeController = TextEditingController();
  var locationController = TextEditingController();
  var privNumberController = TextEditingController();
  var profNumberController = TextEditingController();
  var titleController = GlobalKey<FormFieldState>();
  var genderController = GlobalKey<FormFieldState>();

  void resetControllers() {
    firstNameController.clear();
    lastNameController.clear();
    addressController.clear();
    locationController.clear();
    privNumberController.clear();
    emailController.clear();
    birthdayController.clear();
    zipCodeController.clear();
    profNumberController.clear();
    titleController.currentState!.reset();
    genderController.currentState!.reset();
  }

  void setControllers() {
    if (_personSelected != null) {
      final p = _personSelected!;
      lastNameController = TextEditingController(text: p.lastName);
      firstNameController = TextEditingController(text: p.firstName);
      emailController = TextEditingController(text: p.email);
      birthdayController = TextEditingController(text: p.birthday);
      addressController = TextEditingController(text: p.address);
      zipCodeController = TextEditingController(
        text: p.zipCode.toString(),
      );
      locationController = TextEditingController(text: p.location);
      privNumberController = TextEditingController(text: p.privateNumber);
      profNumberController = TextEditingController(text: p.profNumber);
      _valueGender = p.gender;
      _valueTitle = p.title;
    }
  }

  void resetForm() {
    _valueGender = null;
    _valueTitle = null;
    _personSelected = null;
    resetControllers();
  }

  /// Constructs [PersonViewModel]
  PersonViewModel({required EcomuRepository repository})
      : _repository = repository;

  void initPersonViewModel() async {
    final result = await _repository.fetchPersons();
    if (result) {
      _listPersons = _repository.persons;
      if (_listPersons.isEmpty) {
        _state = PersonState.empty;
      } else {
        _state = PersonState.detail;
      }
    }
    notifyListeners();
  }

  Future<bool> insertPerson() async {
    var zip = int.parse(zipCodeController.text);
    final p = Person(
      idPerson: null,
      title: _valueTitle,
      firstName: firstNameController.text,
      lastName: lastNameController.text,
      gender: _valueGender,
      address: addressController.text,
      zipCode: zip,
      location: locationController.text,
      privateNumber: privNumberController.text,
      profNumber: profNumberController.text,
      email: emailController.text,
      birthday: birthdayController.text,
    );
    final result = await _repository.insertPerson(p);
    if (result) {
      //_state = PersonState.detail;
      resetForm();
    }
    return result;
  }

  Future<bool> updatePerson() async {
    var zip = int.parse(zipCodeController.text);
    final p = Person(
      idPerson: _personSelected!.idPerson,
      title: _valueTitle,
      firstName: firstNameController.text,
      lastName: lastNameController.text,
      gender: _valueGender,
      address: addressController.text,
      zipCode: zip,
      location: locationController.text,
      privateNumber: privNumberController.text,
      profNumber: profNumberController.text,
      email: emailController.text,
      birthday: birthdayController.text,
    );
    final result = await _repository.updatePerson(
      _personSelected!,
      p,
    );
    if (result) {
      _removeSelectedPerson();
      _personSelected = p;
      _listRecentPersons.add(p);
      updated = true;
      back();
    }
    return result;
  }

  Future<bool> deletePerson() async {
    final result = await _repository.deletePerson(_personSelected!);
    if (result) {
      _removeSelectedPerson();
      _personSelected = null;
      if (_repository.persons.isEmpty) {
        _state = PersonState.empty;
      } else {
        _state = PersonState.detail;
      }

      notifyListeners();
    }
    return result;
  }

  void _removeSelectedPerson() {
    _listRecentPersons.remove(_personSelected);
    _listPersons.remove(_personSelected);
    _repository.persons.remove(_personSelected);
  }

  void back() {
    _state = PersonState.detail;
    _valueGender = null;
    _valueTitle = null;
    resetControllers();
    notifyListeners();
  }

  void back2initial() {
    _state = PersonState.detail;
    _personSelected = null;
    notifyListeners();
  }

  bool hasMatch() {
    return _repository.persons.isNotEmpty && searchController.text.isNotEmpty;
  }

  void clearRecent() {
    _listPersons = <Person>[];
    _listRecentPersons = <Person>[];
    notifyListeners();
  }

  void insertAction() {
    _personSelected = null;
    _state = PersonState.insert;
    notifyListeners();
  }

  bool get hasSelected {
    return _personSelected != null;
  }

  void updateAction() {
    setControllers();
    _state = PersonState.modify;
    notifyListeners();
  }

  void searchAction(int index) {
    _state = PersonState.detail;
    _personSelected = _listPersons[index];
    addRecentSearch(index);
    searchController.clear();
    _listPersons = _listRecentPersons;
    notifyListeners();
  }

  /* --- Function related to search ------------------------------------ */

  void searchPerson(String query) {
    if (query.isNotEmpty) {
      _listPersons = _repository.persons.where((person) {
        final name = person.lastName!.toLowerCase();
        final firstname = person.firstName!.toLowerCase();
        final input = query.toLowerCase();
        return "$name $firstname".contains(input) ||
            "$firstname $name".contains(input);
      }).toList();
    } else {
      _listPersons = _listRecentPersons;
    }
    notifyListeners();
  }

  void addRecentSearch(int index) {
    if (!_listRecentPersons.contains(_listPersons[index])) {
      _listRecentPersons.add(_listPersons[index]);
    }
  }

  void removeRecentSearch(int index) {
    if (_listPersons[index] == _personSelected) _personSelected = null;
    _listRecentPersons.remove(_listPersons[index]);
    _listPersons = _listRecentPersons;
    notifyListeners();
  }

  Map<String, String> buildPersonInfo(List<String> labels, bool short) {
    if (short) {
      return <String, String>{
        labels[0]: _personSelected!.gender!,
        labels[1]: _personSelected!.address!,
        labels[2]: "${_personSelected!.zipCode} ${_personSelected!.location}",
        labels[3]: _personSelected!.privateNumber!,
        labels[4]: _personSelected!.profNumber!,
      };
    } else {
      return <String, String>{
        labels[0]: (_personSelected!.birthday == null)
            ? "No information"
            : DateFormat('dd MMMM yyyy')
                .format(DateTime.parse(_personSelected!.birthday!)),
        labels[1]: _personSelected!.email!,
        labels[2]: _personSelected!.gender!,
        labels[3]: _personSelected!.address!,
        labels[4]: "${_personSelected!.zipCode} ${_personSelected!.location}",
        labels[5]: _personSelected!.privateNumber!,
        labels[6]: _personSelected!.profNumber!,
      };
    }
  }

  /* --- Gettter and Setter for _personSelected ------------------------- */
  Person? get selectedPerson {
    return _personSelected;
  }

  void setSelectPerson(int index) {
    if (index == -1) {
      _personSelected = null;
    } else {
      _personSelected = _listPersons[index];
    }
    notifyListeners();
  }

  Person getPerson(int index) {
    return _listPersons[index];
  }

  /* --- Gettter and Setter for _state ---------------------------------- */
  PersonState get state {
    return _state;
  }

  set setPersonState(PersonState value) {
    _state = value;
    notifyListeners();
  }

  /* --- Gettter and Setter for _valueGender ------------------------- */
  String? get valueGender {
    return _valueGender;
  }

  set setValueGender(String? value) {
    _valueGender = value;
    if (value == null) notifyListeners();
  }

  /* --- Gettter and Setter for _valueTitle ------------------------- */
  String? get valueTitle {
    return _valueTitle;
  }

  set setValueTitle(String? value) {
    _valueTitle = value;
    if (value == null) notifyListeners();
  }

  List<Person> get listPersons {
    final end = (_listPersons.length > 100) ? 100 : _listPersons.length;
    return _listPersons.sublist(0, end);
  }

  bool get hasRecents {
    return _listRecentPersons.isNotEmpty;
  }

  bool hasBirthday(Person person) {
    return person.birthday != null;
  }
}
