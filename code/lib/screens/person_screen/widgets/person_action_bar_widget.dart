/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Person
 
  Purpose: This file contains the widget of the action bar for the person view.

  Author:  Christopher Lucas
  Date:    14.06.2022
*/
import 'package:ecomu_gestion/app/widgets/ecomu_top_bar_widget.dart';
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/screens/person_screen/person_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// Action bar for the [PersonView]
class PersonActionBar extends StatelessWidget {
  /// Constructs the [PersonActionBar]
  const PersonActionBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<PersonViewModel>();
    final local = AppLocalizations.of(context)!;
    return Selector<PersonViewModel, PersonState>(
      builder: (context, state, child) {
        if (state == PersonState.detail) {
          return EcomuTopBar(
            children: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 12.0),
                  child: ElevatedButton.icon(
                    onPressed: viewModel.insertAction,
                    icon: const Icon(Icons.add),
                    label: Text(
                      local.person_btn_new_text,
                    ),
                    style: ElevatedButton.styleFrom(
                      onPrimary: Theme.of(context).colorScheme.onPrimary,
                      primary: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                ),
                Selector<PersonViewModel, bool>(
                  builder: (context, value, child) {
                    return Padding(
                      padding: const EdgeInsets.only(right: 12.0),
                      child: TextButton.icon(
                        onPressed: value ? viewModel.updateAction : null,
                        icon: const Icon(Icons.edit_outlined),
                        label: Text(
                          local.btn_update_txt,
                        ),
                      ),
                    );
                  },
                  selector: (context, _) => viewModel.hasSelected,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 12.0),
                  child: Selector<PersonViewModel, bool>(
                    builder: (context, value, child) {
                      return TextButton.icon(
                        onPressed: value ? viewModel.deletePerson : null,
                        icon: const Icon(Icons.delete_outline),
                        label: Text(
                          local.btn_delete_txt,
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.hasSelected,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 12.0),
                  child: Selector<PersonViewModel, bool>(
                    builder: (context, value, child) {
                      return TextButton.icon(
                        onPressed: value ? viewModel.clearRecent : null,
                        icon: const Icon(Icons.list_outlined),
                        label: Text(
                          local.empty_recent_list,
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.hasRecents,
                  ),
                ),
                /**/
              ],
            ),
          );
        }
        return EcomuTopBar(
          children: Row(
            children: const [
              TextButton(
                onPressed: null,
                child: SizedBox.shrink(),
              ),
            ],
          ),
        );
      },
      selector: (_, __) => viewModel.state,
    );
  }
}
