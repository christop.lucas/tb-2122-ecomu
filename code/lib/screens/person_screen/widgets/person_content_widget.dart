/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Person
 
  Purpose: This Widget handles the different content Widget for the Person View.

  Author:  Christopher Lucas
  Date:    14.06.2022
*/
import 'package:ecomu_gestion/screens/person_screen/person_viewmodel.dart';
import 'package:ecomu_gestion/screens/person_screen/widgets/person_details_widget.dart';
import 'package:ecomu_gestion/screens/person_screen/widgets/person_empty_widget.dart';
import 'package:ecomu_gestion/screens/person_screen/widgets/person_insert_widget.dart';
import 'package:ecomu_gestion/screens/person_screen/widgets/person_search_widget.dart';
import 'package:ecomu_gestion/screens/person_screen/widgets/person_update_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_column_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PersonContent extends StatelessWidget {
  const PersonContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<PersonViewModel>();
    return Selector<PersonViewModel, PersonState>(
      builder: (context, value, child) {
        switch (value) {
          case PersonState.modify:
            return const PersonUpdate();
          case PersonState.insert:
            return const PersonInsert();
          case PersonState.detail:
            return const EcomuSectionColumn(
              children: [
                // Section on the right is the list of recently searched persons
                PersonSearch(),
                PersonDetails(),
              ],
            );
          case PersonState.initial:
            return EcomuSection(
              child: Column(),
            );
          case PersonState.empty:
            return const PersonEmpty();
        }
      },
      selector: (_, __) => viewModel.state,
    );
  }
}
