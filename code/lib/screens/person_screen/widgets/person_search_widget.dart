import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/screens/person_screen/person_viewmodel.dart';
import 'package:ecomu_gestion/widgets/ecomu_search_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_title_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'person_list_widget.dart';

class PersonSearch extends StatelessWidget {
  const PersonSearch({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<PersonViewModel>();
    final local = AppLocalizations.of(context)!;
    return EcomuSection(
      priority: 1,
      child: Padding(
        padding: const EdgeInsets.only(
          left: 16.0,
          right: 16.0,
          top: 16.0,
        ),
        child: Column(
          children: [
            SearchField(
              controller: viewModel.searchController,
              text: local.person_search_text,
              action: viewModel.searchPerson,
            ),
            LayoutBuilder(
              builder: (context, constraints) {
                if (constraints.biggest.width < 250) {
                  return EcomuSectionTitle(
                    text: local.title_list_short,
                  );
                } else {
                  return EcomuSectionTitle(
                    text: local.title_list,
                  );
                }
              },
            ),
            const Divider(),
            Expanded(
              child: LayoutBuilder(
                builder: (context, constraints) {
                  if (constraints.biggest.width < 250) {
                    return const PersonList(type: 2);
                  } else if (constraints.biggest.width < 310) {
                    return const PersonList(type: 1);
                  } else {
                    return const PersonList(type: 0);
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
