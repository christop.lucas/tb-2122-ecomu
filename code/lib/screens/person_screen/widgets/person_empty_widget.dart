import 'package:ecomu_gestion/screens/person_screen/person_viewmodel.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PersonEmpty extends StatelessWidget {
  const PersonEmpty({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const style = TextStyle(
      fontSize: 28.0,
    );
    return EcomuSection(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            "Le système ne possède pas de contact.",
            style: style,
          ),
          const Text(
            "Veuillez insérer un contact !",
            style: style,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: ElevatedButton(
              onPressed: context.read<PersonViewModel>().insertAction,
              style: ElevatedButton.styleFrom(
                onPrimary: Theme.of(context).colorScheme.onPrimary,
                primary: Theme.of(context).colorScheme.primary,
              ),
              child: const Text("Insérer un nouvel contact"),
            ),
          ),
        ],
      ),
    );
  }
}
