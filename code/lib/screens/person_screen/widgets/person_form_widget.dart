/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: General Widget
 
  Purpose: The Widget builds the correct form

  Author:  Christopher Lucas
  Date:    13.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/form_btn.dart';
import 'package:ecomu_gestion/screens/person_screen/person_viewmodel.dart';
import 'package:ecomu_gestion/services/ecomu_helper.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:ecomu_gestion/widgets/ecomu_date_picker_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_form/ecomu_user_form_field_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PersonForm extends StatefulWidget {
  /// List of action buttons at the bottom of form
  final List<FormBtn> actions;

  const PersonForm({
    Key? key,
    required this.actions,
  }) : super(key: key);

  @override
  State<PersonForm> createState() => _PersonFormState();
}

class _PersonFormState extends State<PersonForm> {
  final _key = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final viewModel = context.read<PersonViewModel>();
    return ContentSpacing(
      priority: 5,
      child: Form(
        key: _key,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 16.0,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: DropdownButtonFormField(
                      key: viewModel.titleController,
                      decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        labelText: local.form_user_title,
                      ),
                      hint: Text(local.form_person_dropdown_placeholder),
                      value: viewModel.selectedPerson == null
                          ? null
                          : viewModel.valueTitle,
                      onChanged: (String? value) {
                        viewModel.setValueTitle = value;
                      },
                      items: local.person_titles
                          .split(local.splitter)
                          .map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      validator: EcomuHelper.simpleValidator,
                    ),
                  ),
                  const Expanded(
                    flex: 2,
                    child: SizedBox.shrink(),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 16.0,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: EcomuFormField(
                      controller: viewModel.lastNameController,
                      hasLeftNeighbour: false,
                      text: local.form_user_lastname,
                      validator: EcomuHelper.simpleValidator,
                    ),
                  ),
                  Expanded(
                    child: EcomuFormField(
                      controller: viewModel.firstNameController,
                      hasLeftNeighbour: true,
                      text: local.form_user_firstname,
                      validator: EcomuHelper.simpleValidator,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 16.0,
              ),
              child: EcomuFormField(
                controller: viewModel.emailController,
                hasLeftNeighbour: false,
                text: local.form_user_email,
                validator: EcomuHelper.emailValidator,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
              child: Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: DropdownButtonFormField(
                      key: viewModel.genderController,
                      decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        labelText: local.form_user_gender,
                      ),
                      hint: Text(local.form_person_dropdown_placeholder),
                      value: (viewModel.selectedPerson == null)
                          ? null
                          : viewModel.valueGender,
                      onChanged: (String? value) {
                        viewModel.setValueGender = value;
                      },
                      items: local.person_genders
                          .split(local.splitter)
                          .map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      validator: EcomuHelper.simpleValidator,
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: DatePickerField(
                        title: local.form_user_birthday,
                        controller: viewModel.birthdayController,
                        validator: EcomuHelper.simpleValidator,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const Divider(),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: EcomuFormField(
                controller: viewModel.addressController,
                hasLeftNeighbour: false,
                text: local.form_user_address,
                validator: EcomuHelper.simpleValidator,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 16.0,
                bottom: 8.0,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: EcomuFormField(
                      controller: viewModel.zipCodeController,
                      hasLeftNeighbour: false,
                      text: local.form_user_zipcode,
                      validator: EcomuHelper.numberValidator,
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: EcomuFormField(
                      controller: viewModel.locationController,
                      hasLeftNeighbour: true,
                      text: local.form_user_location,
                      validator: EcomuHelper.simpleValidator,
                    ),
                  ),
                ],
              ),
            ),
            const Divider(),
            Padding(
              padding: const EdgeInsets.only(
                top: 8.0,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: EcomuFormField(
                      controller: viewModel.privNumberController,
                      hasLeftNeighbour: false,
                      text: local.form_user_profnumber,
                      validator: EcomuHelper.numberValidator,
                    ),
                  ),
                  Expanded(
                    child: EcomuFormField(
                      controller: viewModel.profNumberController,
                      hasLeftNeighbour: true,
                      text: local.form_user_privnumber,
                      validator: EcomuHelper.numberValidator,
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: Builder(
                  builder: (context) {
                    final action1 = widget.actions[0];
                    final action2 = widget.actions[1];
                    return Wrap(
                      spacing: 20,
                      children: [
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            onPrimary:
                                Theme.of(context).colorScheme.onSecondary,
                            primary: Theme.of(context).colorScheme.secondary,
                          ),
                          onPressed: action1.function,
                          child: Text(action1.text),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            onPrimary: Theme.of(context).colorScheme.onPrimary,
                            primary: Theme.of(context).colorScheme.primary,
                          ),
                          onPressed: () {
                            if (_key.currentState!.validate()) {
                              action2.function!();
                            }
                          },
                          child: Text(action2.text),
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
