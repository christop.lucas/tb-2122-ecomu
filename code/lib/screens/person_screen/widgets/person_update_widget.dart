/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Persons
 
  Purpose: This file contains the update person content.

  Author:  Christopher Lucas
  Date:    16.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/form_btn.dart';
import 'package:ecomu_gestion/screens/person_screen/person_viewmodel.dart';
import 'package:ecomu_gestion/screens/person_screen/widgets/person_form_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_back_btn.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_title_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PersonUpdate extends StatelessWidget {
  const PersonUpdate({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final viewModel = context.read<PersonViewModel>();
    return EcomuSection(
      priority: 3,
      padding: const EdgeInsets.only(top: 16.0, left: 16.0),
      child: Column(
        children: [
          // Return Button top left
          ReturnButton(
            action: viewModel.back,
          ),
          // Title of the page
          ContentSpacing(
            child: Column(
              children: [
                EcomuSectionTitle(
                  text: local.form_person_modify_title,
                ),
                const Divider(),
              ],
            ),
          ),
          // Form
          Expanded(
            child: SingleChildScrollView(
              controller: ScrollController(),
              child: FocusTraversalGroup(
                child: PersonForm(
                  actions: <FormBtn>[
                    FormBtn(
                      text: local.btn_cancel_txt,
                      function: viewModel.back,
                    ),
                    FormBtn(
                      text: local.btn_update_txt,
                      function: viewModel.updatePerson,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
