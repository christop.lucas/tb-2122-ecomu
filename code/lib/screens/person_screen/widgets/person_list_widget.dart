/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Persons
 
  Purpose: This file contains the Widget of the list of persons.

  Author:  Christopher Lucas
  Date:    07.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/person.dart';
import 'package:ecomu_gestion/screens/person_screen/person_viewmodel.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_placeholder_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PersonList extends StatefulWidget {
  /// Determines the look of the list
  final int type;

  /// Constructs [PersonList]
  const PersonList({
    Key? key,
    required this.type,
  }) : super(key: key);

  @override
  State<PersonList> createState() => _PersonListState();
}

class _PersonListState extends State<PersonList> {
  @override
  Widget build(BuildContext context) {
    // localization text
    var local = AppLocalizations.of(context)!;
    // var data = widget.persons;
    final viewModel = context.read<PersonViewModel>();
    // boolean for medium list
    final md = widget.type == 1;
    // boolean for large list
    final lg = widget.type == 0;
    return Selector<PersonViewModel, List<Person>>(
      builder: (context, persons, child) {
        if (persons.isNotEmpty) {
          return ListView.builder(
            itemCount: persons.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "${persons[index].lastName} ${persons[index].firstName}",
                  ),
                ),
                subtitle: (md || lg)
                    ? Align(
                        alignment: Alignment.centerLeft,
                        child: Text(persons[index].email!),
                      )
                    : null,
                leading: (lg)
                    ? Container(
                        width: 48,
                        height: 48,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Theme.of(context).colorScheme.surfaceVariant,
                        ),
                        child: Center(
                          child: Text(
                            persons[index].lastName![0],
                            style: const TextStyle(fontSize: 20),
                          ),
                        ),
                      )
                    : null,
                onTap: () {
                  viewModel.searchAction(index);
                },
              );
            },
          );
        } else {
          if (viewModel.hasMatch()) {
            return EcomuSectionPlaceholder(
              icon: Icons.search_off,
              text: local.match_text,
            );
          } else {
            return EcomuSectionPlaceholder(
              icon: Icons.access_time,
              text: local.person_recent_text,
            );
          }
        }
      },
      selector: (_, __) => viewModel.listPersons,
    );
  }
}
