/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Persons
 
  Purpose: This file contains the a person detail.

  Author:  Christopher Lucas
  Date:    16.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/person.dart';
import 'package:ecomu_gestion/screens/person_screen/person_viewmodel.dart';
import 'package:ecomu_gestion/screens/person_screen/widgets/person_details_title_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_back_btn.dart';
import 'package:ecomu_gestion/widgets/ecomu_info_person_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_placeholder_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PersonDetails extends StatelessWidget {
  final priorityDetails = 5;
  final priority = 3;
  const PersonDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    var lbl = local.person_attributes_list.split(local.splitter);
    final viewModel = context.read<PersonViewModel>();
    if (viewModel.updated) {
      viewModel.updated = false;
      Future.delayed(Duration.zero, () {
        final local = AppLocalizations.of(context)!;
        final snackBar = SnackBar(
          duration: const Duration(seconds: 6),
          behavior: SnackBarBehavior.fixed,
          content: Text(
            local.person_modify_success,
            style: TextStyle(
              color: Theme.of(context).colorScheme.primary,
            ),
            textAlign: TextAlign.start,
          ),
          backgroundColor: Theme.of(context).colorScheme.surface,
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
    }

    return Selector<PersonViewModel, Person?>(
      builder: (context, selectedPerson, child) {
        if (selectedPerson == null) {
          return EcomuSection(
            priority: 3,
            padding: const EdgeInsets.only(top: 16.0, left: 16.0),
            child: EcomuSectionPlaceholder(
              text: local.person_empty_details,
              icon: Icons.group,
            ),
          );
        }
        // Show details of selected person
        return EcomuSection(
          priority: priority,
          padding: const EdgeInsets.all(16.0),
          child: LayoutBuilder(
            builder: (context, constraints) {
              return Column(
                children: [
                  // Return Button top left
                  Padding(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: ReturnButton(
                      action: viewModel.back2initial,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal:
                          (constraints.biggest.width > 700) ? 72.0 : 32.0,
                    ),
                    child: PersonDetailsTitle(
                      person: selectedPerson,
                      bigTitle: constraints.biggest.width > 700,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal:
                          (constraints.biggest.width > 700) ? 72.0 : 32.0,
                    ),
                    child: const Divider(),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal:
                            (constraints.biggest.width > 700) ? 72.0 : 32.0,
                      ),
                      child: SingleChildScrollView(
                        controller: ScrollController(),
                        child: EcomuInfoPerson(
                          bigInfo: constraints.biggest.width > 700,
                          dataLabel: lbl,
                        ),
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        );
      },
      selector: (_, __) => viewModel.selectedPerson,
    );
  }
}
