import 'package:ecomu_gestion/models/person.dart';
import 'package:ecomu_gestion/models/professor.dart';
import 'package:ecomu_gestion/services/ecomu_repository/ecomu_repository.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

enum TeacherState { initial, detail, empty, insert, modify }

class TeacherViewModel extends ChangeNotifier {
  /// Repository with the [Professor] data
  final EcomuRepository _repository;

  /// State of the [TeacherView]
  var _state = TeacherState.initial;

  /// All the displayed [Professor]
  var _listProfessors = <Professor>[];

  Professor? _selectedProfessor;

  Person? _selectedPerson;

  /// The gender dropdown value
  String? _valueGender;

  /// The title dropdown value
  String? _valueTitle;

  bool _toggle = false;

  /// Controls the search Text Field
  final searchController = TextEditingController();

  /// Controls the last name Text Field
  final lastNameController = TextEditingController();

  /// Controls the first name Text Field
  final firstNameController = TextEditingController();

  /// Controls the email Text Field
  final emailController = TextEditingController();

  /// Controls the birthday Text Field
  final birthdayController = TextEditingController();

  /// Controls the address Field
  final addressController = TextEditingController();

  /// Controls the zip code Text Field
  final zipCodeController = TextEditingController();

  /// Controls the location Text Field
  final locationController = TextEditingController();

  /// Controls the private number Text Field
  final privNumberController = TextEditingController();

  /// Controls the professional number Text Field
  final profNumberController = TextEditingController();

  /// Controls the salary Text Field
  final salaryController = TextEditingController();

  /// Controls the working rate Text Field
  final workingRateController = TextEditingController();

  /// Controls the gender Dropdown
  final genderController = GlobalKey<FormFieldState>();

  /// Controls the title Dropdown
  final titleController = GlobalKey<FormFieldState>();

  TeacherViewModel({required EcomuRepository repository})
      : _repository = repository;

  /// Initialises the [TeacherViewModel] by fetching the professor
  /// data and setting the correct State.
  ///
  /// If there is no [Professor], the empty [TeacherState] will be set.
  /// If there is one or more [Professor], the detail [TeacherState]
  /// will be set.
  void init() async {
    final result = await _repository.fetchProfessors();
    if (result) {
      _listProfessors = _repository.professors;
      if (_listProfessors.isEmpty) {
        _state = TeacherState.empty;
      } else {
        _state = TeacherState.detail;
      }
    }
    notifyListeners();
  }

  /// Search function that is executed when the [SearchField] is modified.
  void searchProfessor(String query) {
    final allProfessor = _repository.professors;
    if (query.isNotEmpty) {
      _listProfessors = allProfessor.where((student) {
        final name = student.lastName.toLowerCase();
        final firstname = student.firstName.toLowerCase();
        final input = query.toLowerCase();
        return "$name $firstname".contains(input) ||
            "$firstname $name".contains(input);
      }).toList();
    } else {
      _listProfessors = allProfessor;
    }
    notifyListeners();
  }

  Future<bool> insertProfessor() async {
    final p = Person(
      idPerson: _selectedPerson?.idPerson,
      title: _valueTitle!,
      firstName: firstNameController.text,
      lastName: lastNameController.text,
      gender: _valueGender!,
      address: addressController.text,
      zipCode: int.parse(zipCodeController.text),
      location: locationController.text,
      privateNumber: privNumberController.text,
      profNumber: profNumberController.text,
      email: emailController.text,
      birthday: birthdayController.text,
    );
    final result = await _repository.insertProfessor(
      p,
      double.parse(workingRateController.text) / 100.0,
      double.parse(salaryController.text),
    );
    if (result) {
      _listProfessors = _repository.professors;
      resetForm();
    }
    return result;
  }

  Future<bool> updateProfessor() async {
    final p = Person(
      idPerson: _selectedProfessor?.idPerson,
      title: _valueTitle!,
      firstName: firstNameController.text,
      lastName: lastNameController.text,
      gender: _valueGender!,
      address: addressController.text,
      zipCode: int.parse(zipCodeController.text),
      location: locationController.text,
      privateNumber: privNumberController.text,
      profNumber: profNumberController.text,
      email: emailController.text,
      birthday: birthdayController.text,
    );
    final workingRate = double.parse(workingRateController.text) / 100.0;
    final salary = double.parse(salaryController.text);
    final idProfessor = _selectedProfessor!.idProfessor!;
    final result = await _repository.updateProfessor(
      p,
      workingRate,
      salary,
      idProfessor,
    );
    if (result) {
      _listProfessors = _repository.professors;
      _selectedProfessor = Professor.convertFromPerson(
        p,
        salary,
        workingRate,
        idProfessor,
      );
      _state = TeacherState.detail;
      notifyListeners();
    }

    return result;
  }

  Future<bool> deleteProfessor() async {
    final result =
        await _repository.deleteProfessor(_selectedProfessor!.idProfessor!);
    if (result) {
      _removeSelectedProfessor();
      _selectedProfessor = null;
      notifyListeners();
    }
    return result;
  }

  void _removeSelectedProfessor() {
    _listProfessors.remove(_selectedProfessor);
    _repository.fetchProfessors();
  }

  List<Professor> get listProfessors {
    return _listProfessors;
  }

  int get countProfessors {
    return _listProfessors.length;
  }

  Professor? get selectedProfessor {
    return _selectedProfessor;
  }

  set setSelectedProfessor(Professor? value) {
    _selectedProfessor = value;
    notifyListeners();
  }

  TeacherState get state {
    return _state;
  }

  set setTeacherState(TeacherState value) {
    _state = value;
    notifyListeners();
  }

  void actionBarInsertAction() {
    _state = TeacherState.insert;
    notifyListeners();
  }

  Function()? actionBarDeleteAction() {
    return deleteProfessor;
  }

  void actionBarUpdateAction() {
    setControllers(null);
    _state = TeacherState.modify;
    notifyListeners();
  }

  Map<String, String> buildTeacherInfo(List<String> labels, bool short) {
    if (short) {
      return <String, String>{
        labels[0]: (_selectedProfessor!.workingRate! * 100).toString(),
        labels[1]: "${_selectedProfessor!.salary} CHF",
        labels[2]: _selectedProfessor!.gender!,
        labels[3]: _selectedProfessor!.address!,
        labels[4]:
            "${_selectedProfessor!.zipCode} ${_selectedProfessor!.location}",
        labels[5]: _selectedProfessor!.privateNumber!,
        labels[6]: _selectedProfessor!.profNumber!,
      };
    } else {
      return <String, String>{
        labels[0]: _selectedProfessor!.workingRate.toString(),
        labels[1]: _selectedProfessor!.salary.toString(),
        labels[2]: (_selectedProfessor!.birthday == null)
            ? "No information"
            : DateFormat('dd MMMM yyyy')
                .format(DateTime.parse(_selectedProfessor!.birthday!)),
        labels[3]: _selectedProfessor!.email!,
        labels[4]: _selectedProfessor!.gender!,
        labels[5]: _selectedProfessor!.address!,
        labels[6]:
            "${_selectedProfessor!.zipCode} ${_selectedProfessor!.location}",
        labels[7]: _selectedProfessor!.privateNumber!,
        labels[8]: _selectedProfessor!.profNumber!,
      };
    }
  }

  String formatBirthday() {
    var birthday = "";
    if (_selectedProfessor!.birthday != null) {
      var date = DateTime.parse(_selectedProfessor!.birthday!);
      birthday = DateFormat('dd MMMM yyyy').format(date);
    }
    return birthday;
  }

  bool hasMatch() {
    return _repository.professors.isNotEmpty &&
        searchController.text.isNotEmpty;
  }

  void searchAction(int index) {
    _selectedProfessor = _listProfessors[index];
    searchController.clear();
    notifyListeners();
  }

  /* --- FORM FUNCTIONS ---------------------------- */
  void back() {
    if (_repository.professors.isEmpty) {
      _state = TeacherState.empty;
    } else {
      _state = TeacherState.detail;
    }
    resetForm();
    notifyListeners();
  }

  void resetForm() {
    _valueGender = null;
    _valueTitle = null;
    _selectedPerson = null;
    resetControllers();
  }

  void resetControllers() {
    firstNameController.clear();
    lastNameController.clear();
    addressController.clear();
    locationController.clear();
    privNumberController.clear();
    emailController.clear();
    birthdayController.clear();
    zipCodeController.clear();
    profNumberController.clear();
    salaryController.clear();
    workingRateController.clear();
    genderController.currentState!.reset();
    titleController.currentState!.reset();
  }

  void choosePerson(Person p) {
    setControllers(p);
    _selectedPerson = p;
    notifyListeners();
  }

  void setControllers(Person? p) {
    if (p != null) {
      lastNameController.text = p.lastName!;
      firstNameController.text = p.firstName!;
      emailController.text = p.email!;
      birthdayController.text = p.birthday ?? "";
      addressController.text = p.address!;
      zipCodeController.text = p.zipCode.toString();
      locationController.text = p.location!;
      privNumberController.text = p.privateNumber!;
      profNumberController.text = p.profNumber!;
      _valueGender = p.gender;
      _valueTitle = p.title;
    } else {
      lastNameController.text = _selectedProfessor!.lastName;
      firstNameController.text = _selectedProfessor!.firstName;
      emailController.text = _selectedProfessor!.email!;
      birthdayController.text = _selectedProfessor!.birthday ?? "";
      addressController.text = _selectedProfessor!.address!;
      zipCodeController.text = _selectedProfessor!.zipCode.toString();
      locationController.text = _selectedProfessor!.location!;
      privNumberController.text = _selectedProfessor!.privateNumber!;
      profNumberController.text = _selectedProfessor!.profNumber!;
      salaryController.text = _selectedProfessor!.salary.toString();
      workingRateController.text =
          (_selectedProfessor!.workingRate! * 100).toString();
      _valueGender = _selectedProfessor!.gender;
      _valueTitle = _selectedProfessor!.title;
    }
  }

  /* --- Gettter and Setter for _valueGender ------------------------- */
  String? get valueGender {
    return _valueGender;
  }

  set setValueGender(String? value) {
    _valueGender = value;
    if (value == null) notifyListeners();
  }

  /* --- Gettter and Setter for _valueTitle ------------------------- */
  String? get valueTitle {
    return _valueTitle;
  }

  set setValueTitle(String? value) {
    _valueTitle = value;
    if (value == null) notifyListeners();
  }

  bool get toggle {
    return _toggle;
  }

  set setToggle(bool value) {
    if (value || _selectedPerson != null) resetControllers();
    _valueGender = null;
    _valueTitle = null;
    _selectedPerson = null;
    _toggle = value;
    notifyListeners();
  }

  Person? get selectedPerson {
    return _selectedPerson;
  }
}
