import 'package:ecomu_gestion/screens/teacher_screen/teacher_viewmodel.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TeacherEmpty extends StatelessWidget {
  const TeacherEmpty({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const style = TextStyle(
      fontSize: 28.0,
    );
    return EcomuSection(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            "Le système ne possède pas de professeur.",
            style: style,
          ),
          const Text(
            "Veuillez insérer un professeur !",
            style: style,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: ElevatedButton(
              onPressed: context.read<TeacherViewModel>().actionBarInsertAction,
              style: ElevatedButton.styleFrom(
                onPrimary: Theme.of(context).colorScheme.onPrimary,
                primary: Theme.of(context).colorScheme.primary,
              ),
              child: const Text("Insérer un nouveau professeur"),
            ),
          ),
        ],
      ),
    );
  }
}
