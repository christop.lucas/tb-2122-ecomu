/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Person
 
  Purpose: This Widget handles the different content Widget for the Person View.

  Author:  Christopher Lucas
  Date:    14.06.2022
*/
import 'package:ecomu_gestion/screens/teacher_screen/teacher_viewmodel.dart';
import 'package:ecomu_gestion/screens/teacher_screen/widgets/teacher_details_widget.dart';
import 'package:ecomu_gestion/screens/teacher_screen/widgets/teacher_insert_widget.dart';
import 'package:ecomu_gestion/screens/teacher_screen/widgets/teacher_update_widget.dart';
import 'package:ecomu_gestion/screens/teacher_screen/widgets/teacher_empty_widget.dart';
import 'package:ecomu_gestion/screens/teacher_screen/widgets/teacher_search_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_column_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TeacherContent extends StatelessWidget {
  const TeacherContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<TeacherViewModel>();
    return Selector<TeacherViewModel, TeacherState>(
      builder: (context, state, child) {
        switch (state) {
          case TeacherState.initial:
            return EcomuSection(
              child: Column(),
            );
          case TeacherState.detail:
            return const EcomuSectionColumn(
              children: [
                TeacherSearch(),
                TeacherDetails(),
              ],
            );
          case TeacherState.empty:
            return const TeacherEmpty();
          case TeacherState.modify:
            return const TeacherUpdate();
          case TeacherState.insert:
            return const TeacherInsert();
        }
      },
      selector: (_, __) => viewModel.state,
    );
  }
}
