/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Teacher
 
  Purpose: This file contains the widget of the action bar for the teacher view.

  Author:  Christopher Lucas
  Date:    14.06.2022
*/
import 'package:ecomu_gestion/app/widgets/ecomu_top_bar_widget.dart';
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/professor.dart';
import 'package:ecomu_gestion/screens/teacher_screen/teacher_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// Action bar for the [PersonView]
class TeacherActionBar extends StatelessWidget {
  /// Constructs the [PersonActionBar]
  const TeacherActionBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<TeacherViewModel>();
    final local = AppLocalizations.of(context)!;
    return EcomuTopBar(
      children: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 12.0),
            child: ElevatedButton.icon(
              onPressed: viewModel.actionBarInsertAction,
              icon: const Icon(Icons.add),
              label: Text(
                local.teacher_btn_new_text,
              ),
              style: ElevatedButton.styleFrom(
                onPrimary: Theme.of(context).colorScheme.onPrimary,
                primary: Theme.of(context).colorScheme.primary,
              ),
            ),
          ),
          Selector<TeacherViewModel, Professor?>(
            builder: (context, professor, child) {
              return Padding(
                padding: const EdgeInsets.only(right: 12.0),
                child: TextButton.icon(
                  onPressed: (professor != null)
                      ? () {} //viewModel.actionBarUpdateAction
                      : null,
                  icon: const Icon(Icons.edit_outlined),
                  label: Text(
                    local.btn_update_txt,
                  ),
                ),
              );
            },
            selector: (_, __) => viewModel.selectedProfessor,
          ),
          Selector<TeacherViewModel, Professor?>(
            builder: (context, professor, child) {
              return Padding(
                padding: const EdgeInsets.only(right: 12.0),
                child: TextButton.icon(
                  onPressed:
                      (professor != null) ? viewModel.deleteProfessor : null,
                  icon: const Icon(Icons.delete_outline),
                  label: Text(
                    local.btn_delete_txt,
                  ),
                ),
              );
            },
            selector: (_, __) => viewModel.selectedProfessor,
          ),
        ],
      ),
    );
  }
}
