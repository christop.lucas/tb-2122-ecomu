/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Persons
 
  Purpose: This file contains the update person content.

  Author:  Christopher Lucas
  Date:    16.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/form_btn.dart';

import 'package:ecomu_gestion/screens/teacher_screen/teacher_viewmodel.dart';
import 'package:ecomu_gestion/screens/teacher_screen/widgets/teacher_form_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_back_btn.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_title_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TeacherUpdate extends StatelessWidget {
  const TeacherUpdate({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final viewModel = context.read<TeacherViewModel>();
    return EcomuSection(
      priority: 3,
      padding: const EdgeInsets.only(top: 16.0, left: 16.0),
      child: Column(
        children: [
          // Return Button top left
          ReturnButton(
            action: viewModel.back,
          ),
          // Title of the page
          ContentSpacing(
            priority: 5,
            child: Column(
              children: [
                EcomuSectionTitle(
                  padding: const EdgeInsets.all(0.0),
                  text: local.form_teacher_modify_title,
                ),
                const Divider(),
              ],
            ),
          ),
          // Form
          Expanded(
            child: SingleChildScrollView(
              controller: ScrollController(),
              child: FocusTraversalGroup(
                child: Builder(
                  builder: (context) {
                    final lst = <FormBtn>[
                      FormBtn(
                        text: local.btn_cancel_txt,
                        function: viewModel.back,
                      ),
                      FormBtn(
                        text: local.btn_update_txt,
                        function: () {
                          updateAction(context);
                        },
                      )
                    ];
                    return ContentSpacing(
                      priority: 5,
                      child: TeacherForm(
                        actions: lst,
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void updateAction(BuildContext context) async {
    final result = await context.read<TeacherViewModel>().updateProfessor();

    Future.delayed(Duration.zero, () {
      final local = AppLocalizations.of(context)!;
      final snackBar = SnackBar(
        content: Text(
          (result) ? local.teacher_modify_success : local.teacher_modify_failed,
          style: const TextStyle(color: Colors.white),
          textAlign: TextAlign.end,
        ),
        backgroundColor: (Colors.black),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }
}
