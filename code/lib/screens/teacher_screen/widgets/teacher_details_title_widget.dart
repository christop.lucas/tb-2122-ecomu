/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Teacher
 
  Purpose: This file contains the Title Widget for the Teacher Details.

  Author:  Christopher Lucas
  Date:    14.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/person.dart';
import 'package:ecomu_gestion/models/professor.dart';
import 'package:ecomu_gestion/screens/teacher_screen/teacher_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TeacherDetailsTitle extends StatelessWidget {
  /// [Person] that is selected by the user
  final Professor professor;

  /// Indicates if there is enough space for the big menu
  final bool bigTitle;

  /// Constructs the [PersonDetailsTitle] Widget
  const TeacherDetailsTitle({
    Key? key,
    required this.professor,
    required this.bigTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<TeacherViewModel>();
    final birthday = viewModel.formatBirthday();
    if (bigTitle) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 3,
            child: Row(
              children: [
                Container(
                  width: 64,
                  height: 64,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Theme.of(context).colorScheme.surfaceVariant,
                  ),
                  child: Center(
                    child: Text(
                      professor.lastName[0],
                      style: const TextStyle(fontSize: 32),
                    ),
                  ),
                ),
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 24.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Text(
                            "${professor.lastName} ${professor.firstName}",
                            style: const TextStyle(fontSize: 26),
                          ),
                        ),
                        FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Text(
                            (birthday.isEmpty)
                                ? professor.email!
                                : "$birthday, ${professor.email}",
                            style: const TextStyle(
                              fontSize: 22,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              IconButton(
                onPressed: viewModel.actionBarUpdateAction,
                icon: const Icon(Icons.edit),
                iconSize: 36.0,
                splashRadius: 8.0,
                color: Theme.of(context).colorScheme.primary,
              ),
              IconButton(
                onPressed: () {}, //() => deleteAction(context),
                icon: const Icon(Icons.delete),
                iconSize: 36.0,
                splashRadius: 8.0,
                color: Theme.of(context).colorScheme.secondary,
              )
            ],
          ),
        ],
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${professor.firstName} ${professor.lastName}",
            style: const TextStyle(
              fontSize: 22,
            ),
          ),
          Row(
            children: [
              IconButton(
                onPressed: viewModel.actionBarUpdateAction,
                icon: const Icon(Icons.edit),
                iconSize: 36.0,
                splashRadius: 8.0,
                color: Theme.of(context).colorScheme.primary,
              ),
              IconButton(
                onPressed: () {}, //() => deleteAction(context),
                icon: const Icon(Icons.delete),
                iconSize: 36.0,
                splashRadius: 8.0,
                color: Theme.of(context).colorScheme.secondary,
              )
            ],
          ),
        ],
      );
    }
  }

  void deleteAction(BuildContext context) async {
    final result = await context.read<TeacherViewModel>().deleteProfessor();
    Future.delayed(Duration.zero, () {
      final local = AppLocalizations.of(context)!;
      final snackBar = SnackBar(
        content: Text(
          (result) ? local.teacher_delete_success : local.teacher_delete_failed,
          style: const TextStyle(color: Colors.white),
          textAlign: TextAlign.end,
        ),
        backgroundColor: (Colors.black),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }
}
