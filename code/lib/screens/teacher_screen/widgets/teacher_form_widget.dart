/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: General Widget
 
  Purpose: The Widget builds the correct form

  Author:  Christopher Lucas
  Date:    13.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/form_btn.dart';
import 'package:ecomu_gestion/screens/teacher_screen/teacher_viewmodel.dart';
import 'package:ecomu_gestion/widgets/ecomu_date_picker_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_form/ecomu_user_form_field_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TeacherForm extends StatefulWidget {
  /// List of action buttons at the bottom of form
  final List<FormBtn> actions;

  const TeacherForm({
    Key? key,
    required this.actions,
  }) : super(key: key);

  @override
  State<TeacherForm> createState() => _TeacherFormState();
}

class _TeacherFormState extends State<TeacherForm> {
  final _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final viewModel = context.read<TeacherViewModel>();
    return Form(
      key: _key,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              top: 16.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: DropdownButtonFormField(
                    key: viewModel.titleController,
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      labelText: local.form_user_title,
                    ),
                    value: viewModel.valueTitle,
                    hint: Text(local.form_person_dropdown_placeholder),
                    onChanged: (String? value) {
                      viewModel.setValueTitle = value;
                    },
                    items: local.person_titles
                        .split(local.splitter)
                        .map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    validator: simpleValidator,
                  ),
                ),
                const Expanded(
                  flex: 2,
                  child: SizedBox.shrink(),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: EcomuFormField(
                    controller: viewModel.lastNameController,
                    hasLeftNeighbour: false,
                    text: local.form_user_lastname,
                    validator: simpleValidator,
                  ),
                ),
                Expanded(
                  child: EcomuFormField(
                    controller: viewModel.firstNameController,
                    hasLeftNeighbour: true,
                    text: local.form_user_firstname,
                    validator: simpleValidator,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16.0,
            ),
            child: EcomuFormField(
              controller: viewModel.emailController,
              hasLeftNeighbour: false,
              text: local.form_user_email,
              validator: emailValidator,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: DropdownButtonFormField(
                    key: viewModel.genderController,
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      labelText: local.form_user_gender,
                    ),
                    hint: Text(local.form_person_dropdown_placeholder),
                    value: viewModel.valueGender,
                    onChanged: (String? value) {
                      viewModel.setValueGender = value;
                    },
                    items: local.person_genders
                        .split(local.splitter)
                        .map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    validator: simpleValidator,
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: DatePickerField(
                      title: local.form_user_birthday,
                      controller: viewModel.birthdayController,
                      validator: simpleValidator,
                    ),
                  ),
                ),
              ],
            ),
          ),
          const Divider(),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: EcomuFormField(
              controller: viewModel.addressController,
              hasLeftNeighbour: false,
              text: local.form_user_address,
              validator: simpleValidator,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16.0,
              bottom: 8.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: EcomuFormField(
                    controller: viewModel.zipCodeController,
                    hasLeftNeighbour: false,
                    text: local.form_user_zipcode,
                    validator: numberValidator,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: EcomuFormField(
                    controller: viewModel.locationController,
                    hasLeftNeighbour: true,
                    text: local.form_user_location,
                    validator: simpleValidator,
                  ),
                ),
              ],
            ),
          ),
          const Divider(),
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 8.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: EcomuFormField(
                    controller: viewModel.privNumberController,
                    hasLeftNeighbour: false,
                    text: local.form_user_profnumber,
                    validator: numberValidator,
                  ),
                ),
                Expanded(
                  child: EcomuFormField(
                    controller: viewModel.profNumberController,
                    hasLeftNeighbour: true,
                    text: local.form_user_privnumber,
                    validator: numberValidator,
                  ),
                ),
              ],
            ),
          ),
          const Divider(),
          Padding(
            padding: const EdgeInsets.only(
              top: 8.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: EcomuFormField(
                    controller: viewModel.salaryController,
                    hasLeftNeighbour: false,
                    text: local.form_teacher_salary,
                    validator: numberValidator,
                  ),
                ),
                Expanded(
                  child: EcomuFormField(
                    controller: viewModel.workingRateController,
                    hasLeftNeighbour: true,
                    text: local.form_teacher_working_rate,
                    validator: rateValidator,
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Builder(
                builder: (context) {
                  final action1 = widget.actions[0];
                  final action2 = widget.actions[1];
                  return Wrap(
                    spacing: 20,
                    children: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          onPrimary: Theme.of(context).colorScheme.onSecondary,
                          primary: Theme.of(context).colorScheme.secondary,
                        ),
                        onPressed: action1.function,
                        child: Text(action1.text),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          onPrimary: Theme.of(context).colorScheme.onPrimary,
                          primary: Theme.of(context).colorScheme.primary,
                        ),
                        onPressed: () {
                          if (_key.currentState!.validate()) {
                            action2.function!();
                          }
                        },
                        child: Text(action2.text),
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  String? simpleValidator(String? value) {
    return (value == null || value.isEmpty)
        ? 'Veuillez renseigner une valeur'
        : null;
  }

  String? emailValidator(String? value) {
    String? result;
    if (value == null || value.isEmpty) {
      result = 'Veuillez renseigner une valeur';
    } else {
      var emailValid = RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(value);
      if (!emailValid) {
        result = "L'email n'est pas valide";
      }
    }
    return result;
  }

  String? numberValidator(String? value) {
    String? result;
    if (value == null || value.isEmpty) {
      result = 'Veuillez renseigner une valeur';
    } else {
      var fieldValid = RegExp(r"^[0-9.]*$").hasMatch(value);
      if (!fieldValid) {
        result = "Ce champs ne contenir seulement des numéros";
      }
    }
    return result;
  }

  String? rateValidator(String? value) {
    String? result;
    if (value == null || value.isEmpty) {
      result = 'Veuillez renseigner une valeur';
    } else {
      var fieldValid = RegExp(r"^[0-9.]*$").hasMatch(value);
      if (!fieldValid) {
        result = "Ce champs doit contenir seulement des numéros";
      } else {
        var rate = double.parse(value);
        if (rate < 0 || rate > 100) {
          result = "Ce champs doit être une valeur entre 0 et 100";
        }
      }
    }
    return result;
  }
}
