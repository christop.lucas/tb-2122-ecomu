import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/professor.dart';
import 'package:ecomu_gestion/screens/teacher_screen/teacher_viewmodel.dart';
import 'package:ecomu_gestion/widgets/ecomu_list_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_search_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_title_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TeacherSearch extends StatelessWidget {
  const TeacherSearch({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final viewModel = context.read<TeacherViewModel>();
    return EcomuSection(
      priority: 1,
      child: Padding(
        padding: const EdgeInsets.only(
          left: 16.0,
          right: 16.0,
          top: 16.0,
        ),
        child: Column(
          children: [
            SearchField(
              controller: viewModel.searchController,
              text: local.teacher_search_text,
              action: viewModel.searchProfessor,
            ),
            LayoutBuilder(
              builder: (context, constraints) {
                if (constraints.biggest.width < 250) {
                  return EcomuSectionTitle(
                    text: local.teacher_short_title_list,
                  );
                } else {
                  return EcomuSectionTitle(
                    text: local.teacher_title_list,
                  );
                }
              },
            ),
            const Divider(),
            Expanded(
              child: LayoutBuilder(
                builder: (context, constraints) {
                  var type = 0;
                  if (constraints.biggest.width < 250) {
                    type = 2;
                  } else if (constraints.biggest.width < 310) {
                    type = 1;
                  }
                  return Selector<TeacherViewModel, List<Professor>>(
                    builder: (context, value, child) {
                      return EcomuList(
                        type: type,
                        matchFunction: viewModel.hasMatch,
                        list: value,
                        onItemClick: viewModel.searchAction,
                      );
                    },
                    selector: (_, __) => viewModel.listProfessors,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
