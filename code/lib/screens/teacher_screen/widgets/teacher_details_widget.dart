/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Persons
 
  Purpose: This file contains the a person detail.

  Author:  Christopher Lucas
  Date:    16.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/professor.dart';
import 'package:ecomu_gestion/screens/teacher_screen/teacher_viewmodel.dart';
import 'package:ecomu_gestion/screens/teacher_screen/widgets/teacher_calendar_widget.dart';
import 'package:ecomu_gestion/screens/teacher_screen/widgets/teacher_details_title_widget.dart';
import 'package:ecomu_gestion/screens/teacher_screen/widgets/teacher_info_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_back_btn.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_placeholder_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TeacherDetails extends StatelessWidget {
  final priorityDetails = 5;
  final priority = 3;
  const TeacherDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final viewModel = context.watch<TeacherViewModel>();
    return Selector<TeacherViewModel, Professor?>(
      builder: (context, professor, child) {
        // Display Tooltip
        if (professor == null) {
          return EcomuSection(
            priority: priority,
            padding: const EdgeInsets.only(top: 16.0, left: 16.0),
            child: EcomuSectionPlaceholder(
              text: local.person_empty_details,
              icon: Icons.group,
            ),
          );
        }
        // Display Professor details
        return EcomuSection(
          priority: priority,
          padding: const EdgeInsets.all(16.0),
          child: LayoutBuilder(
            builder: (context, constraints) {
              final bigTitle = constraints.biggest.width > 700;
              return Column(
                children: [
                  // Return Button top left
                  ReturnButton(
                    action: () {
                      viewModel.setSelectedProfessor = null;
                    },
                  ),
                  ContentSpacing(
                    priority: priorityDetails,
                    child: Column(
                      children: [
                        TeacherDetailsTitle(
                          professor: professor,
                          bigTitle: bigTitle,
                        ),
                        const Divider(),
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    controller: ScrollController(),
                    child: ContentSpacing(
                      priority: priorityDetails,
                      child: TeacherInfo(
                        bigInfo: bigTitle,
                      ),
                    ),
                  ),
                  const TeacherCalendar(),
                ],
              );
            },
          ),
        );
      },
      selector: (_, __) => viewModel.selectedProfessor,
    );
  }
}
