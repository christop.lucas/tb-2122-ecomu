/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Teacher
 
  Purpose: This file contains the widget of the calendar for Teacher view.

  Author:  Christopher Lucas
  Date:    14.06.2022
*/
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class TeacherCalendar extends StatelessWidget {
  const TeacherCalendar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final now = DateTime.now().add(const Duration(days: 2));
    return Expanded(
      child: SfCalendar(
        minDate: now.subtract(Duration(days: now.weekday)),
        maxDate: now.add(Duration(days: 7 - now.weekday)),
        onTap: null,
        dataSource: _getCalendarDataSource(),
        view: CalendarView.workWeek,
        timeSlotViewSettings: const TimeSlotViewSettings(
          dateFormat: 'd',
          timeFormat: 'HH:mm',
          dayFormat: 'EEEE',
          timeIntervalHeight: 25,
          startHour: 8,
          endHour: 22,
          nonWorkingDays: <int>[DateTime.sunday, DateTime.saturday],
        ),
        headerHeight: 0.0,
      ),
    );
  }

  int getMonday() {
    return 1;
  }
}

_AppointmentDataSource _getCalendarDataSource() {
  // heure de depart
  // heure fin : heure de depart + durée
  // jour = now.day + (jour du cours - now.weekday)
  List<Appointment> appointments = <Appointment>[];
  final now = DateTime.now().add(const Duration(days: 3));

  appointments.add(Appointment(
    startTime: DateTime(now.year, now.month, now.day + (1 - now.weekday), 9),
    endTime: DateTime(now.year, now.month, now.day + (1 - now.weekday), 11),
    subject: "Arton Hoxha, guitare",
  ));
  appointments.add(Appointment(
    startTime: DateTime(now.year, now.month, now.day + (1 - now.weekday), 15),
    endTime: DateTime(now.year, now.month, now.day + (1 - now.weekday), 22),
    subject: "Pascal Bruegger, guitare",
  ));
  appointments.add(Appointment(
    startTime: DateTime(now.year, now.month, now.day + (2 - now.weekday), 9),
    endTime: DateTime(now.year, now.month, now.day + (2 - now.weekday), 12),
    subject: "Solfege",
  ));
  appointments.add(Appointment(
    startTime: DateTime(now.year, now.month, now.day + (3 - now.weekday), 15),
    endTime: DateTime(now.year, now.month, now.day + (3 - now.weekday), 20),
    subject: "Christopher Lucas, guitare",
  ));

  return _AppointmentDataSource(appointments);
}

class _AppointmentDataSource extends CalendarDataSource {
  _AppointmentDataSource(List<Appointment> source) {
    appointments = source;
  }
}
