/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Inscriptions
 
  Purpose: This file contains the View Model for the Inscriptions View.

  Author:  Christopher Lucas
  Date:    07.06.2022
*/
import 'package:ecomu_gestion/models/course.dart';
import 'package:ecomu_gestion/models/course_day.dart';
import 'package:ecomu_gestion/models/duration.dart';
import 'package:ecomu_gestion/models/inscription.dart';
import 'package:ecomu_gestion/models/instrument.dart';
import 'package:ecomu_gestion/models/lesson.dart';
import 'package:ecomu_gestion/models/person.dart';
import 'package:ecomu_gestion/models/professor.dart';
import 'package:ecomu_gestion/models/registration.dart';
import 'package:ecomu_gestion/models/registration_add.dart';
import 'package:ecomu_gestion/models/section.dart';
import 'package:ecomu_gestion/models/type_course.dart';
import 'package:ecomu_gestion/models/workshop.dart';
import 'package:ecomu_gestion/services/ecomu_repository/ecomu_repository.dart';
import 'package:flutter/material.dart';

enum InscriptionState { initial, home, failed, insert }

class InscriptionsViewModel extends ChangeNotifier {
  /// Repository with the inscriptions data
  final EcomuRepository _repository;

  /// State of the [InscriptionsView]
  var _state = InscriptionState.initial;

  var _selectedForm = 0;

  Person? _student;

  String? _studentTitle;

  String? _studentGender;

  Person? _parent;

  String? _parentTitle;

  String? _parentGender;

  Person? _billing;

  String? _billingTitle;

  String? _billingGender;

  Professor? professor;

  CourseDuration? duration;

  CourseDay? day;

  Section? section;

  Workshop? _workshop;

  Instrument? _instrument;

  Registration? _selectedRegistration;

  CourseType? _selectedCourseType;

  Course? _selectedCourse;

  var _toggleStudent = false;

  var _checkParent = false;

  var _toggle = false;

  var sameStudent = false;

  /// Controls the search Text Field
  final searchController = TextEditingController();

  /// Controls the last name Text Field
  final lastNameController = TextEditingController();

  /// Controls the first name Text Field
  final firstNameController = TextEditingController();

  /// Controls the email Text Field
  final emailController = TextEditingController();

  /// Controls the birthday Text Field
  final birthdayController = TextEditingController();

  /// Controls the address Field
  final addressController = TextEditingController();

  /// Controls the zip code Text Field
  final zipCodeController = TextEditingController();

  /// Controls the location Text Field
  final locationController = TextEditingController();

  /// Controls the private number Text Field
  final privNumberController = TextEditingController();

  /// Controls the professional number Text Field
  final profNumberController = TextEditingController();

  /// Controls the gender Dropdown
  final genderController = GlobalKey<FormFieldState>();

  /// Controls the title Dropdown
  final titleController = GlobalKey<FormFieldState>();

  /// Controls the section Dropdown
  final sectionController = GlobalKey<FormFieldState>();

  /// Controls the section Dropdown
  final professorController = GlobalKey<FormFieldState>();

  /// Controls the section Dropdown
  final durationController = GlobalKey<FormFieldState>();

  /// Controls the section Dropdown
  final instrumentController = GlobalKey<FormFieldState>();

  /// Controls the section Dropdown
  final dayController = GlobalKey<FormFieldState>();

  /// Controls the section Dropdown
  final workshopController = GlobalKey<FormFieldState>();

  /// Controls the course type Dropdown
  final courseTypeController = GlobalKey<FormFieldState>();

  /// Controls the course type Dropdown
  final courseController = GlobalKey<FormFieldState>();

  final int _rowsPerPage = 14;

  final double _dataPagerHeight = 60.0;

  /// Constructs [InscriptionsViewModel]
  InscriptionsViewModel({required EcomuRepository repository})
      : _repository = repository {
    initViewModel();
  }

  void initViewModel() async {
    await _repository.fetchInstruments();
    await _repository.fetchDays();
    await _repository.fetchDurations();
    await _repository.fetchProfessors();
    await _repository.fetchSections();
    await _repository.fetchWorkshops();
  }

  Future<bool> insertInscription() async {
    final result = await _repository.insertRegistration(
      _student!,
      _parent,
      _billing,
      sameStudent,
      RegistrationAdd(
        idProfessor: professor!.idProfessor!,
        idCourse: _selectedCourse!.idCourse!,
        idInstrument: _instrument?.idInstrument,
        idWorkshop: _workshop?.idWorkshop,
        idDay: day!.idDay,
        idSection: section!.idSection,
        hour: "18:00",
        beginDate: DateTime.now().toIso8601String(),
      ),
    );
    if (result) {
      resetAll();
    }
    return result;
  }

  void resetAll() {
    genderController.currentState!.reset();
    titleController.currentState!.reset();
    sectionController.currentState!.reset();
    professorController.currentState!.reset();
    durationController.currentState!.reset();
    instrumentController.currentState!.reset();
    dayController.currentState!.reset();
    workshopController.currentState!.reset();
    courseTypeController.currentState!.reset();
    courseController.currentState!.reset();
    professor = null;
    duration = null;
    day = null;
    section = null;
    _workshop = null;
    _instrument = null;
    _selectedRegistration = null;
    _selectedCourseType = null;
    _selectedCourse = null;
  }

  Future<List<Registration>> getRegistrations() async {
    final result = await _repository.fetchRegistrations();
    if (result != null) {
      return result;
    }
    return <Registration>[];
  }

  Future<List<Section>> getSections() async {
    final result = await _repository.fetchSections();
    if (result != null) {
      return result;
    }
    return <Section>[];
  }

  Future<List<CourseDuration>> getDurations() async {
    final result = await _repository.fetchDurations();
    if (result != null) {
      return result;
    }
    return <CourseDuration>[];
  }

  Future<List<CourseType>> getCourseTypes() async {
    final result = await _repository.fetchCourseTypes();
    if (result != null) {
      return result;
    }
    return <CourseType>[];
  }

  Future<List<Course>> getCourses() async {
    final result = await _repository.fetchCourses();
    if (result != null) {
      return result;
    }
    return <Course>[];
  }

  Future<List<Instrument>> getInstruments() async {
    final result = await _repository.fetchInstruments();
    if (result != null) {
      return result;
    }
    return <Instrument>[];
  }

  Future<List<Workshop>> getWorkshops() async {
    final result = await _repository.fetchWorkshops();
    if (result != null) {
      return result;
    }
    return <Workshop>[];
  }

  Future<List<Professor>> getProfessors() {
    if (instrument != null) {
      return _repository.fetchProfessorsByInstrument(instrument!.idInstrument);
    }
    return _repository.fetchProfessors2();
  }

  void setStudent() async {
    var idPerson = (_student != null) ? _student!.idPerson : null;
    var zip = int.parse(zipCodeController.text);
    _student = Person(
      idPerson: idPerson,
      title: _studentTitle,
      firstName: firstNameController.text,
      lastName: lastNameController.text,
      gender: _studentGender,
      address: addressController.text,
      zipCode: zip,
      location: locationController.text,
      privateNumber: privNumberController.text,
      profNumber: profNumberController.text,
      email: emailController.text,
      birthday: birthdayController.text,
    );
    resetForm(0);
    _toggle = false;
    notifyListeners();
  }

  void setParent() async {
    var idPerson = (_parent != null) ? _parent!.idPerson : null;
    var zip = int.parse(zipCodeController.text);
    _parent = Person(
      idPerson: idPerson,
      title: _parentTitle,
      firstName: firstNameController.text,
      lastName: lastNameController.text,
      gender: _parentGender,
      address: addressController.text,
      zipCode: zip,
      location: locationController.text,
      privateNumber: privNumberController.text,
      profNumber: profNumberController.text,
      email: emailController.text,
      birthday: birthdayController.text,
    );
    resetForm(1);
    _toggle = false;
    notifyListeners();
  }

  void setBilling() async {
    var idPerson = (_billing != null) ? _billing!.idPerson : null;
    var zip = int.parse(zipCodeController.text);
    _billing = Person(
      idPerson: idPerson,
      title: _billingTitle,
      firstName: firstNameController.text,
      lastName: lastNameController.text,
      gender: _billingGender,
      address: addressController.text,
      zipCode: zip,
      location: locationController.text,
      privateNumber: privNumberController.text,
      profNumber: profNumberController.text,
      email: emailController.text,
      birthday: birthdayController.text,
    );
    resetForm(2);
    _toggle = false;
    notifyListeners();
  }

  void next() {
    if (_selectedForm < 5) {
      _selectedForm += 1;
      notifyListeners();
    }
  }

  void prev() {
    if (_selectedForm > 0) {
      _selectedForm -= 1;
      notifyListeners();
    }
  }

  void reset() {
    _state = InscriptionState.initial;
    _selectedForm = 0;
    _student = null;
    _studentTitle = null;
    _studentGender = null;
    _parent = null;
    _parentGender = null;
    _parentTitle = null;
    notifyListeners();
  }

  void chooseStudent(Person p) {
    _student = p;
    setControllers(_student!);
    _studentGender = p.gender;
    _studentTitle = p.title;
    notifyListeners();
  }

  void chooseParent(Person p) {
    _parent = p;
    setControllers(_parent!);
    _parentGender = p.gender;
    _parentTitle = p.title;
    notifyListeners();
  }

  void chooseBilling(Person p) {
    _billing = p;
    setControllers(_billing!);
    _billingGender = p.gender;
    _billingTitle = p.title;
    notifyListeners();
  }

  void resetForm(int form) {
    resetControllers();
    if (form == 0) {
      _studentGender = null;
      _studentTitle = null;
    } else if (form == 1) {
      _parentGender = null;
      _parentTitle = null;
    } else if (form == 2) {
      _billingGender = null;
      _billingTitle = null;
    }
  }

  void resetControllers() {
    firstNameController.clear();
    lastNameController.clear();
    addressController.clear();
    locationController.clear();
    privNumberController.clear();
    emailController.clear();
    birthdayController.clear();
    zipCodeController.clear();
    profNumberController.clear();
    genderController.currentState!.reset();
    titleController.currentState!.reset();
  }

  void setControllers(Person p) {
    lastNameController.text = p.lastName!;
    firstNameController.text = p.firstName!;
    emailController.text = p.email!;
    birthdayController.text = p.birthday ?? "";
    addressController.text = p.address!;
    zipCodeController.text = p.zipCode.toString();
    locationController.text = p.location!;
    privNumberController.text = p.privateNumber!;
    profNumberController.text = p.profNumber!;
  }

  InscriptionState get state {
    return _state;
  }

  set setInscriptionState(InscriptionState value) {
    _state = value;
    notifyListeners();
  }

  int get selectedForm {
    return _selectedForm;
  }

  set setSelectedForm(int value) {
    _selectedForm = value;
    notifyListeners();
  }

  Person? get student {
    return _student;
  }

  /* --- Gettter and Setter for _studentGender ------------------------- */
  String? get studentGender {
    return _studentGender;
  }

  set setStudentGender(String? value) {
    _studentGender = value;
    if (value == null) notifyListeners();
  }

  /* --- Gettter and Setter for _studentTitle ------------------------- */
  String? get studentTitle {
    return _studentTitle;
  }

  set setStudentTitle(String? value) {
    _studentTitle = value;
    if (value == null) notifyListeners();
  }

  /* --- Gettter and Setter for _parentGender ------------------------- */
  Person? get parent {
    return _parent;
  }

  String? get parentGender {
    return _parentGender;
  }

  set setParentGender(String? value) {
    _parentGender = value;
    if (value == null) notifyListeners();
  }

  /* --- Gettter and Setter for _parentTitle ------------------------- */
  String? get parentTitle {
    return _parentTitle;
  }

  set setParentTitle(String? value) {
    _parentTitle = value;
    if (value == null) notifyListeners();
  }

  /* --- Gettter and Setter for _billingGender ------------------------- */
  Person? get billing {
    return _billing;
  }

  String? get billingGender {
    return _billingGender;
  }

  set setBillingGender(String? value) {
    _billingGender = value;
    if (value == null) notifyListeners();
  }

  /* --- Gettter and Setter for _billingTitle ------------------------- */
  String? get billingTitle {
    return _billingTitle;
  }

  set setBillingTitle(String? value) {
    _billingTitle = value;
    if (value == null) notifyListeners();
  }

  List<Instrument> get instruments {
    /*if (_workshop != null) {
      return <Instrument>[];
    }*/
    return _repository.instruments;
  }

  Instrument? get instrument {
    return _instrument;
  }

  set setInstrument(Instrument? value) {
    _instrument = value;
    notifyListeners();
  }

  Workshop? get workshop {
    return _workshop;
  }

  set setWorkshop(Workshop? value) {
    _workshop = value;
    notifyListeners();
  }

  List<CourseDuration> get lengthsCours {
    return _repository.durations;
  }

  List<CourseDay> get days {
    return _repository.days;
  }

  List<Professor> get professors {
    return _repository.professors;
  }

  List<Workshop> get workshops {
    return _repository.workshops;
  }

  List<Section> get sections {
    return _repository.sections;
  }

  bool get toggleStudent {
    return _toggleStudent;
  }

  set setToggleStudent(bool value) {
    _toggleStudent = value;
    notifyListeners();
  }

  bool get checkParent {
    return _checkParent;
  }

  set setCheckParent(bool value) {
    _checkParent = value;
    notifyListeners();
  }

  get rowsPerPage {
    return _rowsPerPage;
  }

  get dataPagerHeight {
    return _dataPagerHeight;
  }

  get rowHeight {
    return 49.0;
  }

  get headerHeight {
    return 56.0;
  }

  get wishedTableHeight {
    return headerHeight + rowHeight * rowsPerPage;
  }

  bool get toggle {
    return _toggle;
  }

  set setToggle(bool value) {
    _toggle = value;
    notifyListeners();
  }

  set setSelectedRegistration(Registration registration) {
    _selectedRegistration = registration;
    notifyListeners();
  }

  CourseType? get selectedCourseType {
    return _selectedCourseType;
  }

  set setSelectedCourseType(CourseType? value) {
    if (_selectedCourseType != null) {
      resetCourse();
      resetInstrument();
    }

    _selectedCourseType = value;
    _workshop = null;
    notifyListeners();
  }

  void resetInstrument() {
    if (_instrument != null) {
      instrumentController.currentState!.reset();
    }
    _instrument = null;
  }

  void resetCourse() {
    if (_selectedCourse != null) {
      courseController.currentState!.reset();
    }
    _selectedCourse = null;
  }

  Course? get selectedCourse {
    return _selectedCourse;
  }

  set setSelectedCourse(Course? value) {
    _selectedCourse = value;
    notifyListeners();
  }

  bool canStep2() {
    return _selectedCourse != null && _selectedCourseType != null;
  }

  bool canStep3() {
    var collectif = false;
    if (selectedCourseType != null) {
      collectif =
          selectedCourseType!.name == "Collectif" && _selectedCourse != null;
    }
    return instrument != null || workshop != null || collectif;
  }

  bool showInstruments() {
    return _selectedCourseType!.isIndividuel();
  }

  bool showWorkshops() {
    return _selectedCourseType!.isWorkshop();
  }

  bool hasRepLeg() {
    return _parent != null;
  }
}
