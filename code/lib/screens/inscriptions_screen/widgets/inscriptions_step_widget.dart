import 'package:ecomu_gestion/screens/inscriptions_screen/inscriptions_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InscriptionsStep extends StatelessWidget {
  /// Value in the circle
  final int number;

  /// Value under the circle
  final String text;

  /// Padding value
  final int padding;

  /// Radius of outer CircleAvatar
  final double radius;
  const InscriptionsStep({
    Key? key,
    required this.number,
    required this.text,
    required this.padding,
    required this.radius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.watch<InscriptionsViewModel>();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          CircleAvatar(
            radius: radius,
            backgroundColor: Theme.of(context).colorScheme.primary,
            child: Builder(builder: (context) {
              if (viewModel.selectedForm == number - 1) {
                return Text("$number");
              } else {
                return CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Text("$number"),
                );
              }
            }),
          ),
          Text(text),
        ],
      ),
    );
  }
}
