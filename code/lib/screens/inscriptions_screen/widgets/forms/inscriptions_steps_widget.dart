import 'package:ecomu_gestion/screens/inscriptions_screen/widgets/inscriptions_step_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_line_widget.dart';
import 'package:flutter/material.dart';

class InscriptionSteps extends StatelessWidget {
  final List<String> listSteps;
  const InscriptionSteps({Key? key, required this.listSteps}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      const radius = 21.0;
      const padding = 8;
      const formProportion = 4 / 6;
      var otherWidget =
          listSteps.length * radius + listSteps.length * 2 * padding;
      const nbLines = 3;
      final width =
          (constraints.maxWidth * formProportion - otherWidget) / nbLines;
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InscriptionsStep(
            number: 1,
            text: listSteps[0],
            padding: padding,
            radius: radius,
          ),
          EcomuLine(width: width),
          InscriptionsStep(
            number: 2,
            text: listSteps[1],
            padding: padding,
            radius: radius,
          ),
          EcomuLine(width: width),
          InscriptionsStep(
            number: 3,
            text: listSteps[2],
            padding: padding,
            radius: radius,
          ),
          EcomuLine(width: width),
          InscriptionsStep(
            number: 4,
            text: listSteps[3],
            padding: padding,
            radius: radius,
          ),
        ],
      );
    });
  }
}
