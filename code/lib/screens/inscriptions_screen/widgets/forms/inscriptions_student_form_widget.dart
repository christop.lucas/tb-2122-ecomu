/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Persons
 
  Purpose: This file contains the insert person content.

  Author:  Christopher Lucas
  Date:    16.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/inscriptions_viewmodel.dart';
import 'package:ecomu_gestion/services/ecomu_helper.dart';
import 'package:ecomu_gestion/widgets/ecomu_date_picker_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_form/ecomu_user_form_field_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InscriptionsStudentForm extends StatelessWidget {
  const InscriptionsStudentForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final key = GlobalKey<FormState>();
    final local = AppLocalizations.of(context)!;
    final viewModel = context.read<InscriptionsViewModel>();
    return Form(
      key: key,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              top: 8.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: DropdownButtonFormField(
                    key: viewModel.titleController,
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      labelText: local.form_user_title,
                    ),
                    value: viewModel.studentTitle,
                    hint: Text(local.form_person_dropdown_placeholder),
                    onChanged: (String? value) {
                      viewModel.setStudentTitle = value;
                    },
                    items: local.person_titles
                        .split(local.splitter)
                        .map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    validator: EcomuHelper.simpleValidator,
                  ),
                ),
                const Expanded(
                  flex: 2,
                  child: SizedBox.shrink(),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: EcomuFormField(
                    controller: viewModel.lastNameController,
                    hasLeftNeighbour: false,
                    text: local.form_user_lastname,
                    validator: EcomuHelper.simpleValidator,
                  ),
                ),
                Expanded(
                  child: EcomuFormField(
                    controller: viewModel.firstNameController,
                    hasLeftNeighbour: true,
                    text: local.form_user_firstname,
                    validator: EcomuHelper.simpleValidator,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16.0,
            ),
            child: EcomuFormField(
              controller: viewModel.emailController,
              hasLeftNeighbour: false,
              text: local.form_user_email,
              validator: EcomuHelper.emailValidator,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: DropdownButtonFormField(
                    key: viewModel.genderController,
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      labelText: local.form_user_gender,
                    ),
                    hint: Text(local.form_person_dropdown_placeholder),
                    value: viewModel.studentGender,
                    onChanged: (String? value) {
                      viewModel.setStudentGender = value;
                    },
                    items: local.person_genders
                        .split(local.splitter)
                        .map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    validator: EcomuHelper.simpleValidator,
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: DatePickerField(
                      title: local.form_user_birthday,
                      controller: viewModel.birthdayController,
                      validator: EcomuHelper.simpleValidator,
                    ),
                  ),
                ),
              ],
            ),
          ),
          const Divider(),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: EcomuFormField(
              controller: viewModel.addressController,
              hasLeftNeighbour: false,
              text: local.form_user_address,
              validator: EcomuHelper.simpleValidator,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16.0,
              bottom: 8.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: EcomuFormField(
                    controller: viewModel.zipCodeController,
                    hasLeftNeighbour: false,
                    text: local.form_user_zipcode,
                    validator: EcomuHelper.numberValidator,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: EcomuFormField(
                    controller: viewModel.locationController,
                    hasLeftNeighbour: true,
                    text: local.form_user_location,
                    validator: EcomuHelper.simpleValidator,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 8.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: EcomuFormField(
                    controller: viewModel.privNumberController,
                    hasLeftNeighbour: false,
                    text: local.form_user_profnumber,
                    validator: EcomuHelper.numberValidator,
                  ),
                ),
                Expanded(
                  child: EcomuFormField(
                    controller: viewModel.profNumberController,
                    hasLeftNeighbour: true,
                    text: local.form_user_privnumber,
                    validator: EcomuHelper.numberValidator,
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Builder(
                builder: (context) {
                  return Wrap(
                    spacing: 20,
                    children: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          onPrimary: Theme.of(context).colorScheme.onPrimary,
                          primary: Theme.of(context).colorScheme.primary,
                        ),
                        onPressed: () {
                          if (key.currentState!.validate()) {
                            viewModel.setStudent();
                            viewModel.setSelectedForm = 1;
                          }
                        },
                        child: Text(local.btn_next_text),
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
