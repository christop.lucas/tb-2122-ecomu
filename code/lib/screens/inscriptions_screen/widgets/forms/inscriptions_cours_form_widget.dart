import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/course.dart';
import 'package:ecomu_gestion/models/course_day.dart';
import 'package:ecomu_gestion/models/instrument.dart';
import 'package:ecomu_gestion/models/professor.dart';
import 'package:ecomu_gestion/models/section.dart';
import 'package:ecomu_gestion/models/type_course.dart';
import 'package:ecomu_gestion/models/workshop.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/inscriptions_viewmodel.dart';
import 'package:ecomu_gestion/services/ecomu_helper.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:ecomu_gestion/widgets/ecomu_date_picker_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_time_picker_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InscriptionsCoursForm extends StatefulWidget {
  const InscriptionsCoursForm({Key? key}) : super(key: key);

  @override
  State<InscriptionsCoursForm> createState() => _InscriptionsCoursFormState();
}

class _InscriptionsCoursFormState extends State<InscriptionsCoursForm> {
  final _key = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final viewModel = context.read<InscriptionsViewModel>();

    return ContentSpacing(
      priority: 5,
      child: Form(
        key: _key,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 16.0,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: FutureBuilder<List<Section>>(
                        future: viewModel.getSections(),
                        builder: (context, snapshot) {
                          var list = <Section>[];
                          if (snapshot.hasData) {
                            list = snapshot.data!;
                          }
                          return DropdownButtonFormField<Section>(
                            key: viewModel.sectionController,
                            decoration: InputDecoration(
                              border: const OutlineInputBorder(),
                              labelText: local.form_inscription_cours_section,
                            ),
                            value: viewModel.section,
                            hint: Text(local.form_person_dropdown_placeholder),
                            onChanged: (Section? value) {
                              viewModel.section = value;
                            },
                            items: list.map((Section value) {
                              return DropdownMenuItem<Section>(
                                value: value,
                                child: Text(value.name),
                              );
                            }).toList(),
                            validator: (value) => null,
                          );
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: FutureBuilder<List<CourseType>>(
                        future: viewModel.getCourseTypes(),
                        builder: (context, snapshot) {
                          var list = <CourseType>[];
                          if (snapshot.hasData) {
                            list = snapshot.data!;
                          }
                          print(list.length);
                          print(viewModel.selectedCourseType != null);
                          return DropdownButtonFormField<CourseType?>(
                            key: viewModel.courseTypeController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Type de cours",
                            ),
                            value: viewModel.selectedCourseType,
                            hint: Text(local.form_person_dropdown_placeholder),
                            onChanged: (CourseType? value) {
                              viewModel.setSelectedCourseType = value;
                            },
                            items: list.map((CourseType value) {
                              return DropdownMenuItem<CourseType>(
                                value: value,
                                child: Text(value.name),
                              );
                            }).toList(),
                            validator: (value) => null,
                          );
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: FutureBuilder<List<Course>>(
                        future: viewModel.getCourses(),
                        builder: (context, snapshot) {
                          var list = <Course>[];
                          if (snapshot.hasData) {
                            list = snapshot.data!;
                          }
                          return DropdownButtonFormField<Course?>(
                            key: viewModel.courseController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Cours",
                            ),
                            value: viewModel.selectedCourse,
                            hint: Text(local.form_person_dropdown_placeholder),
                            onChanged: (Course? value) {
                              viewModel.setSelectedCourse = value;
                            },
                            items: list.map((Course value) {
                              return DropdownMenuItem<Course>(
                                value: value,
                                child: Text(value.name),
                              );
                            }).toList(),
                            validator: (value) => null,
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 16.0,
              ),
              child: Selector<InscriptionsViewModel, bool>(
                builder: (context, show, child) {
                  if (show) {
                    return Row(
                      children: [
                        Builder(
                          builder: (context) {
                            if (viewModel.showInstruments()) {
                              return Expanded(
                                child: FutureBuilder<List<Instrument>>(
                                  future: viewModel.getInstruments(),
                                  builder: (context, snapshot) {
                                    var list = <Instrument>[];
                                    if (snapshot.hasData) {
                                      list = snapshot.data!;
                                    }
                                    return DropdownButtonFormField<Instrument?>(
                                      key: viewModel.instrumentController,
                                      decoration: InputDecoration(
                                        border: const OutlineInputBorder(),
                                        labelText: local
                                            .form_inscription_cours_instrument,
                                      ),
                                      value: viewModel.instrument,
                                      hint: Text(local
                                          .form_person_dropdown_placeholder),
                                      onChanged: (Instrument? value) {
                                        viewModel.setInstrument = value;
                                      },
                                      items: list.map((Instrument value) {
                                        return DropdownMenuItem<Instrument>(
                                          value: value,
                                          child: Text(value.name),
                                        );
                                      }).toList(),
                                      validator: (value) => null,
                                    );
                                  },
                                ),
                              );
                            } else if (viewModel.showWorkshops()) {
                              return Expanded(
                                child: FutureBuilder<List<Workshop>>(
                                  future: viewModel.getWorkshops(),
                                  builder: (context, snapshot) {
                                    var list = <Workshop>[];
                                    if (snapshot.hasData) {
                                      list = snapshot.data!;
                                    }
                                    return DropdownButtonFormField<Workshop?>(
                                      key: viewModel.workshopController,
                                      decoration: InputDecoration(
                                        border: const OutlineInputBorder(),
                                        labelText: "Atelier",
                                      ),
                                      value: viewModel.workshop,
                                      hint: Text(local
                                          .form_person_dropdown_placeholder),
                                      onChanged: (Workshop? value) {
                                        viewModel.setWorkshop = value;
                                      },
                                      items: list.map((Workshop value) {
                                        return DropdownMenuItem<Workshop>(
                                          value: value,
                                          child: Text(value.workshop),
                                        );
                                      }).toList(),
                                      validator: (value) => null,
                                    );
                                  },
                                ),
                              );
                            }
                            return const SizedBox.shrink();
                          },
                        ),
                        const Expanded(
                          child: SizedBox.shrink(),
                        ),
                      ],
                    );
                    /*return Row(
                      children: [
                       
                        
                      ],
                    );*/
                  }
                  return const SizedBox.shrink();
                },
                selector: (_, __) => viewModel.canStep2(),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 16.0,
              ),
              child: Selector<InscriptionsViewModel, bool>(
                builder: (context, show, child) {
                  if (show) {
                    return Row(
                      children: [
                        Selector<InscriptionsViewModel, Instrument?>(
                          builder: (context, instrument, child) {
                            return Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: FutureBuilder<List<Professor>>(
                                    future: viewModel.getProfessors(),
                                    builder: (context, snapshot) {
                                      var list = <Professor>[];
                                      if (snapshot.hasData) {
                                        list = snapshot.data!;
                                      }
                                      return DropdownButtonFormField<
                                          Professor?>(
                                        key: viewModel.professorController,
                                        decoration: const InputDecoration(
                                          border: OutlineInputBorder(),
                                          labelText: "Professeur",
                                        ),
                                        value: viewModel.professor,
                                        hint: Text(local
                                            .form_person_dropdown_placeholder),
                                        onChanged: (Professor? value) {
                                          viewModel.professor = value;
                                        },
                                        items: list.map((Professor value) {
                                          return DropdownMenuItem<Professor>(
                                            value: value,
                                            child: Text(
                                                "${value.firstName} ${value.lastName}"),
                                          );
                                        }).toList(),
                                        validator: (value) => null,
                                      );
                                    }),
                              ),
                            );
                          },
                          selector: (_, __) => viewModel.instrument,
                        ),
                        Expanded(
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 8.0),
                            child: DropdownButtonFormField<CourseDay>(
                              key: viewModel.dayController,
                              decoration: InputDecoration(
                                border: const OutlineInputBorder(),
                                labelText: local.form_inscription_cours_day,
                              ),
                              value: viewModel.day,
                              hint:
                                  Text(local.form_person_dropdown_placeholder),
                              onChanged: (CourseDay? value) {
                                viewModel.day = value;
                              },
                              items: viewModel.days.map((CourseDay value) {
                                return DropdownMenuItem<CourseDay>(
                                  value: value,
                                  child: Text(value.day),
                                );
                              }).toList(),
                              validator: (value) =>
                                  EcomuHelper.simpleValidator(value?.day),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 8.0),
                            child: TimePickerField(
                              title: local.form_inscription_cours_hours,
                              controller: TextEditingController(),
                              validator: EcomuHelper.simpleValidator,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: DatePickerField(
                              title: local.form_inscription_cours_start_date,
                              controller: TextEditingController(),
                              validator: EcomuHelper.simpleValidator,
                              initDate: DateTime.now(),
                              firstDate: DateTime.now().subtract(
                                const Duration(days: 1),
                              ),
                              lastDate: DateTime.now()
                                  .add(const Duration(days: 365 * 2)),
                            ),
                          ),
                        ),
                      ],
                    );
                  }
                  return const SizedBox.shrink();
                },
                selector: (_, __) => viewModel.canStep3(),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 32.0),
                child: Builder(
                  builder: (context) {
                    return Wrap(
                      spacing: 20,
                      children: [
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            onPrimary: Theme.of(context).colorScheme.onPrimary,
                            primary: Theme.of(context).colorScheme.primary,
                          ),
                          onPressed: () {
                            if (_key.currentState!.validate()) {
                              viewModel.insertInscription();
                              viewModel.reset();
                              //resetControllers(context);
                            }
                          },
                          child: Text(local.btn_next_text),
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
/*
 Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: FutureBuilder<List<CourseDuration>>(
                        future: viewModel.getDurations(),
                        builder: (context, snapshot) {
                          var list = <CourseDuration>[];
                          if (snapshot.hasData) {
                            list = snapshot.data!;
                          }
                          return DropdownButtonFormField<CourseDuration?>(
                            key: viewModel.durationController,
                            decoration: InputDecoration(
                              border: const OutlineInputBorder(),
                              labelText: local.form_inscription_cours_length,
                            ),
                            value: viewModel.duration,
                            hint: Text(local.form_person_dropdown_placeholder),
                            onChanged: (CourseDuration? value) {
                              viewModel.duration = value;
                            },
                            items: list.map((CourseDuration value) {
                              return DropdownMenuItem<CourseDuration>(
                                value: value,
                                child: Text(value.formatedDuration),
                              );
                            }).toList(),
                            validator: (value) => null,
                          );
                        },
                      ),
                    ),
                  ),
*/