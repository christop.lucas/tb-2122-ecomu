import 'package:ecomu_gestion/models/registration.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/inscriptions_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class InscriptionTable extends StatelessWidget {
  const InscriptionTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<InscriptionsViewModel>();

    return LayoutBuilder(
      builder: (context, constraint) {
        final maxHeight = constraint.maxHeight - viewModel.dataPagerHeight;
        final height = viewModel.wishedTableHeight;
        return FutureBuilder<List<Registration>>(
          future: viewModel.getRegistrations(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var registrations = snapshot.data!;
              final source = RegistrationDataSource(
                rowsPerPage: viewModel.rowsPerPage,
                dataPagerHeight: viewModel.dataPagerHeight,
                registrations: registrations,
              );
              return Column(
                children: [
                  SizedBox(
                    height: (maxHeight < height) ? maxHeight : height,
                    width: constraint.maxWidth,
                    child: SfDataGrid(
                      source: source,
                      rowHeight: viewModel.rowHeight,
                      headerRowHeight: viewModel.headerHeight,
                      columns: [
                        GridColumn(
                          columnWidthMode: ColumnWidthMode.fill,
                          columnName: 'type',
                          label: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Type',
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        GridColumn(
                            columnWidthMode: ColumnWidthMode.fill,
                            columnName: 'details',
                            label: Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                alignment: Alignment.centerLeft,
                                child: const Text(
                                  'Details',
                                  overflow: TextOverflow.ellipsis,
                                ))),
                        GridColumn(
                          columnWidthMode: ColumnWidthMode.fill,
                          columnName: 'prof',
                          label: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            alignment: Alignment.centerLeft,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: const [
                                Text(
                                  'Prof',
                                  overflow: TextOverflow.ellipsis,
                                ),
                                /* IconButton(
                                onPressed: () {
                                  source.sortedColumns.add(
                                    const SortColumnDetails(
                                      name: 'prof',
                                      sortDirection:
                                          DataGridSortDirection.ascending,
                                    ),
                                  );
                                  source.sort();
                                },
                                icon: const Icon(Icons.filter_alt_outlined),
                              ),*/
                              ],
                            ),
                          ),
                        ),
                        GridColumn(
                          columnWidthMode: ColumnWidthMode.fill,
                          columnName: 'eleve',
                          label: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            alignment: Alignment.centerLeft,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: const [
                                Text(
                                  'Eleve',
                                  overflow: TextOverflow.ellipsis,
                                ),
                                /*IconButton(
                                onPressed: () {
                                  source.sortedColumns.add(
                                    const SortColumnDetails(
                                      name: 'eleve',
                                      sortDirection:
                                          DataGridSortDirection.descending,
                                    ),
                                  );
                                  source.sort();
                                },
                                icon: const Icon(Icons.filter_alt_outlined),
                              ),*/
                              ],
                            ),
                          ),
                        ),
                        GridColumn(
                          columnWidthMode: ColumnWidthMode.fill,
                          columnName: 'duration',
                          label: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Durée',
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        GridColumn(
                          columnWidthMode: ColumnWidthMode.fill,
                          columnName: 'jour',
                          label: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Jour',
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        GridColumn(
                          columnWidthMode: ColumnWidthMode.fill,
                          columnName: 'heure',
                          label: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Heure',
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                      ],
                      selectionMode: SelectionMode.single,
                      onSelectionChanged: (addedRows, removedRows) {
                        final index =
                            source.dataGridRows.indexOf(addedRows.last);
                        viewModel.setSelectedRegistration =
                            registrations[index];
                      },
                    ),
                  ),
                  SizedBox(
                    height: viewModel.dataPagerHeight,
                    child: SfDataPager(
                      delegate: source,
                      pageCount: (registrations.length / viewModel.rowsPerPage)
                          .ceil()
                          .toDouble(),
                      direction: Axis.horizontal,
                    ),
                  ),
                ],
              );
            }
            return Column();
          },
        );
      },
    );
  }
}

class RegistrationDataSource extends DataGridSource {
  final int rowsPerPage;
  final double dataPagerHeight;
  final List<Registration> registrations;
  var paginatedRegistrations = <Registration>[];
  RegistrationDataSource({
    required this.registrations,
    required this.rowsPerPage,
    required this.dataPagerHeight,
  }) {
    buildPaginatedDataGridRows();
  }

  List<DataGridRow> dataGridRows = [];

  @override
  List<DataGridRow> get rows => dataGridRows;

  @override
  DataGridRowAdapter? buildRow(DataGridRow row) {
    return DataGridRowAdapter(
      cells: row.getCells().map<Widget>(
        (dataGridCell) {
          return Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(
              dataGridCell.value.toString(),
              overflow: TextOverflow.ellipsis,
            ),
          );
        },
      ).toList(),
    );
  }

  @override
  Future<bool> handlePageChange(int oldPageIndex, int newPageIndex) async {
    int startIndex = newPageIndex * rowsPerPage;
    int endIndex = startIndex + rowsPerPage;
    if (startIndex < registrations.length) {
      paginatedRegistrations = registrations
          .getRange(
              startIndex,
              (endIndex <= registrations.length)
                  ? endIndex
                  : registrations.length)
          .toList(growable: false);
      buildPaginatedDataGridRows();
    } else {
      paginatedRegistrations = [];
    }
    notifyListeners();

    return true;
  }

  void buildPaginatedDataGridRows() {
    dataGridRows = paginatedRegistrations
        .map<DataGridRow>(
          (registration) => DataGridRow(
            cells: [
              DataGridCell<String>(
                columnName: 'type',
                value: registration.courseType,
              ),
              DataGridCell<String>(
                  columnName: 'name', value: registration.courseName),
              DataGridCell<String>(
                  columnName: 'prof', value: registration.professsor),
              DataGridCell<String>(
                columnName: 'eleve',
                value: registration.student,
              ),
              DataGridCell<String>(
                columnName: 'duration',
                value: registration.duration.formatedDuration,
              ),
              DataGridCell<String>(
                columnName: 'jour',
                value: registration.day,
              ),
              DataGridCell<String>(
                columnName: 'heure',
                value: registration.time,
              ),
            ],
          ),
        )
        .toList(growable: false);
  }
}
