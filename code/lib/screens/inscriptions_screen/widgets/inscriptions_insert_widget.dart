/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Persons
 
  Purpose: This file contains the insert person content.

  Author:  Christopher Lucas
  Date:    16.06.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/person.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/inscriptions_viewmodel.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/widgets/forms/inscriptions_cours_form_widget.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/widgets/forms/inscriptions_steps_widget.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/widgets/steps/inscription_step_one_widget.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/widgets/steps/inscription_step_three_widget.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/widgets/steps/inscription_step_two_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_back_btn.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';

class InscriptionInsert extends StatelessWidget {
  const InscriptionInsert({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final viewModel = context.read<InscriptionsViewModel>();
    final listSteps = local.inscription_steps.split(local.splitter);
    return EcomuSection(
      priority: 3,
      padding: const EdgeInsets.only(top: 16.0, left: 16.0),
      child: Column(
        children: [
          ReturnButton(
            action: viewModel.reset,
          ),
          ContentSpacing(
            priority: 5,
            child: Column(
              children: [
                InscriptionSteps(
                  listSteps: listSteps,
                ),
                Selector<InscriptionsViewModel, Tuple2<bool, int>>(
                  builder: (context, data, child) {
                    switch (data.item2) {
                      case 0:
                        return Row(
                          children: [
                            Switch(
                              onChanged: (value) {
                                viewModel.setToggle = value;
                              },
                              value: data.item1,
                            ),
                            Text(
                              local.use_existing_person,
                              style: const TextStyle(fontSize: 18),
                            ),
                          ],
                        );
                      case 1:
                        return Row(
                          children: [
                            Switch(
                              onChanged: (value) {
                                viewModel.setToggle = value;
                              },
                              value: data.item1,
                            ),
                            Text(
                              local.use_existing_person,
                              style: const TextStyle(fontSize: 18),
                            ),
                            Checkbox(
                                value: false,
                                onChanged: (value) {
                                  viewModel.setSelectedForm = 2;
                                }),
                            const Text(
                              "Pas de représentant légal",
                              style: TextStyle(fontSize: 18),
                            ),
                          ],
                        );
                      case 2:
                        return Row(
                          children: [
                            Switch(
                              onChanged: (value) {
                                viewModel.setToggle = value;
                              },
                              value: data.item1,
                            ),
                            Text(
                              local.use_existing_person,
                              style: const TextStyle(fontSize: 18),
                            ),
                            Checkbox(
                                value: false,
                                onChanged: (value) {
                                  viewModel.setSelectedForm = 3;
                                  viewModel.sameStudent = true;
                                }),
                            const Text(
                              "Idem Eleve",
                              style: TextStyle(fontSize: 18),
                            ),
                            Selector<InscriptionsViewModel, bool>(
                              builder: (context, person, child) {
                                if (person) {
                                  return Row(
                                    children: [
                                      Checkbox(
                                          value: false,
                                          onChanged: (value) {
                                            viewModel.setSelectedForm = 3;
                                            viewModel.sameStudent = false;
                                          }),
                                      const Text(
                                        "Idem représentant légal",
                                        style: TextStyle(fontSize: 18),
                                      ),
                                    ],
                                  );
                                }
                                return const SizedBox.shrink();
                              },
                              selector: (_, __) => viewModel.hasRepLeg(),
                            )
                          ],
                        );
                      default:
                        return Row();
                    }
                  },
                  selector: (_, __) => Tuple2(
                    viewModel.toggle,
                    viewModel.selectedForm,
                  ),
                )
              ],
            ),
          ),
          Selector<InscriptionsViewModel, Tuple2<bool, int>>(
            builder: (context, data, child) {
              switch (data.item2) {
                case 0:
                  return InscriptionStepOne(
                    requestSearch: data.item1,
                  );
                case 1:
                  return InscriptionStepTwo(
                    requestSearch: data.item1,
                  );
                case 2:
                  return InscriptionStepThree(
                    requestSearch: data.item1,
                  );
                default:
                  return const InscriptionsCoursForm();
              }
            },
            selector: (_, __) =>
                Tuple2(viewModel.toggle, viewModel.selectedForm),
          ),
        ],
      ),
    );
  }
}
