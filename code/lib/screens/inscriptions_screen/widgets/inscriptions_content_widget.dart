/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Inscriptions
 
  Purpose: This Widget handles the different content Widget 
      for the Inscription View.

  Author:  Christopher Lucas
  Date:    17.06.2022
*/
import 'package:ecomu_gestion/screens/inscriptions_screen/inscriptions_viewmodel.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/widgets/inscriptions_initial_widget.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/widgets/inscriptions_insert_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InscriptionContent extends StatelessWidget {
  const InscriptionContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.watch<InscriptionsViewModel>();
    switch (viewModel.state) {
      case InscriptionState.initial:
        return const InscriptionInitial();
      case InscriptionState.insert:
        return const InscriptionInsert();
      default:
        return EcomuSection(child: Column());
    }
  }
}
