/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Student
 
  Purpose: This file contains the widget of the action bar for the student view.

  Author:  Christopher Lucas
  Date:    14.06.2022
*/
import 'package:ecomu_gestion/app/widgets/ecomu_top_bar_widget.dart';
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/inscriptions_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// Action bar for the [PersonView]
class InscriptionsActionBar extends StatelessWidget {
  /// Constructs the [PersonActionBar]
  const InscriptionsActionBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.watch<InscriptionsViewModel>();
    final local = AppLocalizations.of(context)!;
    return EcomuTopBar(
      children: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 12.0),
            child: ElevatedButton.icon(
              onPressed: () {
                viewModel.setInscriptionState = InscriptionState.insert;
              },
              icon: const Icon(Icons.add),
              label: Text(
                local.inscription_btn_new_txt,
              ),
              style: ElevatedButton.styleFrom(
                onPrimary: Theme.of(context).colorScheme.onPrimary,
                primary: Theme.of(context).colorScheme.primary,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
