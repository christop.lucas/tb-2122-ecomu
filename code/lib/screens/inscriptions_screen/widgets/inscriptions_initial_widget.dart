import 'package:ecomu_gestion/screens/inscriptions_screen/widgets/inscription_table_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_title_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';

class InscriptionInitial extends StatelessWidget {
  const InscriptionInitial({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return EcomuSection(
      child: ContentSpacing(
        priority: 5,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            EcomuSectionTitle(
              text: "Liste des inscriptions",
            ),
            Divider(),
            Expanded(
              child: InscriptionTable(),
            ),
          ],
        ),
      ),
    );
  }
}
