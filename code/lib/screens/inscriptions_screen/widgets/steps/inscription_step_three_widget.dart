import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/person.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/inscriptions_viewmodel.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/widgets/forms/inscriptions_billing_form_widget.dart';
import 'package:ecomu_gestion/screens/inscriptions_screen/widgets/forms/inscriptions_parent_form_widget.dart';
import 'package:ecomu_gestion/screens/person_screen/person_viewmodel.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:ecomu_gestion/widgets/ecomu_list_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_search_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InscriptionStepThree extends StatelessWidget {
  final bool requestSearch;
  const InscriptionStepThree({
    Key? key,
    required this.requestSearch,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<InscriptionsViewModel>();
    final local = AppLocalizations.of(context)!;
    return Selector<InscriptionsViewModel, Person?>(
      builder: (context, value, child) {
        if (requestSearch && value == null) {
          final personViewModel = context.read<PersonViewModel>();
          return Expanded(
            child: ContentSpacing(
              priority: 5,
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: SearchField(
                            controller: personViewModel.searchController,
                            text: local.person_search_text,
                            action: personViewModel.searchPerson,
                          ),
                        ),
                        Expanded(
                          child: Selector<PersonViewModel, List<Person>>(
                            builder: (context, value, child) {
                              return EcomuList(
                                type: 0,
                                matchFunction: personViewModel.hasMatch,
                                list: value,
                                onItemClick: (index) {
                                  final person =
                                      personViewModel.getPerson(index);
                                  viewModel.chooseBilling(person);
                                },
                              );
                            },
                            selector: (_, __) => personViewModel.listPersons,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Expanded(
            child: SingleChildScrollView(
              controller: ScrollController(),
              child: FocusTraversalGroup(
                child: const ContentSpacing(
                  priority: 5,
                  child: InscriptionsBillingForm(),
                ),
              ),
            ),
          );
        }
      },
      selector: (_, __) => viewModel.billing,
    );
  }
}
