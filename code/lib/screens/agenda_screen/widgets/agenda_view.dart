import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';

class EcomuAgendaView extends StatelessWidget {
  const EcomuAgendaView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return EcomuSection(child: Column());
  }
}
