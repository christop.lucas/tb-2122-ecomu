/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Home
 
  Purpose: This file contains the View Model for the Person view.

  Author:  Christopher Lucas
  Date:    12.06.2022
*/
import 'package:ecomu_gestion/services/ecomu_repository/ecomu_repository.dart';
import 'package:flutter/material.dart';

/// The different states of the [HomeViewModel]
enum HomeState { initial, success, httpfailed, sqlitefailed }

/// The View Model gets the necessary data for all the views.
class HomeViewModel extends ChangeNotifier {
  /// The current state [HomeState] of the viewModel.
  var _state = HomeState.initial;

  /// The current state [HomeState] of the viewModel.
  var _statePerson = HomeState.initial;

  /// The current state [HomeState] of the viewModel.
  var _stateInscription = HomeState.initial;

  /// The repository with the data.
  final EcomuRepository _repository;

  /// Constructs the View Model by checking all the local data.
  HomeViewModel({required EcomuRepository repository})
      : _repository = repository {
    _initHomeViewModel();
  }

  void _initHomeViewModel() {
    print("Init HomeViewModel");
  }
}
