import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class EcomuHomeTitle extends StatelessWidget {
  const EcomuHomeTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(left: 16.0, top: 16.0),
      child: Text(
        "Quick Actions",
        style: TextStyle(
          fontSize: 24,
        ),
      ),
    );
  }
}
