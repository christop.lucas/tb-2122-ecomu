/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Home Screen
 
  Purpose: This file contains the home view.

  Author:  Christopher Lucas
  Date:    04.06.2022
*/
import 'package:ecomu_gestion/screens/teacher_screen/teacher_viewmodel.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_column_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_row_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_title_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EcomuHomeView extends StatelessWidget {
  const EcomuHomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    context.read<TeacherViewModel>();
    return EcomuSection(
      child: Column(),
    );
  }
}
