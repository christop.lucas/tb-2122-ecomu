/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Login
 
  Purpose: This file contains the Login View Model

  Author:  Christopher Lucas
  Date:    12.07.2022
*/
import 'package:ecomu_gestion/services/ecomu_repository/ecomu_repository.dart';
import 'package:flutter/material.dart';

class LoginViewModel extends ChangeNotifier {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final EcomuRepository _repository;

  LoginViewModel({required EcomuRepository repository})
      : _repository = repository;

  bool signIn() {
    final result = _repository.signIn(
      usernameController.text,
      passwordController.text,
    );
    _clearControllers();
    return result;
  }

  void _clearControllers() {
    usernameController.clear();
    passwordController.clear();
  }
}
