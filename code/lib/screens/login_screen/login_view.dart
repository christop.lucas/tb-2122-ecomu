/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Login
 
  Purpose: This file contains the widget of the Login View

  Author:  Christopher Lucas
  Date:    12.07.2022
*/
import 'package:ecomu_gestion/app/viewmodel/app_viewmodel.dart';
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/screens/login_screen/login_viewmodel.dart';
import 'package:ecomu_gestion/screens/login_screen/widgets/login_form_widget.dart';
import 'package:ecomu_gestion/services/ecomu_helper.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final _key = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final viewModel = context.read<LoginViewModel>();
    return LoginForm(
      keyForm: _key,
      image: const AssetImage("images/logo.png"),
      title: local.login_username_text,
      usernameController: viewModel.usernameController,
      passwordController: viewModel.passwordController,
      usernameHintText: local.login_username_hint_text,
      usernameText: local.login_username_text,
      passwordHintText: local.login_password_hint_text,
      passwordText: local.login_password_text,
      btnAction: () {
        if (_key.currentState!.validate()) {
          final result = viewModel.signIn();
          context.read<AppViewModel>().setConnected = result;
          if (!result) {
            Future.delayed(Duration.zero, () {
              final local = AppLocalizations.of(context)!;
              final snackBar = SnackBar(
                content: Text(
                  local.login_error,
                  textAlign: TextAlign.end,
                ),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            });
          }
        }
      },
      btnText: local.login_btn_connect,
    );
  }
}
