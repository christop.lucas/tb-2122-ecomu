/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Login
 
  Purpose: This file contains the widget of the Login View

  Author:  Christopher Lucas
  Date:    12.07.2022
*/
import 'package:ecomu_gestion/services/ecomu_helper.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:flutter/material.dart';

class LoginForm extends StatefulWidget {
  /// Form key used to validate form
  final GlobalKey<FormState> keyForm;

  /// Form Logo
  final AssetImage image;

  /// Logo Title
  final String title;

  /// Username Textfield controller
  final TextEditingController usernameController;

  /// Username Textfield controller
  final TextEditingController passwordController;

  /// Text above username TextField
  final String usernameText;

  /// Username Textfield hint text
  final String usernameHintText;

  /// Text above password TextField
  final String passwordText;

  /// Password Textfield hint text
  final String passwordHintText;

  /// Action when button is clicked
  final void Function() btnAction;

  /// Text on the button
  final String btnText;
  const LoginForm({
    Key? key,
    required this.keyForm,
    required this.image,
    required this.title,
    required this.usernameController,
    required this.passwordController,
    required this.usernameText,
    required this.usernameHintText,
    required this.passwordText,
    required this.passwordHintText,
    required this.btnAction,
    required this.btnText,
  }) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  @override
  Widget build(BuildContext context) {
    const style = TextStyle(fontSize: 18.0);
    return LayoutBuilder(builder: (context, constraints) {
      return ContentSpacing(
        priority: constraints.biggest.width < 1300 ? 5 : 2,
        child: Form(
          key: widget.keyForm,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 24.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Spacer(),
                            Image(
                              image: widget.image,
                              height: 60,
                              width: 60,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: Text(
                                widget.title,
                                style: style,
                              ),
                            ),
                            const Spacer(),
                          ],
                        ),
                      ),
                      Text(
                        widget.usernameText,
                        style: style,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 16.0),
                        child: TextFormField(
                          controller: widget.usernameController,
                          decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            hintText: widget.usernameHintText,
                          ),
                          validator: EcomuHelper.simpleValidator,
                        ),
                      ),
                      Text(
                        widget.passwordText,
                        style: style,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: TextFormField(
                          obscureText: true,
                          controller: widget.passwordController,
                          decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            hintText: widget.passwordHintText,
                          ),
                          validator: EcomuHelper.simpleValidator,
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 16.0),
                          child: ElevatedButton(
                            onPressed: widget.btnAction,
                            style: ElevatedButton.styleFrom(
                              onPrimary:
                                  Theme.of(context).colorScheme.onPrimary,
                              primary: Theme.of(context).colorScheme.primary,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6.0),
                              ),
                            ),
                            child: Text(widget.btnText),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
