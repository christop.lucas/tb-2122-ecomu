/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Other
 
  Purpose: This file contains the Widget to change app color.

  Author:  Christopher Lucas
  Date:    06.07.2022
*/
import 'package:ecomu_gestion/app/viewmodel/theme_viewmodel.dart';
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_title_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_whitespace.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OtherColorParam extends StatelessWidget {
  const OtherColorParam({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<ThemeViewModel>();
    final local = AppLocalizations.of(context)!;
    return Expanded(
      flex: 5,
      child: Column(
        children: [
          EcomuSectionTitle(
            text: local.other_color_title,
            padding: const EdgeInsets.symmetric(vertical: 16.0),
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: GridView.count(
                    controller: ScrollController(),
                    crossAxisCount: 8,
                    crossAxisSpacing: 4.0,
                    mainAxisSpacing: 8.0,
                    children: List.generate(
                      viewModel.availableColors.length,
                      (index) {
                        return Selector<ThemeViewModel, int>(
                          builder: (context, value, child) {
                            return ClipOval(
                              child: Material(
                                color: viewModel.availableColors[index],
                                child: InkWell(
                                  onTap: (viewModel.selectedColor == index)
                                      ? null
                                      : () {
                                          viewModel.setAppColor = index;
                                        },
                                  child: (viewModel.selectedColor == index)
                                      ? const SizedBox(
                                          width: 56,
                                          height: 56,
                                          child: Icon(Icons.check),
                                        )
                                      : null,
                                ),
                              ),
                            );
                          },
                          selector: (_, __) => viewModel.selectedColor,
                        );
                      },
                    ),
                  ),
                ),
                const WhiteSpace(isActive: true),
                const WhiteSpace(isActive: true),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
