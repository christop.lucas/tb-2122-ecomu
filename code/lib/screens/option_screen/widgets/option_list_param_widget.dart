/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Option
 
  Purpose: This file contains the Widget for the list of items in database.

  Author:  Christopher Lucas
  Date:    06.07.2022
*/
import 'package:ecomu_gestion/screens/option_screen/option_viewmodel.dart';
import 'package:ecomu_gestion/widgets/ecomu_icon_btn_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OptionListParam extends StatelessWidget {
  final Future<List<dynamic>> futureList;
  final TextEditingController modifyController;
  final void Function(dynamic) actionEdit;
  final void Function(dynamic) actionDelete;
  final void Function(dynamic) actionSelect;
  const OptionListParam({
    Key? key,
    required this.futureList,
    required this.modifyController,
    required this.actionSelect,
    required this.actionDelete,
    required this.actionEdit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.watch<OptionViewModel>();
    return Expanded(
      child: FutureBuilder<List<dynamic>>(
        future: futureList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final list = snapshot.data!;
            return ListView.separated(
              separatorBuilder: (context, index) {
                return const Divider(
                  indent: 20,
                  height: 0.1,
                );
              },
              controller: ScrollController(),
              itemCount: list.length,
              itemBuilder: (context, index) {
                final selected = viewModel.hasSelected(list[index]);
                return ListTile(
                  selected: selected,
                  title: viewModel.modify && selected
                      ? SizedBox(
                          height: 40,
                          child: TextField(
                            controller: modifyController,
                            style: const TextStyle(
                              height: 1.0,
                            ),
                          ),
                        )
                      : Text(list[index].toString()),
                  trailing: viewModel.modify && selected
                      ? Wrap(
                          children: [
                            IconButtonList(
                              icon: Icons.check,
                              color: Colors.green,
                              action: () {
                                actionEdit(list[index]);
                              },
                            ),
                            IconButtonList(
                              icon: Icons.cancel,
                              color: Theme.of(context).colorScheme.primary,
                              action: () {
                                viewModel.resetSelected(true);
                              },
                            ),
                          ],
                        )
                      : Wrap(
                          children: [
                            IconButtonList(
                              icon: Icons.edit,
                              action: () {
                                actionSelect(list[index]);
                              },
                            ),
                            IconButtonList(
                              icon: Icons.delete,
                              color: Theme.of(context).colorScheme.error,
                              action: () {
                                actionDelete(list[index]);
                              },
                            ),
                          ],
                        ),
                  onTap: () {
                    viewModel.setSelected(list[index]);
                  },
                  selectedColor: Theme.of(context).colorScheme.primary,
                );
              },
            );
          } else {
            return const SizedBox.shrink();
          }
        },
      ),
    );
  }
}
