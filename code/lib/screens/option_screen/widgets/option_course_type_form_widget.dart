/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Option
 
  Purpose: This file contains the Widget to add a new CourseType

  Author:  Christopher Lucas
  Date:    06.07.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';

import 'package:ecomu_gestion/screens/option_screen/option_viewmodel.dart';
import 'package:ecomu_gestion/services/ecomu_helper.dart';
import 'package:ecomu_gestion/widgets/ecomu_form/ecomu_user_form_field_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OptionCourseTypeForm extends StatefulWidget {
  const OptionCourseTypeForm({Key? key}) : super(key: key);

  @override
  State<OptionCourseTypeForm> createState() => _OptionCourseTypeFormState();
}

class _OptionCourseTypeFormState extends State<OptionCourseTypeForm> {
  final _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final viewModel = context.read<OptionViewModel>();
    return SingleChildScrollView(
      controller: ScrollController(),
      child: FocusTraversalGroup(
        child: Form(
          key: _key,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  top: 16.0,
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: EcomuFormField(
                          controller: viewModel.insertController,
                          hasLeftNeighbour: false,
                          text: local.other_type_cours_insert_hintText,
                          validator: EcomuHelper.simpleValidator,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Row(
                          children: [
                            Selector<OptionViewModel, bool?>(
                              builder: (context, toggle, child) {
                                return Checkbox(
                                  value: toggle,
                                  onChanged: (value) {
                                    viewModel.setInstrumentToggle = value!;
                                  },
                                );
                              },
                              selector: (_, __) =>
                                  viewModel.hasIntrumentalToggle,
                            ),
                            Flexible(
                              fit: FlexFit.loose,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Text(
                                  local.other_checkbox_instrumental_txt,
                                  style: const TextStyle(fontSize: 16),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 32.0),
                  child: Builder(
                    builder: (context) {
                      return Wrap(
                        spacing: 20,
                        children: [
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              onPrimary:
                                  Theme.of(context).colorScheme.onPrimary,
                              primary: Theme.of(context).colorScheme.primary,
                            ),
                            onPressed: () {
                              if (_key.currentState!.validate()) {
                                viewModel.insertCourseType();
                              }
                            },
                            child: Text(local.btn_insert_txt),
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
