/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Other
 
  Purpose: This file contains the Widget for the details of a specific option.

  Author:  Christopher Lucas
  Date:    06.07.2022
*/
import 'package:ecomu_gestion/widgets/ecomu_back_btn.dart';
import 'package:ecomu_gestion/widgets/ecomu_content_spacing.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_title_widget.dart';
import 'package:flutter/material.dart';

class OptionParamDetails extends StatelessWidget {
  final void Function() backFunction;

  /// Title for the page
  final String title;

  final Widget child;
  const OptionParamDetails({
    Key? key,
    required this.title,
    required this.child,
    required this.backFunction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ReturnButton(
          action: backFunction,
        ),
        Expanded(
          child: ContentSpacing(
            priority: 10,
            child: Column(
              children: [
                EcomuSectionTitle(
                  text: title,
                ),
                const Divider(),
                child,
              ],
            ),
          ),
        ),
      ],
    );
  }
}
