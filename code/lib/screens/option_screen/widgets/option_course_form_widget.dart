/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Option
 
  Purpose: This file contains the Widget to add a new Course.

  Author:  Christopher Lucas
  Date:    06.07.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/duration.dart';
import 'package:ecomu_gestion/models/type_course.dart';
import 'package:ecomu_gestion/screens/option_screen/option_viewmodel.dart';
import 'package:ecomu_gestion/services/ecomu_helper.dart';
import 'package:ecomu_gestion/widgets/ecomu_form/ecomu_user_form_field_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OptionCourseForm extends StatefulWidget {
  const OptionCourseForm({Key? key}) : super(key: key);

  @override
  State<OptionCourseForm> createState() => _OptionCourseFormState();
}

class _OptionCourseFormState extends State<OptionCourseForm> {
  final _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final viewModel = context.read<OptionViewModel>();
    return Form(
      key: _key,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              top: 16.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: FutureBuilder<List<CourseType>>(
                      future: viewModel.getCourseTypes(),
                      builder: (context, snapshot) {
                        var courseTypes = <CourseType>[];
                        if (snapshot.hasData) {
                          courseTypes = snapshot.data!;
                        }
                        return DropdownButtonFormField<CourseType>(
                          key: viewModel.typeController,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: "Type de cours",
                          ),
                          value: viewModel.selectedCourseType,
                          hint: Text(local.form_person_dropdown_placeholder),
                          onChanged: (CourseType? value) {
                            viewModel.setSelectedCourseType = value;
                          },
                          items: courseTypes.map((CourseType value) {
                            return DropdownMenuItem<CourseType>(
                              value: value,
                              child: Text(value.name),
                            );
                          }).toList(),
                          validator: (value) => null,
                        );
                      },
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: EcomuFormField(
                      controller: viewModel.nameController,
                      hasLeftNeighbour: false,
                      text: "Nom du cours",
                      validator: EcomuHelper.simpleValidator,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: FutureBuilder<List<CourseDuration>>(
                        future: viewModel.getDurations(),
                        builder: (context, snapshot) {
                          var durations = <CourseDuration>[];
                          if (snapshot.hasData) {
                            durations = snapshot.data!;
                          }
                          return DropdownButtonFormField<CourseDuration?>(
                            key: viewModel.durationController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Durée du cours",
                            ),
                            value: viewModel.selectedDuration,
                            hint: Text(local.form_person_dropdown_placeholder),
                            onChanged: (CourseDuration? value) {
                              viewModel.setSelectedDuration = value;
                            },
                            items: durations.map((CourseDuration value) {
                              return DropdownMenuItem<CourseDuration>(
                                value: value,
                                child: Text(value.formatedDuration),
                              );
                            }).toList(),
                            validator: (value) => null,
                          );
                        }),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: EcomuFormField(
                      controller: viewModel.childPriceController,
                      hasLeftNeighbour: false,
                      text: "Prix enfant",
                      validator: EcomuHelper.numberValidator,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: EcomuFormField(
                      controller: viewModel.adultPriceController,
                      hasLeftNeighbour: false,
                      text: "Prix adulte",
                      validator: EcomuHelper.numberValidator,
                    ),
                  ),
                ),
                const Expanded(
                  flex: 1,
                  child: SizedBox.shrink(),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16.0,
            ),
            child: Selector<OptionViewModel, bool>(
              builder: (context, show, child) {
                if (show) {
                  return Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: EcomuFormField(
                            controller:
                                viewModel.childPriceInstrumentController,
                            hasLeftNeighbour: false,
                            text: "Prix enfant avec instrumental",
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: EcomuFormField(
                            controller:
                                viewModel.adultPriceInstrumentController,
                            hasLeftNeighbour: false,
                            text: "Prix adulte avec instrumental",
                          ),
                        ),
                      ),
                      const Expanded(
                        flex: 1,
                        child: SizedBox.shrink(),
                      ),
                    ],
                  );
                } else {
                  return const SizedBox.shrink();
                }
              },
              selector: (_, __) => viewModel.showInstrumentalFields,
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 32.0),
              child: Builder(
                builder: (context) {
                  return Wrap(
                    spacing: 20,
                    children: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          onPrimary: Theme.of(context).colorScheme.onPrimary,
                          primary: Theme.of(context).colorScheme.primary,
                        ),
                        onPressed: () {
                          if (_key.currentState!.validate()) {
                            viewModel.insertCourse();
                          }
                        },
                        child: Text(local.btn_next_text),
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
