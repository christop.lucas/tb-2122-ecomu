/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Option
 
  Purpose: This file contains the Widget to insert a new value in database.

  Author:  Christopher Lucas
  Date:    06.07.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/services/ecomu_helper.dart';
import 'package:flutter/material.dart';

class OptionInsert extends StatefulWidget {
  final void Function() action;
  final String hintText;
  final String labelText;
  final TextEditingController insertController;

  const OptionInsert({
    Key? key,
    required this.action,
    required this.hintText,
    required this.labelText,
    required this.insertController,
  }) : super(key: key);

  @override
  State<OptionInsert> createState() => _OptionInsertState();
}

class _OptionInsertState extends State<OptionInsert> {
  final _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    return Form(
      key: _key,
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: TextFormField(
                    controller: widget.insertController,
                    decoration: InputDecoration(
                      hintText: widget.hintText,
                      labelText: widget.labelText,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    validator: EcomuHelper.simpleValidator,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              // Whitespace
              const Expanded(
                child: SizedBox.shrink(),
              ),
              ElevatedButton.icon(
                icon: const Icon(Icons.add),
                label: Text(local.btn_insert_txt),
                onPressed: () {
                  if (_key.currentState!.validate()) {
                    widget.action();
                  }
                },
                style: ElevatedButton.styleFrom(
                  onPrimary: Theme.of(context).colorScheme.onPrimary,
                  primary: Theme.of(context).colorScheme.primary,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
