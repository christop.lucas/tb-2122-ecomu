/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Option
 
  Purpose: This file contains the Widget for the list of options for the
      Option View.

  Author:  Christopher Lucas
  Date:    06.07.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/screens/option_screen/option_viewmodel.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_title_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OptionListParams extends StatelessWidget {
  const OptionListParams({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final params = local.params_inscriptions.split(local.splitter);
    final viewModel = context.read<OptionViewModel>();
    return EcomuSection(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          children: [
            EcomuSectionTitle(
              padding: const EdgeInsets.only(top: 8.0),
              text: local.other_title_params,
              style: const TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.normal,
              ),
            ),
            const Divider(),
            Expanded(
              child: ListView.separated(
                controller: ScrollController(),
                itemCount: params.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    minVerticalPadding: 16.0,
                    title: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        params[index],
                      ),
                    ),
                    trailing: const Icon(Icons.arrow_forward_ios),
                    onTap: () {
                      viewModel.actionListItem(index);
                    },
                  );
                },
                separatorBuilder: (_, __) {
                  return const Divider(
                    height: 0.1,
                    indent: 20,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
