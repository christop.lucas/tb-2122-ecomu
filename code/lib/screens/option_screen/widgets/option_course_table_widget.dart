/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Option
 
  Purpose: This file contains the Widget to display the course table

  Author:  Christopher Lucas
  Date:    06.07.2022
*/
import 'package:ecomu_gestion/models/course.dart';
import 'package:ecomu_gestion/screens/option_screen/option_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class OptionCourseTable extends StatelessWidget {
  final Future<List<Course>> futureCourses;
  const OptionCourseTable({
    Key? key,
    required this.futureCourses,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read<OptionViewModel>();

    return Expanded(
      child: FutureBuilder<List<Course>>(
        future: futureCourses,
        builder: (context, snapshot) {
          var courses = <Course>[];
          if (snapshot.hasData) {
            courses = snapshot.data!;
          }
          final source = CourseDataSource(courses: courses);
          return SfDataGrid(
            source: source,
            columns: [
              GridColumn(
                columnWidthMode: ColumnWidthMode.fitByColumnName,
                columnName: 'Type de cours',
                label: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  alignment: Alignment.centerLeft,
                  child: const Text(
                    'Type de cours',
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              GridColumn(
                columnWidthMode: ColumnWidthMode.fitByCellValue,
                columnName: 'name',
                label: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  alignment: Alignment.centerLeft,
                  child: const Text(
                    'Nom du cours',
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              GridColumn(
                columnWidthMode: ColumnWidthMode.fitByColumnName,
                columnName: 'Duree du cours',
                label: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  alignment: Alignment.centerLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      Text(
                        'Durée du cours',
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              ),
              GridColumn(
                columnWidthMode: ColumnWidthMode.fill,
                columnName: 'childPrice',
                label: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  alignment: Alignment.centerLeft,
                  child: const Text(
                    'Prix enfant / (avec instrumental)',
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              GridColumn(
                columnWidthMode: ColumnWidthMode.fill,
                columnName: 'adultPrice',
                label: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  alignment: Alignment.centerLeft,
                  child: const Text(
                    'Prix adulte / (avec instrumental)',
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ],
            selectionMode: SelectionMode.single,
            onSelectionChanged: (addedRows, removedRows) {
              final index = source.dataGridRows.indexOf(addedRows.last);
              viewModel.setSelectedCourse = courses[index];
            },
          );
        },
      ),
    );
  }
}

class CourseDataSource extends DataGridSource {
  final List<Course> courses;
  CourseDataSource({
    required this.courses,
  }) {
    buildDataGridRows();
  }

  List<DataGridRow> dataGridRows = [];

  @override
  List<DataGridRow> get rows => dataGridRows;

  @override
  DataGridRowAdapter? buildRow(DataGridRow row) {
    return DataGridRowAdapter(
      cells: row.getCells().map<Widget>(
        (dataGridCell) {
          return Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(
              dataGridCell.value.toString(),
              overflow: TextOverflow.ellipsis,
            ),
          );
        },
      ).toList(),
    );
  }

  void buildDataGridRows() {
    dataGridRows = courses
        .map<DataGridRow>(
          (course) => DataGridRow(
            cells: [
              DataGridCell<String>(
                columnName: 'Type de cours',
                value: course.courseTypeName,
              ),
              DataGridCell<String>(
                columnName: 'name',
                value: course.name,
              ),
              DataGridCell<String>(
                columnName: 'duration',
                value: course.displayDuration(),
              ),
              DataGridCell<String>(
                columnName: 'childPrice',
                value: course.childPriceCombined(),
              ),
              DataGridCell<String>(
                columnName: 'adultPrice',
                value: course.adultPriceCombined(),
              ),
            ],
          ),
        )
        .toList(growable: false);
  }
}
