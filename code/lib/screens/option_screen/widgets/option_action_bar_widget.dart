/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Option
 
  Purpose: This file contains the Widget for the action bar for the Option View

  Author:  Christopher Lucas
  Date:    06.07.2022
*/
import 'package:ecomu_gestion/app/widgets/ecomu_top_bar_widget.dart';
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/models/course.dart';
import 'package:ecomu_gestion/models/duration.dart';
import 'package:ecomu_gestion/models/instrument.dart';
import 'package:ecomu_gestion/models/section.dart';
import 'package:ecomu_gestion/models/type_course.dart';
import 'package:ecomu_gestion/models/workshop.dart';
import 'package:ecomu_gestion/screens/option_screen/option_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OptionActionBar extends StatelessWidget {
  const OptionActionBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final viewModel = context.read<OptionViewModel>();
    final primaryStyle = ElevatedButton.styleFrom(
      onPrimary: Theme.of(context).colorScheme.onPrimary,
      primary: Theme.of(context).colorScheme.primary,
    );
    return EcomuTopBar(
      children: Selector<OptionViewModel, OptionState>(
        builder: (context, state, child) {
          switch (state) {
            case OptionState.section:
              return Row(
                children: [
                  ElevatedButton.icon(
                    onPressed: viewModel.insertActionSection,
                    icon: const Icon(Icons.add),
                    label: Text(local.other_new_section),
                    style: primaryStyle,
                  ),
                  Selector<OptionViewModel, Section?>(
                    builder: (context, section, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: section == null
                              ? null
                              : () {
                                  viewModel.setSelectedSection = section;
                                },
                          icon: const Icon(Icons.edit),
                          label: Text(local.btn_update_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedSection,
                  ),
                  Selector<OptionViewModel, Section?>(
                    builder: (context, section, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: section == null
                              ? null
                              : () {
                                  viewModel.deleteSection(section);
                                },
                          icon: const Icon(Icons.delete),
                          label: Text(local.btn_delete_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedSection,
                  ),
                ],
              );
            case OptionState.courseType:
              return Row(
                children: [
                  ElevatedButton.icon(
                    onPressed: viewModel.insertActionCourseType,
                    icon: const Icon(Icons.add),
                    label: Text(local.other_new_type_cours),
                    style: primaryStyle,
                  ),
                  Selector<OptionViewModel, CourseType?>(
                    builder: (context, courseType, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: courseType == null
                              ? null
                              : () {
                                  viewModel.setSelectedCourseType = courseType;
                                },
                          icon: const Icon(Icons.edit),
                          label: Text(local.btn_update_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedCourseType,
                  ),
                  Selector<OptionViewModel, CourseType?>(
                    builder: (context, courseType, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: courseType == null
                              ? null
                              : () {
                                  viewModel.deleteCourseType(courseType);
                                },
                          icon: const Icon(Icons.delete),
                          label: Text(local.btn_delete_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedCourseType,
                  ),
                ],
              );
            case OptionState.course:
              return Row(
                children: [
                  ElevatedButton.icon(
                    onPressed: viewModel.insertActionCourse,
                    icon: const Icon(Icons.add),
                    label: Text(local.other_new_cours),
                    style: primaryStyle,
                  ),
                  Selector<OptionViewModel, Course?>(
                    builder: (context, course, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: course == null
                              ? null
                              : () {
                                  viewModel.setSelectedCourse = course;
                                },
                          icon: const Icon(Icons.edit),
                          label: Text(local.btn_update_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedCourse,
                  ),
                  Selector<OptionViewModel, Course?>(
                    builder: (context, course, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: course == null
                              ? null
                              : () {
                                  viewModel.deleteCourse(course);
                                },
                          icon: const Icon(Icons.delete),
                          label: Text(local.btn_delete_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedCourse,
                  ),
                ],
              );
            case OptionState.instrument:
              return Row(
                children: [
                  ElevatedButton.icon(
                    onPressed: viewModel.insertActionInstrument,
                    icon: const Icon(Icons.add),
                    label: Text(local.other_new_instrument),
                    style: primaryStyle,
                  ),
                  Selector<OptionViewModel, Instrument?>(
                    builder: (context, instrument, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: instrument == null
                              ? null
                              : () {
                                  viewModel.setSelectedInsturment = instrument;
                                },
                          icon: const Icon(Icons.edit),
                          label: Text(local.btn_update_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedInstrument,
                  ),
                  Selector<OptionViewModel, Instrument?>(
                    builder: (context, instrument, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: instrument == null
                              ? null
                              : () {
                                  viewModel.deleteInstrument(instrument);
                                },
                          icon: const Icon(Icons.delete),
                          label: Text(local.btn_delete_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedInstrument,
                  ),
                ],
              );
            case OptionState.workshop:
              return Row(
                children: [
                  ElevatedButton.icon(
                    onPressed: viewModel.insertActionWorkshop,
                    icon: const Icon(Icons.add),
                    label: Text(local.other_new_workshop),
                    style: primaryStyle,
                  ),
                  Selector<OptionViewModel, Workshop?>(
                    builder: (context, workshop, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: workshop == null
                              ? null
                              : () {
                                  viewModel.setSelectedWorkshop = workshop;
                                },
                          icon: const Icon(Icons.edit),
                          label: Text(local.btn_update_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedWorkshop,
                  ),
                  Selector<OptionViewModel, Workshop?>(
                    builder: (context, workshop, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: workshop == null
                              ? null
                              : () {
                                  viewModel.deleteWorkshop(workshop);
                                },
                          icon: const Icon(Icons.delete),
                          label: Text(local.btn_delete_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedWorkshop,
                  ),
                ],
              );
            case OptionState.collectif:
              return Row(
                children: [
                  ElevatedButton.icon(
                    onPressed: viewModel.insertActionCollectiveCourse,
                    icon: const Icon(Icons.add),
                    label: Text(local.other_new_cours_collectif),
                    style: primaryStyle,
                  ),
                  Selector<OptionViewModel, Course?>(
                    builder: (context, course, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: course == null
                              ? null
                              : () {
                                  viewModel.setSelectedCourse = course;
                                },
                          icon: const Icon(Icons.edit),
                          label: Text(local.btn_update_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedCourse,
                  ),
                  Selector<OptionViewModel, Course?>(
                    builder: (context, course, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: course == null
                              ? null
                              : () {
                                  viewModel.deleteCourse(course);
                                },
                          icon: const Icon(Icons.delete),
                          label: Text(local.btn_delete_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedCourse,
                  ),
                ],
              );
            case OptionState.duration:
              return Row(
                children: [
                  ElevatedButton.icon(
                    onPressed: viewModel.insertActionDuration,
                    icon: const Icon(Icons.add),
                    label: Text(local.other_new_duration),
                    style: primaryStyle,
                  ),
                  Selector<OptionViewModel, CourseDuration?>(
                    builder: (context, duration, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: duration == null
                              ? null
                              : () {
                                  viewModel.setSelectedDuration = duration;
                                },
                          icon: const Icon(Icons.edit),
                          label: Text(local.btn_update_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedDuration,
                  ),
                  Selector<OptionViewModel, CourseDuration?>(
                    builder: (context, duration, child) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: TextButton.icon(
                          onPressed: duration == null
                              ? null
                              : () {
                                  viewModel.deleteDuration(duration);
                                },
                          icon: const Icon(Icons.delete),
                          label: Text(local.btn_delete_txt),
                        ),
                      );
                    },
                    selector: (_, __) => viewModel.selectedDuration,
                  ),
                ],
              );
            default:
              return Row(
                children: const [
                  TextButton(
                    onPressed: null,
                    child: SizedBox.shrink(),
                  ),
                ],
              );
          }
        },
        selector: (_, __) => viewModel.state,
      ),
    );
  }
}
