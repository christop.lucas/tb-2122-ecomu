/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Option
 
  Purpose: This file chooses wich option to display.

  Author:  Christopher Lucas
  Date:    06.07.2022
*/
import 'package:ecomu_gestion/l10n/app_localizations.dart';
import 'package:ecomu_gestion/screens/option_screen/option_viewmodel.dart';
import 'package:ecomu_gestion/screens/option_screen/widgets/option_collective_course_form_widget.dart';
import 'package:ecomu_gestion/screens/option_screen/widgets/option_course_form_widget.dart';
import 'package:ecomu_gestion/screens/option_screen/widgets/option_course_table_widget.dart';
import 'package:ecomu_gestion/screens/option_screen/widgets/option_course_type_form_widget.dart';
import 'package:ecomu_gestion/screens/option_screen/widgets/option_insert_duration_widget.dart';
import 'package:ecomu_gestion/screens/option_screen/widgets/option_insert_widget.dart';
import 'package:ecomu_gestion/screens/option_screen/widgets/option_list_param_widget.dart';
import 'package:ecomu_gestion/screens/option_screen/widgets/option_param_details.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OptionContent extends StatelessWidget {
  const OptionContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final local = AppLocalizations.of(context)!;
    final viewModel = context.read<OptionViewModel>();
    return EcomuSection(
      priority: 4,
      padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
      child: Selector<OptionViewModel, OptionState>(
        builder: (context, state, child) {
          switch (state) {
            case OptionState.formSection:
              return OptionParamDetails(
                backFunction: viewModel.back2Section,
                title: local.other_section_insert_title,
                child: OptionInsert(
                  hintText: local.other_section_insert_hintText,
                  labelText: local.other_section_insert_labelText,
                  insertController: viewModel.insertController,
                  action: viewModel.insertSection,
                ),
              );
            case OptionState.section:
              return OptionParamDetails(
                backFunction: viewModel.back2Initial,
                title: local.other_section_title,
                child: Selector<OptionViewModel, bool>(
                  builder: (context, value, child) {
                    return OptionListParam(
                      futureList: viewModel.getSections(),
                      modifyController: viewModel.modifyController,
                      actionDelete: (section) {
                        viewModel.deleteSection(section);
                      },
                      actionSelect: (section) {
                        viewModel.setSelectedSection = section;
                      },
                      actionEdit: (section) {},
                    );
                  },
                  selector: (_, __) => viewModel.updater,
                ),
              );
            case OptionState.courseType:
              return OptionParamDetails(
                backFunction: viewModel.back2Initial,
                title: local.other_type_cours_title,
                child: Selector<OptionViewModel, bool>(
                  builder: (context, value, child) {
                    return OptionListParam(
                      futureList: viewModel.getCourseTypes(),
                      modifyController: viewModel.modifyController,
                      actionDelete: (courseType) {
                        viewModel.deleteCourseType(courseType);
                      },
                      actionSelect: (courseType) {
                        viewModel.setSelectedCourseType = courseType;
                      },
                      actionEdit: (courseType) {},
                    );
                  },
                  selector: (_, __) => viewModel.updater,
                ),
              );
            case OptionState.formCourseType:
              return OptionParamDetails(
                backFunction: viewModel.back2CourseType,
                title: local.other_type_cours_insert_title,
                child: const OptionCourseTypeForm(),
              );
            case OptionState.instrument:
              return OptionParamDetails(
                backFunction: viewModel.back2Initial,
                title: local.other_instrument_title,
                child: Selector<OptionViewModel, bool>(
                  builder: (context, _, child) {
                    return OptionListParam(
                      futureList: viewModel.getInstruments(),
                      modifyController: viewModel.modifyController,
                      actionEdit: (instrument) {}, //viewModel.updateInstrument,
                      actionDelete: (instrument) {
                        viewModel.deleteInstrument(instrument);
                      },
                      actionSelect: (instrument) {
                        viewModel.setSelectedInsturment = instrument;
                      },
                    );
                  },
                  selector: (_, __) => viewModel.updater,
                ),
              );
            case OptionState.formInstrument:
              return OptionParamDetails(
                backFunction: viewModel.back2Instrument,
                title: local.other_instrument_insert_title,
                child: OptionInsert(
                  hintText: local.other_instrument_insert_hintText,
                  labelText: local.other_instrument_insert_labelText,
                  insertController: viewModel.insertController,
                  action: viewModel.insertInstrument,
                ),
              );
            case OptionState.workshop:
              return OptionParamDetails(
                backFunction: viewModel.back2Initial,
                title: local.other_atelier_title,
                child: Selector<OptionViewModel, bool>(
                  builder: (context, _, child) {
                    return OptionListParam(
                      futureList: viewModel.getWorkshops(),
                      modifyController: viewModel.modifyController,
                      actionEdit: (workshop) {}, //viewModel.updateWorkshop,
                      actionDelete: (workshop) {
                        viewModel.deleteWorkshop(workshop);
                      },
                      actionSelect: (workshop) {
                        viewModel.setSelectedWorkshop = workshop;
                      },
                    );
                  },
                  selector: (_, __) => viewModel.updater,
                ),
              );
            case OptionState.formWorkshop:
              return OptionParamDetails(
                backFunction: viewModel.back2Workshop,
                title: local.other_atelier_insert_title,
                child: OptionInsert(
                  hintText: local.other_atelier_insert_hintText,
                  labelText: local.other_atelier_insert_labelText,
                  insertController: viewModel.insertController,
                  action: viewModel.insertWorkshop,
                ),
              );
            case OptionState.duration:
              return OptionParamDetails(
                backFunction: viewModel.back2Initial,
                title: local.other_duree_title,
                child: Selector<OptionViewModel, bool>(
                  builder: (context, _, child) {
                    return OptionListParam(
                      futureList: viewModel.getDurations(),
                      modifyController: viewModel.modifyController,
                      actionEdit: (workshop) {}, //viewModel.updateWorkshop,
                      actionDelete: (duration) {
                        viewModel.deleteDuration(duration);
                      },
                      actionSelect: (duration) {
                        viewModel.setSelectedDuration = duration;
                      },
                    );
                  },
                  selector: (_, __) => viewModel.updater,
                ),
              );
            case OptionState.formDuration:
              return OptionParamDetails(
                backFunction: viewModel.back2Duration,
                title: local.other_duree_insert_title,
                child: OptionInsertDuration(
                  hintText: local.other_duree_insert_hintText,
                  labelText: local.other_duree_insert_labelText,
                  insertController: viewModel.insertController,
                  action: viewModel.insertDuration,
                ),
                /*OptionInsert(
                  hintText: local.other_duree_insert_hintText,
                  labelText: local.other_duree_insert_labelText,
                  insertController: viewModel.insertController,
                  action: viewModel.insertDuration,
                ),*/
              );
            case OptionState.collectif:
              return OptionParamDetails(
                backFunction: viewModel.back2Initial,
                title: local.other_cours_collectifs_title,
                child: Selector<OptionViewModel, bool>(
                  builder: (context, _, child) {
                    return OptionCourseTable(
                      futureCourses: viewModel.getCollectiveCourses(),
                    );
                  },
                  selector: (_, __) => viewModel.updater,
                ),
              );
            case OptionState.formCollectiveCourse:
              return OptionParamDetails(
                backFunction: viewModel.back2CollectiveCourse,
                title: local.other_cours_collectifs_insert_title,
                child: const OptionCollectiveCourseForm(),
              );
            case OptionState.course:
              return OptionParamDetails(
                backFunction: viewModel.back2Initial,
                title: local.other_cours_title,
                child: Selector<OptionViewModel, bool>(
                  builder: (context, value, child) {
                    return OptionCourseTable(
                      futureCourses: viewModel.getCourses(),
                    );
                  },
                  selector: (_, __) => viewModel.updater,
                ),
              );
            case OptionState.formCourse:
              return OptionParamDetails(
                backFunction: viewModel.back2Course,
                title: local.other_cours_insert_title,
                child: const OptionCourseForm(),
              );
            default:
              return Column();
          }
        },
        selector: (_, __) => viewModel.state,
      ),
    );
  }
}
