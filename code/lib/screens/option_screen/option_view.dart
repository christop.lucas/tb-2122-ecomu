/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Option
 
  Purpose: This file contains the Widget for the View

  Author:  Christopher Lucas
  Date:    06.07.2022
*/
import 'package:ecomu_gestion/screens/option_screen/widgets/option_content_widget.dart';
import 'package:ecomu_gestion/screens/option_screen/widgets/option_list_params_widget.dart';
import 'package:ecomu_gestion/widgets/ecomu_section_column_widget.dart';
import 'package:flutter/material.dart';

class EcomuOptionView extends StatelessWidget {
  const EcomuOptionView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const EcomuSectionColumn(
      children: [
        OptionListParams(),
        OptionContent(),
      ],
    );
  }
}
