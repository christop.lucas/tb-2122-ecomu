/*
  Copyright 2022 University of Applied Sciences Western Switzerland / Fribourg
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
  Project: HEIA-FR / Ecomu : logiciel de gestion pour école de musique
 
  Abstract: Other
 
  Purpose: This file contains the View Model for the Other View.

  Author:  Christopher Lucas
  Date:    06.07.2022
*/
import 'package:ecomu_gestion/models/course.dart';
import 'package:ecomu_gestion/models/duration.dart';
import 'package:ecomu_gestion/models/instrument.dart';
import 'package:ecomu_gestion/models/section.dart';
import 'package:ecomu_gestion/models/type_course.dart';
import 'package:ecomu_gestion/models/workshop.dart';
import 'package:ecomu_gestion/services/ecomu_repository/ecomu_repository.dart';
import 'package:flutter/material.dart';

enum OptionState {
  initial,
  section,
  courseType,
  course,
  instrument,
  workshop,
  collectif,
  duration,
  color,
  language,
  formSection,
  formCourseType,
  formCourse,
  formInstrument,
  formWorkshop,
  formCollectiveCourse,
  formDuration
}

class OptionViewModel extends ChangeNotifier {
  var _state = OptionState.initial;
  final EcomuRepository _repository;
  final insertController = TextEditingController();
  final modifyController = TextEditingController();
  var _updater = false;
  var _modify = false;
  var showInstrumentalFields = true;

  /* --- Course type insert form ---------------------------- */

  bool _hasInstrumentalToggle = false;

  /* --- Course insert form ---------------------------- */
  final nameController = TextEditingController();
  final typeController = GlobalKey<FormFieldState>();
  final durationController = GlobalKey<FormFieldState>();
  final childPriceController = TextEditingController();
  final adultPriceController = TextEditingController();
  final childPriceInstrumentController = TextEditingController();
  final adultPriceInstrumentController = TextEditingController();

  CourseType? _selectedCourseType;

  CourseDuration? _selectedDuration;

  Course? _selectedCourse;

  Section? _selectedSection;

  Instrument? _selectedInstrument;

  Workshop? _selectedWorkshop;

  OptionViewModel({required EcomuRepository repository})
      : _repository = repository;

  /* --- INSTRUMENTS ------------------------------------------- */
  Future<List<Instrument>> getInstruments() async {
    final result = await _repository.fetchInstruments();
    if (result != null) {
      return result;
    }
    return <Instrument>[];
  }

  void insertActionInstrument() {
    _state = OptionState.formInstrument;
    notifyListeners();
  }

  void insertInstrument() async {
    final result = await _repository.insertInstrument(insertController.text);
    if (result) {
      insertController.clear();
      _state = OptionState.instrument;
      notifyListeners();
    }
  }

  void updateInstrument(int index) {
    //_repository.instruments[index] = modifyController.text;
    notifyListeners();
  }

  void deleteInstrument(Instrument i) async {
    final result = await _repository.deleteInstrument(i);
    if (result) {
      _selectedInstrument = null;
      _updater = !_updater;
      notifyListeners();
    }
  }

  get selectedInstrument {
    return _selectedInstrument;
  }

  set setSelectedInsturment(Instrument? value) {
    _selectedInstrument = value;
    if (value != null) modifyController.text = value.name;
    _modify = true;
    notifyListeners();
  }

  void back2Instrument() {
    resetControllers();
    _state = OptionState.instrument;
    resetSelected(false);
    notifyListeners();
  }

/* --- ATELIERS ------------------------------------------- */
  Future<List<Workshop>> getWorkshops() async {
    final result = await _repository.fetchWorkshops();
    if (result != null) {
      return result;
    }
    return <Workshop>[];
  }

  void insertActionWorkshop() {
    _state = OptionState.formWorkshop;
    notifyListeners();
  }

  void insertWorkshop() async {
    final result = await _repository.insertWorkshop(insertController.text);
    if (result) {
      insertController.clear();
      _state = OptionState.workshop;
      notifyListeners();
    }
  }

  void updateWorkshop(int index) {
    //_repository.workshops[index] = modifyController.text;
    notifyListeners();
  }

  void deleteWorkshop(Workshop workshop) async {
    final result = await _repository.deleteWorkshop(workshop);
    if (result) {
      _selectedWorkshop = null;
      _updater = !_updater;
      notifyListeners();
    }
  }

  get selectedWorkshop {
    return _selectedWorkshop;
  }

  set setSelectedWorkshop(Workshop? value) {
    _selectedWorkshop = value;
    if (value != null) modifyController.text = value.workshop;
    _modify = true;
    notifyListeners();
  }

  void back2Workshop() {
    resetControllers();
    _state = OptionState.workshop;
    resetSelected(false);
    notifyListeners();
  }

/* --- COURS COLLECTIFS ------------------------------------------- */
  List<Course> get coursCollectifs {
    return _repository.collectiveCourses;
  }

  Future<List<Course>> getCollectiveCourses() async {
    final result = await _repository.fetchCollectiveCourses();
    if (result != null) {
      return result;
    }
    return <Course>[];
  }

  void insertActionCollectiveCourse() {
    _state = OptionState.formCollectiveCourse;
    notifyListeners();
  }

  void insertCollectiveCourse() async {
    final result = await _repository.insertCollectiveCourse(
      Course(
        idCourse: null,
        name: nameController.text,
        childPrice: int.parse(childPriceController.text),
        adultPrice: int.parse(adultPriceController.text),
        adultInstrumentalPrice: adultPriceInstrumentController.text.isEmpty
            ? 0
            : int.parse(adultPriceInstrumentController.text),
        childInstrumentalPrice: childPriceInstrumentController.text.isEmpty
            ? 0
            : int.parse(childPriceInstrumentController.text),
        duration: _selectedDuration!.duration,
      ),
    );
    if (result) {
      resetCourseForm(true);
      _state = OptionState.collectif;
      notifyListeners();
    }
  }

  void updateCollectiveCourse(int index) {
    //_repository.coursCollectifs[index] = modifyController.text;
    notifyListeners();
  }

  void back2CollectiveCourse() {
    resetControllers();
    _state = OptionState.collectif;
    resetSelected(false);
    notifyListeners();
  }

/* --- COURS ------------------------------------------- */
  List<Course> get courses {
    return _repository.courses;
  }

  Future<List<Course>> getCourses() async {
    final result = await _repository.fetchCourses();
    if (result != null) {
      return result;
    }
    return <Course>[];
  }

  void insertActionCourse() {
    _state = OptionState.formCourse;
    notifyListeners();
  }

  void insertCourse() async {
    final result = await _repository.insertCourse(
      Course(
        idCourse: null,
        name: nameController.text,
        childPrice: int.parse(childPriceController.text),
        adultPrice: int.parse(adultPriceController.text),
        idCourseType: selectedCourseType!.idCourseType,
        courseTypeName: selectedCourseType!.name,
        adultInstrumentalPrice: adultPriceInstrumentController.text.isEmpty
            ? 0
            : int.parse(adultPriceInstrumentController.text),
        childInstrumentalPrice: childPriceInstrumentController.text.isEmpty
            ? 0
            : int.parse(childPriceInstrumentController.text),
        duration: _selectedDuration!.duration,
      ),
    );
    if (result) {
      resetCourseForm(false);
      _state = OptionState.course;
      notifyListeners();
    }
  }

  void updateCourse(int index) {
    //_repository.coursCollectifs[index] = modifyController.text;
    notifyListeners();
  }

  void deleteCourse(Course course) async {
    final result = await _repository.deleteCourse(course);
    if (result) {
      _selectedCourse = null;
      _updater = !_updater;
      notifyListeners();
    }
  }

  get selectedCourse {
    return _selectedCourse;
  }

  set setSelectedCourse(Course? value) {
    _selectedCourse = value;
    notifyListeners();
  }

  void back2Course() {
    resetControllers();
    _state = OptionState.course;
    resetSelected(false);
    notifyListeners();
  }

/* --- DUREES COURS ------------------------------------------- */
  List<CourseDuration> get durations {
    return _repository.durations;
  }

  Future<List<CourseDuration>> getDurations() async {
    final result = await _repository.fetchDurations();
    if (result != null) {
      return result;
    }
    return <CourseDuration>[];
  }

  void insertActionDuration() {
    _state = OptionState.formDuration;
    notifyListeners();
  }

  void insertDuration() async {
    final date = insertController.text.split(":");
    final result = await _repository.insertDuration(
      int.parse(date[0]) * 3600000 + int.parse(date[1]) * 60000,
    );
    if (result) {
      insertController.clear();
      _state = OptionState.duration;
      notifyListeners();
    }
  }

  void updateDureeCours(int index) {
    // _repository.durees[index] = int.parse(modifyController.text);
    notifyListeners();
  }

  void deleteDuration(CourseDuration duration) async {
    final result = await _repository.deleteDuration(duration);
    if (result) {
      _selectedDuration = null;
      _updater = !_updater;
      notifyListeners();
    }
  }

  get selectedDuration {
    return _selectedDuration;
  }

  set setSelectedDuration(CourseDuration? value) {
    _selectedDuration = value;
    if (value != null) modifyController.text = value.formatedDuration;
    _modify = true;
    notifyListeners();
  }

  void back2Duration() {
    resetControllers();
    _state = OptionState.duration;
    resetSelected(false);
    notifyListeners();
  }

  /* --- ATELIERS ------------------------------------------- */
  List<Section> get sections {
    return _repository.sections.toList();
  }

  Future<List<Section>> getSections() async {
    final result = await _repository.fetchSections();
    if (result != null) {
      return result;
    }
    return <Section>[];
  }

  void insertActionSection() {
    _state = OptionState.formSection;
    notifyListeners();
  }

  void insertSection() async {
    final result = await _repository.insertSection(insertController.text);
    if (result) {
      insertController.clear();
      _state = OptionState.section;
      notifyListeners();
    }
  }

  void updateSection(int index) {
    //_repository.sections[index] = modifyController.text;
    notifyListeners();
  }

  void deleteSection(Section s) async {
    final result = await _repository.deleteSection(s);
    if (result) {
      _updater = !_updater;
      _selectedSection = null;
      notifyListeners();
    }
  }

  get selectedSection {
    return _selectedSection;
  }

  set setSelectedSection(Section? value) {
    _selectedSection = value;
    if (value != null) modifyController.text = value.name;
    _modify = true;
    notifyListeners();
  }

  void back2Section() {
    resetControllers();
    _state = OptionState.section;
    resetSelected(false);
    notifyListeners();
  }

  /* --- TYPE COURS ------------------------------------------- */
  List<String> get courseTypesNames {
    return _repository.courseTypes.map((e) => e.name).toList();
  }

  List<CourseType> get courseTypes {
    return _repository.courseTypes;
  }

  CourseType? get selectedCourseType {
    return _selectedCourseType;
  }

  set setSelectedCourseType(CourseType? value) {
    _selectedCourseType = value;
    if (value != null) {
      showInstrumentalFields = value.hasInstrumental;
      notifyListeners();
    }
  }

  Future<List<CourseType>> getCourseTypes() async {
    final result = await _repository.fetchCourseTypes();
    if (result != null) {
      return result;
    }
    return <CourseType>[];
  }

  void insertActionCourseType() {
    _state = OptionState.formCourseType;
    notifyListeners();
  }

  void insertCourseType() async {
    final result = await _repository.insertCourseType(
      insertController.text,
      _hasInstrumentalToggle,
    );
    if (result) {
      insertController.clear();
      _state = OptionState.courseType;
      notifyListeners();
    }
  }

  void updateTypeCours(int index) {
    //_repository.typeCours[index] = modifyController.text;
    notifyListeners();
  }

  void deleteCourseType(CourseType courseType) async {
    final result = await _repository.deleteCourseType(courseType);
    if (result) {
      _updater = !_updater;
      _selectedCourseType = null;
      notifyListeners();
    }
  }

  void selectTypeCours(int index) {
    modifyController.text = _repository.courseTypes[index].name;
    notifyListeners();
  }

  void back2CourseType() {
    resetControllers();
    _state = OptionState.courseType;
    resetSelected(false);
    notifyListeners();
  }

  void back2Initial() {
    resetControllers();
    _state = OptionState.initial;
    resetSelected(false);
    notifyListeners();
  }

  void resetCourseForm(bool isCollective) {
    nameController.clear();
    if (!isCollective) typeController.currentState!.reset();
    durationController.currentState!.reset();
    childPriceController.clear();
    adultPriceController.clear();
    childPriceInstrumentController.clear();
    adultPriceInstrumentController.clear();
    _selectedCourseType = null;
    _selectedDuration = null;
  }

  void resetControllers() {
    modifyController.clear();
    insertController.clear();
  }

  void insertAction() {
    _state = OptionState.formSection;
    notifyListeners();
  }

  OptionState get state {
    return _state;
  }

  set setOptionState(OptionState value) {
    _state = value;
    notifyListeners();
  }

  bool get hasIntrumentalToggle {
    return _hasInstrumentalToggle;
  }

  set setInstrumentToggle(bool value) {
    _hasInstrumentalToggle = value;
    notifyListeners();
  }

  bool hasAction() {
    return _state != OptionState.color &&
        _state != OptionState.initial &&
        _state != OptionState.language;
  }

  void actionListItem(int index) {
    resetControllers();
    _state = OptionState.values[index + 1];
    notifyListeners();
  }

  bool hasSelected(dynamic item) {
    return _selectedCourse == item ||
        _selectedCourseType == item ||
        _selectedDuration == item ||
        _selectedInstrument == item ||
        _selectedSection == item ||
        _selectedWorkshop == item;
  }

  void setSelected(dynamic item) async {
    if (item is Course) {
      _selectedCourse = item;
    } else if (item is CourseType) {
      _selectedCourseType = item;
    } else if (item is CourseDuration) {
      _selectedDuration = item;
    } else if (item is Instrument) {
      _selectedInstrument = item;
    } else if (item is Section) {
      _selectedSection = item;
    } else if (item is Workshop) {
      _selectedWorkshop = item;
    }
    _modify = false;
    notifyListeners();
  }

  void resetSelected(bool notify) {
    _selectedCourse = null;
    _selectedCourseType = null;
    _selectedDuration = null;
    _selectedInstrument = null;
    _selectedSection = null;
    _selectedWorkshop = null;
    if (notify) notifyListeners();
  }

  bool get modify {
    return _modify;
  }

  bool get updater {
    return _updater;
  }
}
