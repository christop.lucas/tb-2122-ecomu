import 'package:ecomu_gestion/services/ecomu_api/ecomu_api.dart';
import 'package:test/test.dart';

void main() {
  final api = EcomuApi();
  group('Request Person', () {
    test('Fetch all the persons', () async {
      final result = await api.fetchPersons();
      expect(result!.length, 4);
    });
  });
}
