-- phpMyAdmin SQL Dump
-- version 4.9.6
-- https://www.phpmyadmin.net/
--
-- Host: yn08u.myd.infomaniak.com
-- Generation Time: Jul 05, 2022 at 03:29 PM
-- Server version: 10.4.17-MariaDB-1:10.4.17+maria~jessie-log
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yn08u_ecomu`
--

-- --------------------------------------------------------

--
-- Table structure for table `inscriptions`
--

CREATE TABLE `inscriptions` (
  `id_inscription` int(11) NOT NULL,
  `fk_eleve` int(11) NOT NULL,
  `fk_rep_legal` int(11) NOT NULL,
  `fk_payeur` int(11) NOT NULL,
  `fk_professeur` int(11) NOT NULL,
  `fk_cours` int(11) NOT NULL,
  `fk_instrument` int(11) DEFAULT NULL,
  `fk_atelier` int(11) DEFAULT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date DEFAULT NULL,
  `fk_jour` int(11) NOT NULL,
  `heure` time NOT NULL,
  `fk_section` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inscriptions`
--

INSERT INTO `inscriptions` (`id_inscription`, `fk_eleve`, `fk_rep_legal`, `fk_payeur`, `fk_professeur`, `fk_cours`, `fk_instrument`, `fk_atelier`, `date_debut`, `date_fin`, `fk_jour`, `heure`, `fk_section`, `is_active`) VALUES
(5, 1, 1, 1, 2, 1, 1, 1, '2022-05-16', NULL, 1, '09:54:38', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `inscriptions`
--
ALTER TABLE `inscriptions`
  ADD PRIMARY KEY (`id_inscription`) USING BTREE,
  ADD KEY `fk_rep_legal` (`fk_rep_legal`),
  ADD KEY `fk_payeur` (`fk_payeur`),
  ADD KEY `fk_eleve` (`fk_eleve`),
  ADD KEY `fk_professeur` (`fk_professeur`),
  ADD KEY `fk_cours` (`fk_cours`),
  ADD KEY `fk_instrument` (`fk_instrument`),
  ADD KEY `fk_atelier` (`fk_atelier`),
  ADD KEY `fk_jour` (`fk_jour`),
  ADD KEY `fk_section` (`fk_section`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `inscriptions`
--
ALTER TABLE `inscriptions`
  MODIFY `id_inscription` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `inscriptions`
--
ALTER TABLE `inscriptions`
  ADD CONSTRAINT `fk_atelier` FOREIGN KEY (`fk_atelier`) REFERENCES `cours_atelier` (`id_cours_atelier`),
  ADD CONSTRAINT `fk_cours` FOREIGN KEY (`fk_cours`) REFERENCES `cours` (`id_cours`),
  ADD CONSTRAINT `fk_eleve` FOREIGN KEY (`fk_eleve`) REFERENCES `personnes` (`id_personne`),
  ADD CONSTRAINT `fk_instrument` FOREIGN KEY (`fk_instrument`) REFERENCES `instruments` (`id_instrument`),
  ADD CONSTRAINT `fk_jour` FOREIGN KEY (`fk_jour`) REFERENCES `jours` (`id_jour`),
  ADD CONSTRAINT `fk_payeur` FOREIGN KEY (`fk_payeur`) REFERENCES `personnes` (`id_personne`),
  ADD CONSTRAINT `fk_professeur` FOREIGN KEY (`fk_professeur`) REFERENCES `professeurs` (`id_professeur`),
  ADD CONSTRAINT `fk_rep_legal` FOREIGN KEY (`fk_rep_legal`) REFERENCES `personnes` (`id_personne`),
  ADD CONSTRAINT `fk_section` FOREIGN KEY (`fk_section`) REFERENCES `sections` (`id_section`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
