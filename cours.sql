-- phpMyAdmin SQL Dump
-- version 4.9.6
-- https://www.phpmyadmin.net/
--
-- Host: yn08u.myd.infomaniak.com
-- Generation Time: Jul 05, 2022 at 03:28 PM
-- Server version: 10.4.17-MariaDB-1:10.4.17+maria~jessie-log
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yn08u_ecomu`
--

-- --------------------------------------------------------

--
-- Table structure for table `cours`
--

CREATE TABLE `cours` (
  `id_cours` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `tarif_enfant` float NOT NULL,
  `tarif_adulte` float NOT NULL,
  `fk_cours_type` int(11) NOT NULL,
  `tarif_instrumental_enfant` float NOT NULL,
  `tarif_instrumental_adulte` float NOT NULL,
  `fk_duree_cours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cours`
--

INSERT INTO `cours` (`id_cours`, `nom`, `tarif_enfant`, `tarif_adulte`, `fk_cours_type`, `tarif_instrumental_enfant`, `tarif_instrumental_adulte`, `fk_duree_cours`) VALUES
(1, 'Individuel 30', 1460, 2350, 1, 0, 0, 1656462600),
(2, 'Individuel 45', 1940, 3095, 1, 0, 0, 1656463500),
(3, 'Individuel 60', 2320, 3870, 1, 0, 0, 1656464400),
(4, 'Atelier', 700, 900, 2, 560, 700, 1656463500),
(5, 'Solfège', 450, 480, 3, 140, 140, 1656463500),
(6, 'Jardin des chansons', 300, 0, 3, 0, 0, 1656463500),
(7, 'Initiation musicale Willems', 480, 0, 3, 0, 0, 1656463500),
(8, 'Petit Choeur / Coeur des ados', 300, 0, 3, 0, 0, 1656463500);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cours`
--
ALTER TABLE `cours`
  ADD PRIMARY KEY (`id_cours`),
  ADD KEY `fk_duree_cours` (`fk_duree_cours`),
  ADD KEY `fk_cours_type` (`fk_cours_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cours`
--
ALTER TABLE `cours`
  MODIFY `id_cours` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cours`
--
ALTER TABLE `cours`
  ADD CONSTRAINT `fk_cours_type` FOREIGN KEY (`fk_cours_type`) REFERENCES `cours_type` (`id_cours_type`),
  ADD CONSTRAINT `fk_duree_cours` FOREIGN KEY (`fk_duree_cours`) REFERENCES `duree_cours` (`id_duree_cours`) ON DELETE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
