# Travail de bachelor - Ecomu
## Contexte
Ce document décrit les choix techniques ainsi que les étapes pour réaliser le travail de bachelor « Ecomu – logiciel de gestion pour école de musique ». Le mandat viens de l’entreprise SoftDesign Sàrl et supervisé par Pascal Bruegger. Ce projet est possible grâce au Conservatoire de musique du Nord vaudois. Le CMNV est un Conservatoire et École de Musique se situant à Yverdon-les-Bains. Le Conservatoire fait partie de l’Association Vaudoise et Suisse des Conservatoires et Écoles de musique. L’établissement propose une formation musicale à des enfants, adolescents et adultes. Les cours sont donnés par des professeurs diplômés professionnellement. Les diplômes sont obtenus en accomplissant une Haute École de Musique. Les professeurs donnent des cours individuels, collectifs ou sous forme d’ateliers. Les cours doivent être suivis de manières régulières. L’étudiant peut pratiquer l’instrument de musique de son choix. Le chapitre suivant introduit le besoin de ce projet.

## Objectifs
Le projet « Ecomu – logiciel de gestion pour école de musique » a pour but primaire d’explorer le développement d’application Windows et macOS avec Flutter en réalisant le gestionnaire de cours de musique pour le Conservatoire de musique du Nord vaudois. Le projet doit livrer une application fonctionnelle avec les fonctionnalités « Must have » suivantes :
-	Visualiser, ajouter, modifier et supprimer des clients (élèves, représentant légal, payeur).
-	Visualiser, ajouter, modifier et supprimer des professeurs ainsi que leur disponibilité.
-	Visualiser et gérer les inscriptions et les cours.
-	Visualiser, ajouter, imprimer, modifier et supprimer des factures.
-	Synchronisation simple des données entre les bases de données locales et externes.
-	Protéger l’application avec un login

En fonction du temps, le projet peut offrir les fonctionnalités « Nice to Have » suivantes :
-	Synchronisation plus avancée des données entre les bases de données locales et externes.
-	S’assurer du fonctionnement de l’application Web
-	Le multilingue

Flutter vient de publier une version stable pour développer des applications Windows, macOS et Linux. Si les objectifs primaires sont atteints, ce projet prouvera à l’entreprise SoftDesign que la technologie Flutter est capable de produire des bons logiciels de gestion Desktop. SoftDesign n’aura pas besoin de recommencer l’application de gestion avec une autre technologie.


## Contenu

* `analyse/` : Tous les fichiers en lien avec l'analyse du projet
* `code/` : Tous les fichiers en lien avec l'implémentation du projet
* `conception/` :  Tous les fichiers en lien avec la conception du projet
* `documents/` : Toutes la documentation et fichiers d'administrations du projet
* `expert/` : Tous les documents en lien avec les experts


## Contributor and Author

- **Contributor1** : [Lucas Christopher](https://gitlab.forge.hefr.ch/christop.lucas) - christop.lucas@edu.hefr.ch